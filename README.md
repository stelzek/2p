# Patient Messenger: The messaging system for your waiting room.
## Install PHP 5.6 ##

* https://github.com/Homebrew/homebrew-php/blob/master/README.md
* brew install php56-pdo-pgsql

## To install on your computer, type in command-line: ##
  
      git clone https://bitbucket.org/onekit/patientMessenger.git

      cd patientMessenger

      curl -s https://getcomposer.org/installer | php

      php app/check.php

      mkdir app/logs
      mkdir app/cache
      mkdir vendor
      chmod 777 app/logs -R
      chmod 777 app/cache -R
      chmod 777 vendor -R
      chmod 755 build.sh
      php composer.phar install
      php app/console doctrine:database:create
      php app/console doctrine:schema:create
      php app/console doctrine:fixtures:load
      php app/console asset:install --symlink
      php app/console asset:dump


      php app/console server:run localhost:8080
    
    Available on localhost:8080 with credentials:
    Login: admin
    Password: admin
    
    Login: doctor
    Password: admin
    
    Login: assistant
    Password: admin
    
    Login: patient
    Password: admin
    
    While installation on production, probably you require Apache2, PostgreSQL and some modules for PHP: GD, Intl, PGSQL, PDO, etc.

    Launch daemons after all:
    ./schedule-start.sh