#Brief guide for 2p-med.de functionality tests

  2p-med.de web app has been developed to let the doctor make only his job and don't care about queue and appointments.  
  We provide semi-automatic message system to notify patients about their appointments.

##Functionality
  There are three roles presents: Administrator, Doctor and Assistant.

###Administrator
  This role is site owner role have most huge power and can manage all data including changing defaults.  
  Administrator see all users (assistants, doctors, patients) as equal and can edit any data of any user.  
  - can see list of appointments and edit them  
  - can see list of logs (sms history)  
  - can manage templates of messages  and edit them  

  Default administrator's credentials for test:  
  Login: admin  
  Password: admin

###Doctor
  This role is the main customer account, who manage their assistants and patients.  
  - can manage patients (add, edit, delete)   
  - can manage own assistants (add, edit, delete)  
  - can share balance with assistants  
  - can see own and assistants sms history  
  - can customize sms templates messages which override administrator templates settings  

  Default doctor's credentials for test:  
  Login: doctor  
  Password: admin

###Assistant
  - can add patient
  - can make appointments for patients
  - see own sms history

  Default assistant's credentials for test:  
  Login: assistant  
  Password: admin  


---

#Doctor's scenario

##Step 1. Login
  Open [http://2p-med.de](http://2p-med.de)  
  At the right-top corner, open menu:  
>  ![Landing page](http://2p-med.de/bundles/app/img/wiki/screenshot_landing_page_menu.jpg)   

  Then pick __Login__.  
  
>  ![Landing page menu opened](http://2p-med.de/bundles/app/img/wiki/screenshot_landing_page_menu_opened.jpg)  

  To login as doctor role use following credentials:   
    Login: __doctor__   
    Password: __admin__   
>  ![Login page](http://2p-med.de/bundles/app/img/wiki/screenshot_login_page.jpg)  
  

##Step 2. Add new assistant
  
  Doctor can have unlimited quantity of assistants.  
  Let's add first assistant to delegate work with appointments to him.  
  After login, doctor comes to this page.   
  Choose assistants menu.  
>  ![Doctor's home page](http://2p-med.de/bundles/app/img/wiki/screenshot_doctor_home.jpg)  
  
  Here is you can manage (edit/delete) assistants. Press button __Add assistant__.  
>  ![Add assistant](http://2p-med.de/bundles/app/img/wiki/screenshot_doctor_assistant_add.jpg)  
     
  Fill form with data something like that:    
>  ![Add assistant form empty](http://2p-med.de/bundles/app/img/wiki/screenshot_doctor_assistant_add_form_filled.jpg)
  
  As result you'll see assistant's profile page:  
>  ![Add assistant form filled](http://2p-med.de/bundles/app/img/wiki/screenshot_doctor_assistant_added.jpg)  
    
##Step 3. Add credits to assistant

  After assistant created, we need to add some funds on it account. That depend on how much you trust to exact assistant.  
>  ![Add credits to assistant](http://2p-med.de/bundles/app/img/wiki/screenshot_doctor_assistant_credits_add.jpg)  
  
  Let's add 15 credits to assistant:  
>  ![Add 15 credits](http://2p-med.de/bundles/app/img/wiki/screenshot_doctor_assistant_credits_add_form.jpg)    
  
  As you see, doctor's amount reduced on 15 credits and now we see this amount at assistant: 
>  ![Credits added](http://2p-med.de/bundles/app/img/wiki/screenshot_doctor_assistant_credits_added.jpg)       

##Step 4. Manage notification templates (optional)

  Select menu __SMS templates__. Here is multi-language messages which will be sent on any event.
  
>  ![SMS Templates list](http://2p-med.de/bundles/app/img/wiki/screenshot_doctor_sms_templates.jpg)
  
  In template messages, you can use macros which be replace to appointment data: 
   
  *{title}* - Mr/Mrs Herr/Frau  
  *{firstName}* - Name   
  *{lastName}* - Surname  
  *{time}* - hours and minutes (part of appointment date)  
  *{fullDate}* - day, month, year (part of appointment date)    

>  ![SMS Template edit](http://2p-med.de/bundles/app/img/wiki/screenshot_doctor_sms_template_edit.jpg)  

##Step 5. SMS History

  SMS history and spending, doctor can see in the same section.  
>  ![SMS History](http://2p-med.de/bundles/app/img/wiki/screenshot_doctor_sms_history.jpg)
  
---

#Assistant's scenario

##Step 1. Login

  Start as in doctor's scenario.  
  Use following credentials to enter:  
  Login: __assistant__  
  Password: __admin__  
  
##Step 2. Add patient  

 We are on patients list page after enter. Click on __Add patient__ button.  
> ![Patients list](http://2p-med.de/bundles/app/img/wiki/screenshot_doctor_patients.jpg)  
 
 Fill the form with unique data.  
> ![Add patient](http://2p-med.de/bundles/app/img/wiki/screenshot_doctor_patient_add.jpg)  
 
> If edit patient phone number to something else than started with "49" (country code), then SMS notification will work as Email notifications  
 
##Step 3. Assign appointment for patient

 You're on patient page where you can click on any day to assign new appointment for this patient.  
> ![Patient profile page](http://2p-med.de/bundles/app/img/wiki/screenshot_doctor_patient.jpg)
     
 For example, clicked June 15th. You'll see appointment popover dialog:  
> ![Appointment creation](http://2p-med.de/bundles/app/img/wiki/screenshot_doctor_patient_appointment_add.jpg)  
 
 Here is you can choose which notification is required for this patient.  
> If you'll just hit enter, then notification will save without sending and all checkbox will be ignored. In case choosing "Send" button, then all notification will be scheduled.  
    
> ![Notifications](http://2p-med.de/bundles/app/img/wiki/screenshot_doctor_patient_appointment_add_notifications.jpg)      
 
 That's it. Appointment assigned on 15:30.  
> ![Appointment added](http://2p-med.de/bundles/app/img/wiki/screenshot_doctor_patient_appointment_added.jpg)      

##Step 4. Change appointment date or time 
 
 In case of changing date of appointment, you can easily drag and drop appointment to another date.  
> ![Drag 'n' Drop](http://2p-med.de/bundles/app/img/wiki/screenshot_doctor_patient_appointment_drag.jpg)  

 If you need to change only time or cancel appointment, click on appointment and choose required action.  
> ![Change time](http://2p-med.de/bundles/app/img/wiki/screenshot_doctor_patient_appointment_change.jpg)    
 
> To hide popover, click again on appointment (it toggle style control)  



   
  

