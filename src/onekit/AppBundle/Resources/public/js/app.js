/* begin websocket */
var wsPort = 9200;
var wsProtocol = "ws";
if ("https:" == document.location.protocol) { wsPort = 8443; wsProtocol = "wss"; }
var wsuri = wsProtocol + "://" + window.location.hostname + ":" + wsPort;
/* end websocket */


// on escape button
$(document).keyup(function (e) {
    if (e.keyCode == 27) {
        bootbox.hideAll();
    }
});

bootstrap_alert = function() {};;
var i = 1;
bootstrap_alert.flash = function(message, type) {
    type = typeof type !== 'undefined' ? type : 'success';
    i = i + 1;
    $('<div id="alert'+i+'" class="alert alert-'+type+'"><button type="button" class="close" data-dismiss="alert"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>'+message+'</div>').appendTo($('#messageBox')).delay(7000);
    $('#alert'+i).fadeOut(500);
};;


