<?php

namespace onekit\AppBundle;

use Ratchet\ConnectionInterface as Conn;
use Ratchet\Wamp\WampServerInterface;

class PubSub implements WampServerInterface {

    protected $subscribedTopics = [];

    public function onPublish(Conn $conn, $topic, $event, array $exclude, array $eligible) {
        // In this application if clients send data it's because the user hacked around in console
        $conn->close();
        //$topic->broadcast($event);
    }

    public function onCall(Conn $conn, $id, $topic, array $params) {
        $conn->callError($id, $topic, 'RPC not supported on this demo');
    }

    public function onOpen(Conn $conn) {
    }

    public function onClose(Conn $conn) {
    }

    public function onSubscribe(Conn $conn, $topic) {
        echo $topic.PHP_EOL;
        if (!array_key_exists($topic->getId(), $this->subscribedTopics)) {
            $this->subscribedTopics[$topic->getId()] = $topic;
        }
    }

    public function onUnSubscribe(Conn $conn, $topic) {
    }

    public function onError(Conn $conn, \Exception $e) {
    }

    /**
     * @param string JSON'ified string we'll receive from ZeroMQ
     */
    public function onCurrentUpdate($entry) {
        $entryData = json_decode($entry, true);
        if (!array_key_exists($entryData['id'], $this->subscribedTopics)) {
            return;
        }
        $topic = $this->subscribedTopics[$entryData['id']];
        $topic->broadcast($entryData);
    }

}