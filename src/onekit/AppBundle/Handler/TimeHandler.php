<?php
namespace onekit\AppBundle\Handler;


use Symfony\Component\Translation\TranslatorInterface;

class TimeHandler
{
    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * TimeHandler constructor.
     */
    public function __construct(TranslatorInterface $translator)
    {
        $this->translator = $translator;
    }

    public function timeRemaining(\DateTime $time, $locale = null)
    {
        $format = '';
        $diff = $time->diff(new \DateTime());
        if ($diff->i > 5) {
            $diff->i -= $diff->i % 5;
        }
        if ($diff->i || (!$diff->h && !$diff->h && !$diff->d && !$diff->m && !$diff->y)) {
            $format = sprintf('%u %s', $diff->i, $this->translator->trans('date.format.minute'.($diff->i > 1 ? 's' : ''), array(), null, $locale));
        }
        if ($diff->h) {
            $format = sprintf('%u %s', $diff->h, $this->translator->trans('date.format.hour'.($diff->h > 1 ? 's' : ''), array(), null, $locale)).' '.$format;
        }
        if ($diff->d) {
            $format = sprintf('%u %s', $diff->d, $this->translator->trans('date.format.day'.($diff->d > 1 ? 's' : ''), array(), null, $locale)).' '.$format;
        }
        if ($diff->m) {
            $format = sprintf('%u %s', $diff->m, $this->translator->trans('date.format.month'.($diff->m > 1 ? 's' : ''), array(), null, $locale)).' '.$format;
        }
        if ($diff->y) {
            $format = sprintf('%u %s', $diff->y, $this->translator->trans('date.format.year'.($diff->y > 1 ? 's' : ''), array(), null, $locale)).' '.$format;
        }
        return $format;
    }
}