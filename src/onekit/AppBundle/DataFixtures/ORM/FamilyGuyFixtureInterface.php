<?php
namespace onekit\AppBundle\DataFixtures\ORM;


interface FamilyGuyFixtureInterface
{
    function loadFamilyGuy();
}