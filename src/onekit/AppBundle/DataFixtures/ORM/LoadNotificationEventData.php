<?php

namespace onekit\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use onekit\AppBundle\Entity\Appointment;
use onekit\AppBundle\Entity\Doctor;
use onekit\AppBundle\Entity\NotificationEvent;
use onekit\AppBundle\Entity\Patient;
use onekit\AppBundle\Entity\SMSMessageBody;
use onekit\AppBundle\Entity\SMSMessageTemplate;
use onekit\AppBundle\Entity\User;
use onekit\AppBundle\Extractor\NotificationEventExtractor;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Translation\TranslatorInterface;


class LoadNotificationEventData extends AbstractFixture implements ContainerAwareInterface
{
    /**
     * @var NotificationEventExtractor
     */
    protected $notificationEventExtractor;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @var ObjectManager
     */
    protected $manager;
    public function setContainer(ContainerInterface $container = null)
    {
        $this->notificationEventExtractor = $container->get('app.extractor.notification_event');
        $this->translator = $container->get('translator');
    }

    protected function createMessageTemplate(NotificationEvent $event)
    {
        $smsTemplate = new SMSMessageTemplate();
        $smsTemplate->setNotificationEvent($event);
        foreach (User::getLanguages() as $lang) {
            $smsBody = new SMSMessageBody();
            $smsBody->setLanguage($lang);
            $smsBody->setMessage($this->translator->trans(sprintf('notifications.%s.message', $event->getAction()), array(), null, $lang));
            $smsTemplate->addSMSMessageBody($smsBody);
        }
        $this->manager->persist($smsTemplate);
        $this->manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        foreach ($this->notificationEventExtractor->all() as $event) {
            $this->createMessageTemplate($event);
        }
        unset($this->manager);
   }
}