<?php

namespace onekit\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use onekit\AppBundle\Entity\Appointment;
use onekit\AppBundle\Entity\Doctor;
use onekit\AppBundle\Entity\NotificationEvent;
use onekit\AppBundle\Entity\Patient;
use onekit\AppBundle\Entity\SMSMessageBody;
use onekit\AppBundle\Entity\SMSMessageTemplate;
use onekit\AppBundle\Entity\User;
use onekit\AppBundle\Extractor\NotificationEventExtractor;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Translation\TranslatorInterface;


class LoadData extends AbstractFixture implements DependentFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
    }

    function getDependencies()
    {
        return array(
            'onekit\AppBundle\DataFixtures\ORM\LoadPatientData',
            'onekit\AppBundle\DataFixtures\ORM\LoadNotificationEventData',
        );
    }
}