<?php

namespace onekit\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use onekit\AppBundle\Entity\Appointment;
use onekit\AppBundle\Entity\Doctor;
use onekit\AppBundle\Entity\Input\CreateAppointment;
use onekit\AppBundle\Entity\Patient;
use onekit\AppBundle\Entity\User;
use onekit\AppBundle\Manager\ApiAppointmentManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadPatientData extends AbstractFixture implements DependentFixtureInterface, FamilyGuyFixtureInterface, ContainerAwareInterface
{
    /**
     * @var ObjectManager
     */
    protected $manager;

    public function setContainer(ContainerInterface $container = null)
    {
        //$this->manager = $container->get('doctrine.orm.default_entity_manager');
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $this->createPatient('patient-brad', 'doctor', 'Mr.', 'Brad', 'Storm', 1);
        $anna = $this->createPatient('patient-anna', 'doctor', 'Mrs.', 'Anna', 'Storm', 0);
        $date = new \DateTime();
        for ($a =0;  $a < 20; $a++) {
            $start = clone $date->modify('+1 DAY 15 MINUTES');
            $end = clone $date->modify('+15 MINUTES');
            $this->createAppointment($anna, $start, $end);
        }
        $this->createPatient('patient-alex', 'doctor', 'Mr.', 'Alex', 'Vitebsk', 1, null, 'alex@vitebsk.onl', false, true, User::LANG_EN);
        $this->loadFamilyGuy();
    }

    protected function createAppointment(Patient $patient, $start, $end)
    {
        $this->manager->refresh($patient);
        $appointment = new Appointment();
        $appointment->setPatient($patient);
        $appointment->setDoctor($patient->getDoctor());
        $appointment->setStart($start);
        $appointment->setEnd($end);
        $appointment->setNotifySMS($patient->getNotifySMS());
        $appointment->setNotifyEmail($patient->getNotifyEmail());
        $this->manager->persist($appointment);
        $this->manager->flush();
    }

    /**
     * @param $doctorName
     * @return Doctor
     */
    protected function getDoctor($doctorName) {
        if ($this->hasReference(sprintf('doctor:%s', $doctorName))) {
            /** @var Doctor $doctor */
            $doctor = $this->getReference(sprintf('doctor:%s', $doctorName));
            /** @var Doctor $doctor */
            $doctor = $this->manager->getRepository('AppBundle:Doctor')->find($doctor->getId());
            return $doctor;

        }
        return null;
    }

    public function getDependencies()
    {
        return array(
            'onekit\AppBundle\DataFixtures\ORM\LoadDoctorData',
        );
    }

    public function loadFamilyGuy()
    {
        // with accounts
        $this->createPatient('fg:peter.griffin', 'fg:elmer.hartman', 'Mr.', 'Peter', 'Griffin', 1);
        $this->createPatient('fg:lois.griffin', 'fg:elmer.hartman', 'Mrs.', 'Lois', 'Griffin', 0);
        $this->createPatient('fg:chris.griffin', 'fg:elmer.hartman', 'Mr.', 'Chris', 'Griffin', 1);
        $this->createPatient('fg:stewie.griffin', 'fg:elmer.hartman', 'Mr.', 'Stewart', 'Griffin', 1);

        // meg
        $meg = $this->createPatient('fg:meg.griffin', 'fg:elmer.hartman', 'Mrs.', 'Megan', 'Griffin', 0);
        //$this->manager->remove($meg);

        // without accounts
        $this->createPatient('fg:joe.swanson', 'fg:elmer.hartman', 'Mr.', 'Joseph', 'Swanson', 1);
        $this->createPatient('fg:bonnie.swanson', 'fg:elmer.hartman', 'Mrs.', 'Bonnie', 'Swanson', 0);
        $this->createPatient('fg:kevin.swanson', 'fg:elmer.hartman', 'Mr.', 'Kevin', 'Swanson', 1);
        $this->createPatient('fg:susie.swanson', 'fg:elmer.hartman', 'Mrs.', 'Susie', 'Swanson', 0);
        $this->createPatient('fg:john.herbert', 'fg:elmer.hartman', 'Mr.', 'John', 'Herbert', 1);
        $this->createPatient('fg:glenn.quagmire', 'fg:elmer.hartman', 'Mr.', 'Glenn', 'Quagmire', 1);
        $this->createPatient('fg:cleveland.brown', 'fg:elmer.hartman', 'Mr.', 'Cleveland', 'Brown', 1);

    }

    /**
     * @param $username
     * @return User|null
     */
    protected function getUser($username) {
        if ($this->hasReference(sprintf('user:%s', $username))) {
            /** @var User $user */
            $user = $this->getReference(sprintf('user:%s', $username));
            /** @var User $user */
            $user = $this->manager->getRepository('AppBundle:User')->find($user->getId());
            return $user;
        }
        return null;
    }

    protected function createPatient($referenceName, $doctorReference, $title, $firstName, $lastName, $gender, $phone = null, $email = null, $notifySMS = false, $notifyEmail = false, $lang = null)
    {
        $doctor = $this->getDoctor($doctorReference);
        $patient = new Patient();
        $patient->setTitle($title);
        $patient->setGender(intval((bool)$gender));
        $patient->setFirstName($firstName);
        $patient->setLastName($lastName);
        $patient->setAccount($this->getUser($referenceName));
        $patient->setDoctor($doctor);
        $patient->setNotifySMS($notifySMS);
        $patient->setNotifyEmail($notifyEmail);
        $patient->setPhone($phone);
        $patient->setEmail($email);
        $patient->setLanguage($lang);
        $this->manager->persist($patient);
        $this->manager->flush();
        return $patient;
    }

}