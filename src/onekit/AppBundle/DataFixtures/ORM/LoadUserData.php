<?php

namespace onekit\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\Persistence\ObjectManager;
use FOS\UserBundle\Util\UserManipulator;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadUserData extends AbstractFixture implements ContainerAwareInterface, FamilyGuyFixtureInterface
{
    /**
     * @var UserManipulator
     */
    protected $userManipulator;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->userManipulator = $container->get('fos_user.util.user_manipulator');
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        // admins
        $this->createUser(false, 'ant', 'admin', 'onekit@gmail.com', true);
        $this->createUser(false, 'frank', 'admin', 'frank.scrock@mac.com', true);
        $this->createUser(false, 'claus', 'admin', 'frank.scrock@me.com', true);
        $this->createUser('admin', 'admin', 'admin', 'frank.scrock@booming-games.com', true);
        $this->createUser(false, 'stelzek', 'admin', 'stelzek@gmail.com', true);
        $this->createUser(false, 'alexey', 'admin', 'alex@vitebsk.onl', true);

        // doctors
        $this->createUser('doctor', 'doctor', 'admin', 'doctor@domain.com');

        // assistants
        $this->createUser('assistant', 'assistant', 'admin', 'somebody@domain.com');

        // patients
        $this->createUser('patient', 'patient', 'admin', 'random@email.com');

        $this->loadFamilyGuy();
    }

    public function loadFamilyGuy()
    {
        // doctors
        $this->createUser('fg:elmer.hartman', 'elmer.hartman', '911421952796', 'elmer.hartman@family.guy');

        // assistants

        // patients with accounts
        $this->createUser('fg:peter.griffin', 'peter.griffin', '108078842378', 'peter.griffin@family.guy');
        $this->createUser('fg:lois.griffin', 'lois.griffin', '393667422912', 'lois.griffin@family.guy');
        $this->createUser('fg:chris.griffin', 'chris.griffin', '529111506343', 'chris.griffin@family.guy');
        $this->createUser('fg:stewie.griffin', 'stewie.griffin', '616144434555', 'stewie.griffin@family.guy');
        $this->createUser('fg:meg.griffin', 'meg.griffin', '105228169560', 'meg.griffin@family.guy');
    }

    public function loadSimpsonsGuy()
    {
        $this->createUser('sg:nick.riviera', 'nick.riviera', '322425059422', 'nick.riviera@simpsons.guy');
    }

    protected function createUser($referenceName, $username, $password, $email, $isAdmin = false)
    {
        $user = $this->userManipulator->create($username, $password, $email, true, $isAdmin);
        if (trim($referenceName)) {
            $this->setReference(sprintf('user:%s', trim($referenceName)), $user);
        }
        return $user;
    }
}