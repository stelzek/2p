<?php

namespace onekit\AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use onekit\AppBundle\Entity\Doctor;
use onekit\AppBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;


class LoadDoctorData extends AbstractFixture implements DependentFixtureInterface, ContainerAwareInterface, FamilyGuyFixtureInterface
{
    /**
     * @var ObjectManager
     */
    protected $manager;

    public function setContainer(ContainerInterface $container = null)
    {
        //$this->manager = $container->get('doctrine.orm.default_entity_manager');
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $this->createDoctor('doctor', 'Blue Walker');
        $this->loadFamilyGuy();
        unset($this->manager);
    }

    public function loadFamilyGuy()
    {
        $this->createDoctor('fg:elmer.hartman', 'Dr. Elmer Hartman');
    }

    protected function createDoctor($referenceName, $title, $phone = null, $address = null)
    {
        $doctor = new Doctor();
        $account = $this->getUser($referenceName);
        $this->manager->refresh($account);
        $doctor->setAccount($account);
        $doctor->setTitle($title);
        $doctor->setPhone($phone);
        $doctor->setAddress($address);
        $this->manager->persist($doctor);
        $this->manager->flush();
        if (trim($referenceName)) {
            $this->setReference(sprintf('doctor:%s', trim($referenceName)), $doctor);
        }
    }

    /**
     * @param $username
     * @return User|null
     */
    protected function getUser($username) {
        if ($this->hasReference(sprintf('user:%s', $username))) {
            /** @var User $user */
            $user = $this->getReference(sprintf('user:%s', $username));
            /** @var User $user */
            $user = $this->manager->getRepository('AppBundle:User')->find($user->getId());
            return $user;
        }
        return null;
    }

    function getDependencies()
    {
        return array(
            'onekit\AppBundle\DataFixtures\ORM\LoadUserData',
        );
    }
}