<?php
namespace onekit\AppBundle\Repository;


use Doctrine\ORM\EntityRepository;
use onekit\AppBundle\Entity\Appointment;

class ScheduleRepository extends EntityRepository
{
    public function getListQB()
    {
        $qb = $this->createQueryBuilder('s');
        return $qb;
    }

    public function clearAppointmentQB(Appointment $appointment)
    {
        $qb = $this->createQueryBuilder("s")
            ->delete()
            ->where('s.appointment  = :appointment')->setParameter("appointment", $appointment);
        return $qb;
    }

}