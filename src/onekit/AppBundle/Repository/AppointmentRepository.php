<?php

namespace onekit\AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class AppointmentRepository extends EntityRepository
{
    public function getListQB()
    {
        $qb = $this->createQueryBuilder('a');
        return $qb;
    }

    public function getAppointmentListQB(\DateTime $start = null, \DateTime $finish = null)
    {
        $qb = $this->createQueryBuilder('a');
        if ($start) {
            $qb->andWhere('a.start >= :start')->setParameter('start', $start);
        }
        if ($finish) {
            $qb->andWhere('a.start <= :finish')->setParameter('finish', $finish);
        }
        return $qb;
    }

    public function getAppointmentByIdQB($id)
    {
        $qb = $this->createQueryBuilder('a')
            ->where('a.id = :id')
            ->setParameter('id', $id);

        return $qb;
    }

}