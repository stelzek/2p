<?php
namespace onekit\AppBundle\Extractor;


use Doctrine\Common\Annotations\Reader;
use Doctrine\ORM\EntityManagerInterface;
use onekit\AppBundle\Entity\Notification;
use onekit\AppBundle\Entity\NotificationEvent;
use Symfony\Component\Routing\RouterInterface;

class NotificationEventExtractor
{
    const ANNOTATION_CLASS = 'onekit\\AppBundle\\Annotation\\NotificationEvent';

    /**
     * @var RouterInterface
     */
    protected $router;

    /**
     * @var Reader
     */
    protected $reader;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    public function __construct(RouterInterface $router, Reader $reader, EntityManagerInterface $em)
    {
        $this->router = $router;
        $this->reader = $reader;
        $this->em = $em;
    }

    public function all()
    {
        $results = array();
        $routes = $this->router->getRouteCollection()->all();
        foreach ($routes as $routeName => $route) {
            if ($method = $this->getReflectionMethod($route->getDefault('_controller'))) {
                /** @var \onekit\AppBundle\Annotation\NotificationEvent $annotation */
                if ($annotation = $this->reader->getMethodAnnotation($method, self::ANNOTATION_CLASS)) {
                    $action = $annotation->getAction() ? $annotation->getAction() : $routeName;
                    if (!$this->em->getRepository('AppBundle:NotificationEvent')->findOneBy(['action' => $action])) {
                        $results[$action] = new NotificationEvent($action);
                    }
                }
            }
        }
        return $results;
    }

    public function get($action)
    {
        $events = $this->all();
        return isset($events[$action]) ? $events[$action] : null;
    }

    /**
     * Returns the ReflectionMethod for the given controller string.
     *
     * @param string $controller
     * @return \ReflectionMethod|null
     */
    public function getReflectionMethod($controller)
    {
        if (preg_match('#(.+)::([\w]+)#', $controller, $matches)) {
            $class = $matches[1];
            $method = $matches[2];
            if (isset($class) && isset($method)) {
                try {
                    return new \ReflectionMethod($class, $method);
                } catch (\ReflectionException $e) {
                }
            }
        }
        return null;
    }

}