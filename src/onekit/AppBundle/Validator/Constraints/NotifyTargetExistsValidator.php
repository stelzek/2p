<?php
namespace onekit\AppBundle\Validator\Constraints;


use Doctrine\ORM\EntityManager;
use onekit\AppBundle\Entity\Appointment;
use onekit\AppBundle\Entity\Input\CreateAppointment;
use onekit\AppBundle\Entity\Input\CreatePatient;
use onekit\AppBundle\Entity\Patient;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class NotifyTargetExistsValidator
 * @package onekit\AppBundle\Validator\Constraints
 */
class NotifyTargetExistsValidator extends ConstraintValidator
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    public function __construct(EntityManager $em, RequestStack $requestStack)
    {
        $this->em = $em;
        $this->requestStack = $requestStack;
    }

    public function validate($value, Constraint $constraint)
    {
        if ($constraint instanceof NotifyTargetExists && $value instanceof CreateAppointment) {
            /** @var Patient $patient */
            $patient = $this->requestStack->getCurrentRequest()->attributes->get('patient');
            if (is_null($patient)) {
                /** @var Appointment $appointment */
                $appointment = $this->requestStack->getCurrentRequest()->attributes->get('appointment');
                $patient = $appointment->getPatient();
            }
            if (is_null($patient)) {
                throw new \Exception('Patient not found.');
            }
            if ($value->notify_email && !$patient->getEmail()) {
                $this->generateViolation($constraint, 'notify_email');
            }
            if ($value->notify_sms && !$patient->getPhone()) {
                $this->generateViolation($constraint, 'notify_sms');
            }
        } elseif ($constraint instanceof NotifyTargetExists && $value instanceof CreatePatient) {
            if ($value->notify_email && !$value->email) {
                $this->generateViolation($constraint, 'notify_email');
            }
            if ($value->notify_sms && !$value->phone) {
                $this->generateViolation($constraint, 'notify_sms');
            }
        } else {
            throw new \Exception();
        }
    }

    protected function generateViolation(NotifyTargetExists $constraint, $property)
    {
        $this->context
            ->buildViolation($constraint->message)
            ->atPath($property)
            ->addViolation();
    }

}