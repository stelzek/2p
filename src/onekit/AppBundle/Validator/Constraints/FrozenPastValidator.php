<?php
namespace onekit\AppBundle\Validator\Constraints;


use Doctrine\ORM\EntityManager;
use onekit\AppBundle\Entity\Appointment;
use onekit\AppBundle\Entity\Input\CreateAppointment;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class FrozenPastValidator
 * @package onekit\AppBundle\Validator\Constraints
 */
class FrozenPastValidator extends ConstraintValidator
{
    /**
     * @var AuthorizationChecker
     */
    protected $checker;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    public function __construct(AuthorizationChecker $checker, RequestStack $requestStack)
    {
        $this->checker = $checker;
        $this->requestStack = $requestStack;
    }

    public function validate($value, Constraint $constraint)
    {
        if ($constraint instanceof FrozenPast && $value instanceof CreateAppointment) {
            if ($this->checker->isGranted('ROLE_SUPER_ADMIN')) {
                return;
            }
            /** @var Appointment $appointment */
            $appointment = $this->requestStack->getCurrentRequest()->attributes->get('appointment');
            if ($value->start_time->getTimestamp() < time()) {
                if (is_null($appointment) || $appointment->getStart()->getTimestamp() != $value->start_time->getTimestamp()) {
                    $this->generateViolation($constraint, 'start_time');
                }
            }
            if ($value->end_time->getTimestamp() < time()) {
                if (is_null($appointment) || $appointment->getEnd()->getTimestamp() !== $value->end_time->getTimestamp()) {
                    $this->generateViolation($constraint, 'end_time');
                }
            }
        } else {
            throw new \Exception();
        }
    }

    protected function generateViolation(FrozenPast $constraint, $property)
    {
        $this->context
            ->buildViolation($constraint->message)
            ->atPath($property)
            ->addViolation();
    }

}