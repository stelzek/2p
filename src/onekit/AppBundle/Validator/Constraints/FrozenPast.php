<?php
namespace onekit\AppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * Class FrozenPast
 * @package onekit\AppBundle\Validator\Constraints
 *
 * @Annotation
 */
class FrozenPast extends Constraint
{
    public $service = 'app.validator.frozen_past';

    /**
     * @var string
     */
    public $message = 'Should not be in the past.';

    public function getDefaultOption()
    {
        return 'class';
    }

    public function validatedBy()
    {
        return $this->service;
    }

    public function getTargets()
    {
        return Constraint::CLASS_CONSTRAINT;
    }
}