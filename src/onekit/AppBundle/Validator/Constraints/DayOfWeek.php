<?php
namespace onekit\AppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * Class DayOfWeek
 * @package onekit\AppBundle\Validator\Constraints
 *
 * @Annotation
 */
class DayOfWeek extends Constraint
{
    public static function getList()
    {
        return [
            'monday',
            'tuesday',
            'wednesday',
            'thursday',
            'friday',
            'saturday',
            'sunday',
        ];
    }

    /**
     * @var string
     */
    public $message = 'It is not day of week.';

    /**
     * @var string
     */
    public $requirement;

    public function getDefaultOption()
    {
        return 'requirement';
    }


    public function getTargets()
    {
        return Constraint::PROPERTY_CONSTRAINT;
    }
}