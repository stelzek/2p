<?php
namespace onekit\AppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * Class AtLeastOne
 * @package onekit\AppBundle\Validator\Constraints
 *
 * @Annotation
 */
class AtLeastOne extends Constraint
{
    /**
     * @var string
     */
    public $message = 'One of this properties should not be blank: %properties%.';

    /**
     * @var string[]
     */
    public $properties;

    public function getDefaultOption()
    {
        return 'properties';
    }


    public function getTargets()
    {
        return Constraint::CLASS_CONSTRAINT;
    }
}