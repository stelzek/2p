<?php
namespace onekit\AppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\InvalidArgumentException;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class DayOfWeekValidator extends ConstraintValidator
{

    /**
     * @param \object $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof DayOfWeek) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\DayOfWeek');
        }

        if (isset($constraint->requirement) && !in_array($constraint->requirement, DayOfWeek::getList())) {
            throw new InvalidArgumentException(sprintf('`%s` is not a day of week.', $constraint->requirement));
        }
        $valid = true;
        if (!in_array($value, DayOfWeek::getList())) {
            $valid = false;
        }
        if (isset($constraint->requirement) && $constraint->requirement !== $value) {
            $valid = false;
        }
        if (!$valid) {
            $this->context
                ->buildViolation($constraint->message)
                ->addViolation();
        }
    }
}