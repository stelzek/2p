<?php
namespace onekit\AppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\InvalidArgumentException;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class AtLeastOneValidator extends ConstraintValidator
{

    /**
     * @param \object $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof AtLeastOne) {
            throw new UnexpectedTypeException($constraint, __NAMESPACE__.'\AtLeastOne');
        }
        $notBlank = false;
        foreach ($constraint->properties as $property) {
            if (!property_exists($value, $property)) {
                throw new InvalidArgumentException(sprintf('Unsupported class "%s": missing "%s" property.', get_class($value), $property));
            }
            $notBlank = $notBlank || $value->$property || $value->$property === 0 || $value->$property === '0';
        }
        if (!$notBlank) {
            foreach ($constraint->properties as $property) {
                $this->context
                    ->buildViolation($constraint->message)
                    ->atPath($property)
                    ->setParameter('%properties%', implode(', ', $constraint->properties))
                    ->addViolation();
            }
        }
    }
}