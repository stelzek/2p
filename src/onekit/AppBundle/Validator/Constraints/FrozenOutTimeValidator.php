<?php
namespace onekit\AppBundle\Validator\Constraints;


use Doctrine\ORM\EntityManager;
use onekit\AppBundle\Entity\Appointment;
use onekit\AppBundle\Entity\Doctor;
use onekit\AppBundle\Entity\Input\CreateAppointment;
use onekit\AppBundle\Manager\DoctorManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authorization\AuthorizationChecker;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class FrozenOutTimeValidator
 * @package onekit\AppBundle\Validator\Constraints
 */
class FrozenOutTimeValidator extends ConstraintValidator
{
    /**
     * @var AuthorizationChecker
     */
    protected $checker;

    /**
     * @var RequestStack
     */
    protected $requestStack;

    /**
     * @var DoctorManager
     */
    protected $doctorManager;

    public function __construct(AuthorizationChecker $checker, RequestStack $requestStack, DoctorManager $doctorManager)
    {
        $this->checker = $checker;
        $this->requestStack = $requestStack;
        $this->doctorManager = $doctorManager;
    }

    public function validate($value, Constraint $constraint)
    {
        if ($constraint instanceof FrozenOutTime && $value instanceof CreateAppointment) {
            if ($this->checker->isGranted('ROLE_SUPER_ADMIN')) {
                return;
            }
            /** @var Appointment $appointment */
            $appointment = $this->requestStack->getCurrentRequest()->attributes->get('appointment');
            /** @var Doctor $doctor */
            $doctor = $this->requestStack->getCurrentRequest()->attributes->get('doctor');
            if ($this->isOutTime(is_null($appointment) ? $doctor : $appointment->getDoctor(), $value)) {
                if (is_null($appointment) || $appointment->getStart()->getTimestamp() != $value->start_time->getTimestamp()) {
                    $this->generateViolation($constraint, 'start_time');
                }
                if (is_null($appointment) || $appointment->getEnd()->getTimestamp() != $value->end_time->getTimestamp()) {
                    $this->generateViolation($constraint, 'end_time');
                }
            }
        } else {
            throw new \Exception();
        }
    }

    protected function isOutTime(Doctor $doctor, CreateAppointment $createAppointment)
    {
        if (!$this->doctorManager->validateAppointments($doctor)) {
            return false;
        }
        $dayOfWeek = strtolower($createAppointment->start_time->format('l'));
        $times = $this->doctorManager->getTimes($doctor);
        $outOfTime = true;
        foreach($times->times as $hours) {
            if ($hours->day == $dayOfWeek) {
                $opens = new \DateTime($hours->opens);
                $closes = new \DateTime($hours->closes);
                $startTime = new \DateTime($createAppointment->start_time->format('h:i:s'));
                $endTime = new \DateTime($createAppointment->end_time->format('h:i:s'));
                if ($opens < $startTime && $opens < $endTime && $closes > $startTime && $closes > $endTime) {
                    $outOfTime = false;
                    break;
                }
            }
        }
        return $outOfTime;
    }

    protected function generateViolation(FrozenOutTime $constraint, $property)
    {
        $this->context
            ->buildViolation($constraint->message)
            ->atPath($property)
            ->addViolation();
    }

}