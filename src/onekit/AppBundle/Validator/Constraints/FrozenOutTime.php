<?php
namespace onekit\AppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * Class FrozenOutTime
 * @package onekit\AppBundle\Validator\Constraints
 *
 * @Annotation
 */
class FrozenOutTime extends Constraint
{
    public $service = 'app.validator.frozen_out_time';

    /**
     * @var string
     */
    public $message = "Out of doctor's working time.";

    public function getDefaultOption()
    {
        return 'class';
    }

    public function validatedBy()
    {
        return $this->service;
    }

    public function getTargets()
    {
        return Constraint::CLASS_CONSTRAINT;
    }
}