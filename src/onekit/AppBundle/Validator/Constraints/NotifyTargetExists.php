<?php
namespace onekit\AppBundle\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * Class NotifyTargetExists
 * @package onekit\AppBundle\Validator\Constraints
 *
 * @Annotation
 */
class NotifyTargetExists extends Constraint
{
    public $service = 'app.validator.notify_target_exists';

    /**
     * @var string
     */
    public $class;

    /**
     * @var string
     */
    public $message = 'Can not be set `true` for missing target.';

    public function getDefaultOption()
    {
        return 'class';
    }

    public function validatedBy()
    {
        return $this->service;
    }

    public function getTargets()
    {
        return Constraint::CLASS_CONSTRAINT;
    }
}