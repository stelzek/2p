<?php
namespace onekit\AppBundle\EventListener;


use Doctrine\Common\Annotations\Reader;
use Doctrine\Common\Util\ClassUtils;
use Doctrine\ORM\EntityManagerInterface;
use onekit\AppBundle\Entity\Appointment;
use onekit\AppBundle\Entity\NotificationEvent;
use onekit\AppBundle\Extractor\NotificationEventExtractor;
use onekit\AppBundle\Manager\SMSManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\FilterResponseEvent;
use Symfony\Component\HttpKernel\Event\GetResponseForControllerResultEvent;

class NotificationEventListener
{

    /**
     * @var Reader
     */
    protected $reader;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var SMSManager
     */
    protected $smsManager;

    public function __construct(Reader $reader, EntityManagerInterface $entityManager, SMSManager $smsManager)
    {
        $this->reader = $reader;
        $this->em = $entityManager;
        $this->smsManager = $smsManager;
    }

    public function onKernelController(FilterControllerEvent $event)
    {
        if (!is_array($controller = $event->getController())) {
            return;
        }
        $className = class_exists('Doctrine\Common\Util\ClassUtils') ? ClassUtils::getClass($controller[0]) : get_class($controller[0]);
        $object = new \ReflectionClass($className);
        $method = $object->getMethod($controller[1]);

        /** @var \onekit\AppBundle\Annotation\NotificationEvent $annotation */
        $annotation = $this->reader->getMethodAnnotation($method, NotificationEventExtractor::ANNOTATION_CLASS);
        if (!$annotation) {
            return;
        }
        $action = $annotation->getAction() ? $annotation->getAction() : $event->getRequest()->attributes->get('_route');
        $notificationEvent = $this->em->getRepository('AppBundle:NotificationEvent')->findOneBy(array(
            'action' => $action,
        ));
        if ($notificationEvent) {
            $event->getRequest()->attributes->set('_notification_event', $notificationEvent);
        }
    }

    public function onKernelView(GetResponseForControllerResultEvent $event)
    {
        if ($this->isNotificationEvent($event->getRequest())) {
            $result = $event->getControllerResult();
            if ($result instanceof Appointment) {
                $event->getRequest()->attributes->set('_notification_event_appointment', $result);
            }
        }
    }

    public function onKernelResponse(FilterResponseEvent $event)
    {
        if ($this->isNotificationEvent($event->getRequest())) {
            $appointment = null;
            if ($event->getRequest()->attributes->has('_notification_event_appointment')) {
                /** @var Appointment $appointment */
                $appointment = $event->getRequest()->attributes->get('_notification_event_appointment');
            } elseif ($event->getRequest()->attributes->has('appointment')) {
                /** @var Appointment $appointment */
                $appointment = $event->getRequest()->attributes->get('appointment');
            }
            if ($appointment instanceof Appointment) {
                $this->smsManager->sendByAppointment($appointment, $event->getRequest()->attributes->get('_notification_event'));
            }
        }
    }

    protected function isNotificationEvent(Request $request)
    {
        if ($request->attributes->has('_notification_event')) {
            /** @var NotificationEvent $notificationEvent */
            $notificationEvent = $request->attributes->get('_notification_event');
            if ($notificationEvent instanceof NotificationEvent) {
                return true;
            }
        }
        return false;
    }
}