<?php
namespace onekit\AppBundle\EventListener;


use Doctrine\ORM\Event\LifecycleEventArgs;
use onekit\AppBundle\Entity\Appointment;
use onekit\AppBundle\Manager\ScheduleManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AppointmentManageListener
{
    /**
     * @var ContainerInterface
     */
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof Appointment) {
            $this->updateSchedules($entity, false);
        }
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof Appointment) {
            $this->updateSchedules($entity);
        }
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof Appointment) {
            /** @var ScheduleManager $scheduleManager */
            $scheduleManager = $this->container->get('api.manager.schedule');
            $scheduleManager->deleteAppointmentList($entity);
        }
    }

    protected function updateSchedules(Appointment $appointment, $delete = true)
    {
        /** @var ScheduleManager $scheduleManager */
        $scheduleManager = $this->container->get('api.manager.schedule');
        $scheduleManager->updateAppointmentList($appointment, $delete);
    }
}