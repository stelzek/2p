<?php
namespace onekit\AppBundle\EventListener;


use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use onekit\AppBundle\Entity\Appointment;
use onekit\AppBundle\Entity\Assistant;
use onekit\AppBundle\Entity\Doctor;
use onekit\AppBundle\Entity\Patient;
use onekit\AppBundle\Entity\SystemLog;
use onekit\AppBundle\Entity\User;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class ActionListener
{
    /**
     * @var User|null
     */
    protected $user;

    protected $tokenStorage;

    /**
     * @var string
     */
    protected $ip;

    public function __construct(TokenStorage $tokenStorage, RequestStack $requestStack)
    {
        $this->tokenStorage = $tokenStorage;
        if ($request = $requestStack->getCurrentRequest()) {
            $this->ip = $request->getClientIp();
        }
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        $token = $this->tokenStorage->getToken();
        if ($token) {
            $user = $token->getUser();
            if ($user instanceof User) {
                $this->user = $user;
            }
        }
        $entity = $args->getEntity();
        if ($entity instanceof Appointment) {
            $log = new SystemLog(
                $this->user,
                $this->ip,
                SystemLog::APPOINTMENT_CREATED_ACTION,
                $entity->getId()
            );
            $args->getEntityManager()->persist($log);
        } elseif ($entity instanceof Patient) {
            $log = new SystemLog(
                $this->user,
                $this->ip,
                SystemLog::PATIENT_CREATED_ACTION,
                $entity->getId()
            );
            $args->getEntityManager()->persist($log);
        } elseif ($entity instanceof Assistant) {
            $log = new SystemLog(
                $this->user,
                $this->ip,
                SystemLog::ASSISTANT_CREATED_ACTION,
                $entity->getId()
            );
            $args->getEntityManager()->persist($log);
        } elseif ($entity instanceof Doctor) {
            $log = new SystemLog(
                $this->user,
                $this->ip,
                SystemLog::DOCTOR_CREATED_ACTION,
                $entity->getId()
            );
            $args->getEntityManager()->persist($log);
        } elseif ($entity instanceof User) {
            $log = new SystemLog(
                $this->user,
                $this->ip,
                SystemLog::USER_CREATED_ACTION,
                $entity->getId()
            );
            $args->getEntityManager()->persist($log);
        }
        $args->getEntityManager()->flush();
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $token = $this->tokenStorage->getToken();
        if ($token) {
            $user = $token->getUser();
            if ($user instanceof User) {
                $this->user = $user;
            }
        }
        $entity = $args->getEntity();
        if ($entity instanceof Appointment) {
            $log = new SystemLog(
                $this->user,
                $this->ip,
                SystemLog::APPOINTMENT_EDITED_ACTION,
                $entity->getId()
            );
            $args->getEntityManager()->persist($log);
        } elseif ($entity instanceof Patient) {
            $log = new SystemLog(
                $this->user,
                $this->ip,
                SystemLog::PATIENT_EDITED_ACTION,
                $entity->getId()
            );
            $args->getEntityManager()->persist($log);
        } elseif ($entity instanceof Assistant) {
            $log = new SystemLog(
                $this->user,
                $this->ip,
                SystemLog::ASSISTANT_EDITED_ACTION,
                $entity->getId()
            );
            $args->getEntityManager()->persist($log);
        } elseif ($entity instanceof Doctor) {
            $log = new SystemLog(
                $this->user,
                $this->ip,
                SystemLog::DOCTOR_EDITED_ACTION,
                $entity->getId()
            );
            $args->getEntityManager()->persist($log);
        } elseif ($entity instanceof User) {
            $log = new SystemLog(
                $this->user,
                $this->ip,
                SystemLog::USER_EDITED_ACTION,
                $entity->getId()
            );
            $args->getEntityManager()->persist($log);
        }
        $args->getEntityManager()->flush();
    }

    public function preRemove(LifecycleEventArgs $args)
    {
        $token = $this->tokenStorage->getToken();
        if ($token) {
            $user = $token->getUser();
            if ($user instanceof User) {
                $this->user = $user;
            }
        }
        $entity = $args->getEntity();
        if ($entity instanceof Appointment) {
            $log = new SystemLog(
                $this->user,
                $this->ip,
                SystemLog::APPOINTMENT_DELETED_ACTION,
                $entity->getId()
            );
            $args->getEntityManager()->persist($log);
        } elseif ($entity instanceof Patient) {
            $log = new SystemLog(
                $this->user,
                $this->ip,
                SystemLog::PATIENT_DELETED_ACTION,
                $entity->getId()
            );
            $args->getEntityManager()->persist($log);
        } elseif ($entity instanceof Assistant) {
            $log = new SystemLog(
                $this->user,
                $this->ip,
                SystemLog::ASSISTANT_DELETED_ACTION,
                $entity->getId()
            );
            $args->getEntityManager()->persist($log);
        } elseif ($entity instanceof Doctor) {
            $log = new SystemLog(
                $this->user,
                $this->ip,
                SystemLog::DOCTOR_DELETED_ACTION,
                $entity->getId()
            );
            $args->getEntityManager()->persist($log);
        } elseif ($entity instanceof User) {
            $log = new SystemLog(
                $this->user,
                $this->ip,
                SystemLog::USER_DELETED_ACTION,
                $entity->getId()
            );
            $args->getEntityManager()->persist($log);
        }
        $args->getEntityManager()->flush();
    }

}