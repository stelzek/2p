<?php
namespace onekit\AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use onekit\AppBundle\Validator\Constraints as AppAssert;
use JMS\Serializer\Annotation as Serial;

use onekit\AppBundle\Entity\Behavior\SoftDeletable;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass = "onekit\AppBundle\Repository\DoctorRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name = "doctor", indexes = {
 *      @ORM\Index(name="expiry_index", columns={"expired_in"}),
 * })
 * @AppAssert\AtLeastOne({"address", "phone"})
 * @Gedmo\SoftDeleteable
 */
class Doctor
{
    use SoftDeletable;

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serial\Groups({"default", "doctor"})
     */
    protected $id;

    /**
     * @var \onekit\AppBundle\Entity\User
     *
     * @ORM\OneToOne(targetEntity="onekit\AppBundle\Entity\User", inversedBy="doctor")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", nullable=false)
     * @Serial\MaxDepth(1)
     * @Serial\Groups({"doctor"})
     */
    protected $account;

    /**
     * @var string $title
     * @Assert\NotBlank()
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     * @Serial\Groups({"default", "doctor"})
     */
    protected $title;

    /**
     * @var string $address
     * @ORM\Column(name="address", type="text", nullable=true)
     */
    protected $address;

    /**
     * @var string $times
     * @ORM\Column(name="times", type="text", nullable=true)
     */
    protected $times;

    /**
     * @var string $email
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     * @Assert\Email()
     * @Serial\Groups({"doctor_contact"})
     */
    protected $email;

    /**
     * @var string $phone
     * @ORM\Column(name="phone", type="string", length=32, nullable=true)
     * @Serial\Groups({"doctor_contact"})
     */
    protected $phone;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Patient", mappedBy="doctor")
     */
    protected $patients;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Appointment", mappedBy="doctor")
     */
    protected $appointments;

    /**
     * @var $notifications \Doctrine\Common\Collections\ArrayCollection
     * @ORM\OneToMany(targetEntity="onekit\AppBundle\Entity\Notification", mappedBy="doctor")
     * @ORM\OrderBy({"created"="desc"})
     */
    protected $notifications;

    /**
     * @var $smsMessageTemplates \Doctrine\Common\Collections\ArrayCollection
     * @ORM\OneToMany(targetEntity="onekit\AppBundle\Entity\SMSMessageTemplate", mappedBy="doctor")
     */
    protected $smsMessageTemplates;

    /**
     * @var $schedules \Doctrine\Common\Collections\ArrayCollection
     * @ORM\OneToMany(targetEntity="onekit\AppBundle\Entity\Schedule", mappedBy="doctor")
     * @Serial\MaxDepth(2)
     */
    protected $schedules;

    /**
     * @var \DateTime $expiredIn
     * @ORM\Column(name="expired_in", type="datetime", nullable=true)
     */
    protected $expiredIn;

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->getAccount()->getLanguage();
    }

    public function isExpired()
    {
        return !is_null($this->expiredIn) && $this->expiredIn < new \DateTime();
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->appointments = new ArrayCollection();
        $this->notifications = new ArrayCollection();
        $this->smsMessageTemplates = new ArrayCollection();
        $this->schedules = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set expiredIn
     *
     * @param \DateTime $expiredIn
     *
     * @return Doctor
     */
    public function setExpiredIn($expiredIn)
    {
        $this->expiredIn = $expiredIn;

        return $this;
    }

    /**
     * Get expiredIn
     *
     * @return \DateTime
     */
    public function getExpiredIn()
    {
        return $this->expiredIn;
    }

    /**
     * Set account
     *
     * @param User $account
     *
     * @return Doctor
     */
    public function setAccount(User $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return User
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Add appointment
     *
     * @param Appointment $appointment
     *
     * @return Doctor
     */
    public function addAppointment(Appointment $appointment)
    {
        $this->appointments[] = $appointment;
    
        return $this;
    }

    /**
     * Remove appointment
     *
     * @param Appointment $appointment
     */
    public function removeAppointment(Appointment $appointment)
    {
        $this->appointments->removeElement($appointment);
    }

    /**
     * Get appointments
     *
     * @return ArrayCollection
     */
    public function getAppointments()
    {
        return $this->appointments;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Doctor
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set times
     *
     * @param string $times
     *
     * @return Doctor
     */
    public function setTimes($times)
    {
        $this->times = $times;

        return $this;
    }

    /**
     * Get times
     *
     * @return string
     */
    public function getTimes()
    {
        return $this->times;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Doctor
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Doctor
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Doctor
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Add patient
     *
     * @param \onekit\AppBundle\Entity\Patient $patient
     *
     * @return Doctor
     */
    public function addPatient(\onekit\AppBundle\Entity\Patient $patient)
    {
        $this->patients[] = $patient;

        return $this;
    }

    /**
     * Remove patient
     *
     * @param \onekit\AppBundle\Entity\Patient $patient
     */
    public function removePatient(\onekit\AppBundle\Entity\Patient $patient)
    {
        $this->patients->removeElement($patient);
    }

    /**
     * Get patients
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPatients()
    {
        return $this->patients;
    }

    /**
     * Add notification
     *
     * @param \onekit\AppBundle\Entity\Notification $notification
     *
     * @return Doctor
     */
    public function addNotification(\onekit\AppBundle\Entity\Notification $notification)
    {
        $this->notifications[] = $notification;

        return $this;
    }

    /**
     * Remove notification
     *
     * @param \onekit\AppBundle\Entity\Notification $notification
     */
    public function removeNotification(\onekit\AppBundle\Entity\Notification $notification)
    {
        $this->notifications->removeElement($notification);
    }

    /**
     * Get notifications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotifications()
    {
        return $this->notifications;
    }

    /**
     * Add smsMessageTemplate
     *
     * @param \onekit\AppBundle\Entity\SMSMessageTemplate $smsMessageTemplate
     *
     * @return Doctor
     */
    public function addSmsMessageTemplate(\onekit\AppBundle\Entity\SMSMessageTemplate $smsMessageTemplate)
    {
        $this->smsMessageTemplates[] = $smsMessageTemplate;

        return $this;
    }

    /**
     * Remove smsMessageTemplates
     *
     * @param \onekit\AppBundle\Entity\SMSMessageTemplate $smsMessageTemplates
     */
    public function removeSMSMessageTemplate(\onekit\AppBundle\Entity\SMSMessageTemplate $smsMessageTemplates)
    {
        $this->smsMessageTemplates->removeElement($smsMessageTemplates);
    }

    /**
     * Get smsMessageTemplates
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSMSMessageTemplates()
    {
        return $this->smsMessageTemplates;
    }

    /**
     * Add Schedule
     *
     * @param \onekit\AppBundle\Entity\Schedule $schedule
     *
     * @return Doctor
     */
    public function addSchedule(\onekit\AppBundle\Entity\Schedule $schedule)
    {
        $this->schedules[] = $schedule;

        return $this;
    }

    /**
     * Remove Schedule
     *
     * @param \onekit\AppBundle\Entity\Schedule $schedule
     */
    public function removeSchedule(\onekit\AppBundle\Entity\Schedule $schedule)
    {
        $this->schedules->removeElement($schedule);
    }

    /**
     * Get schedules
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSchedules()
    {
        return $this->schedules;
    }

}
