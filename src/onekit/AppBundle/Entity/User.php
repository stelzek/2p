<?php
namespace onekit\AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serial;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\Validator\Constraints as Assert;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;

use Symfony\Component\HttpFoundation\File\File;
use Doctrine\Common\Collections\ArrayCollection;


/**
 * @ORM\Entity(repositoryClass="onekit\AppBundle\Repository\UserRepository")
 * @DoctrineAssert\UniqueEntity("email")
 * @DoctrineAssert\UniqueEntity("username")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(
 *      name = "patient_messenger_user",
 *      indexes = {
 *          @ORM\Index(name="user_enabled_idx", columns={"enabled"}),
 *          @ORM\Index(name="username_idx", columns={"username"})
 *      }
 * )
 * @Gedmo\SoftDeleteable(fieldName="expiresAt", timeAware=false)
 */
class User extends BaseUser
{
    const POSITION_ADMIN = 'Administrator';
    const POSITION_DOCTOR = 'Doctor';
    const POSITION_ASSIST = 'Assistant';
    const POSITION_GUEST = 'Guest';

    const ROLE_DOCTOR = 'ROLE_DOCTOR';
    const ROLE_ASSIST = 'ROLE_ASSIST';

    const LANG_EN = 'en';
    const LANG_DE = 'de';


    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serial\Groups({"default", "doctor", "profile"})
     */
    protected $id;

    /**
     * @var string $language
     * @ORM\Column(name="language", type="string", length=2)
     *
     * @Assert\Choice(callback = "getLanguages")
     * @Assert\NotNull()
     * @Serial\Groups({"user", "profile"})
     */
    protected $language = self::LANG_DE;

    /**
     * @var \DateTime $created
     * @ORM\Column(name="created", type="datetime", nullable=false)
     * @Serial\Groups({"user"})
     */
    protected $created;

    /**
     * @ORM\OneToOne(targetEntity="onekit\AppBundle\Entity\Doctor", mappedBy="account")
     * @Serial\MaxDepth(1)
     * @Serial\Groups({"user"})
     */
    protected $doctor;

    /**
     * @ORM\OneToOne(targetEntity="onekit\AppBundle\Entity\Assistant", mappedBy="account")
     * @Serial\MaxDepth(1)
     * @Serial\Groups({"user"})
     */
    protected $assistant;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="onekit\AppBundle\Entity\SystemLog", mappedBy="user", cascade={"all"})
     */
    protected $systemLogs;

    /**
     * @Vich\UploadableField(mapping="user_picture", fileNameProperty="imagePath")
     *
     * @var File
     */
    public $image;

    /**
     * @var string
     *
     * @var string $imagePath
     * @ORM\Column(name="image_path", type="string", length=64, nullable=true)
     */
    protected $imagePath;

    /**
     * @var string
     *
     * @var string $imagePath
     * @ORM\Column(name="image_url", type="string", length=255, nullable=true)
     * @Serial\SerializedName("image")
     * @Serial\Groups({"user_picture"})
     */
    protected $imageUrl;

    /**
     * @Serial\VirtualProperty()
     * @Serial\SerializedName("username")
     * @Serial\Groups({"default", "user"})
     * @return string
     */
    public function getVirtualUsername()
    {
        return $this->getUsername();
    }

    /**
     * @Serial\VirtualProperty()
     * @Serial\SerializedName("email")
     * @Serial\Groups({"user"})
     * @return string
     */
    public function getVirtualEmail()
    {
        return $this->getEmail();
    }

    /**
     * @Serial\VirtualProperty()
     * @Serial\SerializedName("roles")
     * @Serial\Groups({"user", "profile"})
     * @return array
     */
    public function getRoles()
    {
        $roles = parent::getRoles();
        if ($this->isDoctor()) {
            $roles[] = self::ROLE_DOCTOR;
        }
        if ($this->isAssistant()) {
            $roles[] = self::ROLE_ASSIST;
        }
        return $roles;
    }

    /**
     * @Serial\VirtualProperty()
     * @Serial\SerializedName("doctor")
     * @Serial\Groups({"profile"})
     * @return string
     */
    public function getProfileDoctor()
    {
        $doctor = $this->getDoctor();
        if (!$doctor) {
            $assistant = $this->getAssistant();
            if ($assistant) {
                $doctor = $assistant->getDoctor();
            }
        }
        return $doctor ? $doctor->getId() : null;
    }

    /**
     * @return string[]
     */
    public static function getLanguages()
    {
        return array(
            self::LANG_EN,
            self::LANG_DE,
        );
    }

    /**
     * @return bool
     */
    public function isDoctor()
    {
        return $this->getDoctor() instanceof Doctor;
    }

    /**
     * @return bool
     */
    public function isAssistant()
    {
        return $this->getAssistant() instanceof Assistant;
    }

    /**
     * @Serial\VirtualProperty()
     * @Serial\SerializedName("full_name")
     * @Serial\Groups({"profile"})
     * @return string
     */
    public function getTitle()
    {
        if ($this->isSuperAdmin()) {
            return $this->getUsername();
        }
        if ($this->isDoctor()) {
            return $this->getDoctor()->getTitle();
        }
        if ($this->isAssistant()) {
            return $this->getAssistant()->getTitle();
        }
        return $this->getUsername();
    }

    /**
     * @Serial\VirtualProperty()
     * @Serial\SerializedName("position")
     * @Serial\Groups({"profile"})
     * @return string
     */
    public function getPosition()
    {
        return $this->isSuperAdmin() ? self::POSITION_ADMIN : ($this->isDoctor() ? self::POSITION_DOCTOR : ($this->isAssistant() ? self::POSITION_ASSIST: self::POSITION_GUEST));
    }

    /**
     * @return bool
     */
    public function isDeleted()
    {
        return $this->isExpired();
    }

    public function __construct()
    {
        parent::__construct();
        $this->systemLogs = new ArrayCollection();
    }


    /**
     * Set language
     *
     * @param string $language
     *
     * @return User
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }


    /**
     * @ORM\PrePersist
     */
    public function setCreated()
    {
        $this->created = new \DateTime();
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Add systemLog
     *
     * @param \onekit\AppBundle\Entity\SystemLog $systemLog
     *
     * @return User
     */
    public function addSystemLog(\onekit\AppBundle\Entity\SystemLog $systemLog)
    {
        $this->systemLogs[] = $systemLog;

        return $this;
    }

    /**
     * Remove systemLog
     *
     * @param \onekit\AppBundle\Entity\SystemLog $systemLog
     */
    public function removeSystemLogsSent(\onekit\AppBundle\Entity\SystemLog $systemLog)
    {
        $this->systemLogs->removeElement($systemLog);
    }

    /**
     * Get systemLogsSent
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSystemLogsSent()
    {
        return $this->systemLogs;
    }

    /**
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * @param string $imagePath
     * @return $this
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * @return string
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * @param string $imageUrl
     * @return $this
     */
    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;

        return $this;
    }

    /**
     * Set doctor
     *
     * @param \onekit\AppBundle\Entity\Doctor $doctor
     *
     * @return User
     */
    public function setDoctor(\onekit\AppBundle\Entity\Doctor $doctor = null)
    {
        $this->doctor = $doctor;

        return $this;
    }

    /**
     * Get doctor
     *
     * @return \onekit\AppBundle\Entity\Doctor|null
     */
    public function getDoctor()
    {
        return $this->doctor;
    }


    /**
     * Set assistant
     *
     * @param \onekit\AppBundle\Entity\Assistant $assistant
     *
     * @return User
     */
    public function setAssistant(\onekit\AppBundle\Entity\Assistant $assistant = null)
    {
        $this->assistant = $assistant;

        return $this;
    }

    /**
     * Get assistant
     *
     * @return \onekit\AppBundle\Entity\Assistant
     */
    public function getAssistant()
    {
        return $this->assistant;
    }
}
