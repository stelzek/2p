<?php

namespace onekit\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use onekit\AppBundle\Entity\Doctor;

/**
 * NotificationEvent
 *
 * @ORM\Table(name="notification_event")
 * @ORM\Entity
 */
class NotificationEvent
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="action", type="string", length=255)
     */
    protected $action;

    /**
     * @param string|bool $action
     */
    public function __construct($action = false)
    {
        if (is_string($action) && trim($action)) {
            $this->setAction(trim($action));
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set action
     *
     * @param string $action
     *
     * @return NotificationEvent
     */
    public function setAction($action)
    {
        $this->action = $action;
    
        return $this;
    }

    /**
     * Get action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }
}
