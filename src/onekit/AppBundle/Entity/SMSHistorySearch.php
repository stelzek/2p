<?php
namespace onekit\AppBundle\Entity;

class SMSHistorySearch
{
    protected $startRange;
    protected $endRange;

    /**
     * @return mixed
     */
    public function getStartRange()
    {
        return $this->startRange;
    }

    /**
     * @param mixed $startRange
     */
    public function setStartRange($startRange)
    {
        $this->startRange = $startRange;
    }

    /**
     * @return mixed
     */
    public function getEndRange()
    {
        return $this->endRange;
    }

    /**
     * @param mixed $endRange
     */
    public function setEndRange($endRange)
    {
        $this->endRange = $endRange;
    }


}