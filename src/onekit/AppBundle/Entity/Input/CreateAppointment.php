<?php
namespace onekit\AppBundle\Entity\Input;


use JMS\Serializer\Annotation as Serial;
use Symfony\Component\Validator\Constraints as Assert;
use onekit\AppBundle\Validator\Constraints as AppAssert;

/**
 * Class CreateAppointment
 * @package onekit\AppBundle\Rest\Input
 *
 * @AppAssert\FrozenPast
 * @AppAssert\FrozenOutTime
 * @AppAssert\NotifyTargetExists
 */
class CreateAppointment
{
    /**
     * @var \DateTime $startTime
     *
     * @Assert\NotNull()
     * @Serial\Type("DateTime<'Y-m-d H:i:s'>")
     */
    public $start_time;

    /**
     * @var \DateTime $startTime
     *
     * @Assert\NotNull()
     * @Serial\Type("DateTime<'Y-m-d H:i:s'>")
     */
    public $end_time;

    /**
     * @var integer $notify_email
     *
     * @Serial\Type("boolean")
     */
    public $notify_email;

    /**
     * @var integer $notify_sms
     *
     * @Serial\Type("boolean")
     */
    public $notify_sms;
}