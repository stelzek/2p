<?php
namespace onekit\AppBundle\Entity\Input;


use JMS\Serializer\Annotation as Serial;
use Symfony\Component\Validator\Constraints as Assert;
use onekit\AppBundle\Validator\Constraints as AppAssert;

class DoctorHours
{
    /**
     * @var string $company
     *
     * @Assert\NotNull()
     * @AppAssert\DayOfWeek()
     * @Serial\Type("string")
     */
    public $day;

    /**
     * @var string $company
     *
     * @Assert\NotNull()
     * @Serial\Type("DateTime<'H:i'>")
     */
    public $opens;

    /**
     * @var string $company
     *
     * @Assert\NotNull()
     * @Serial\Type("DateTime<'H:i'>")
     */
    public $closes;
}