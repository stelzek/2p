<?php
namespace onekit\AppBundle\Entity\Input;


use JMS\Serializer\Annotation as Serial;
use Symfony\Component\Validator\Constraints as Assert;

class UserLanguage
{
    /**
     * @var string $language
     *
     * @Assert\NotNull
     * @Assert\Choice(callback = {"\onekit\AppBundle\Entity\User", "getLanguages"})
     * @Serial\Type("string")
     */
    public $language;
}