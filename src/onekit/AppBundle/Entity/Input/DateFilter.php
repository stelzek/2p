<?php
namespace onekit\AppBundle\Entity\Input;


use JMS\Serializer\Annotation as Serial;
use Symfony\Component\Validator\Constraints as Assert;
use onekit\AppBundle\Validator\Constraints as AppAssert;

/**
 * Class DateFilter
 * @package onekit\AppBundle\Rest\Input
 *
 */
class DateFilter
{
    /**
     * @var \DateTime $startTime
     *
     * @Assert\NotNull()
     * @Serial\Type("DateTime<'Y-m-d H:i:s'>")
     */
    public $start_time;

    /**
     * @var \DateTime $startTime
     *
     * @Assert\NotNull()
     * @Serial\Type("DateTime<'Y-m-d H:i:s'>")
     */
    public $end_time;
}