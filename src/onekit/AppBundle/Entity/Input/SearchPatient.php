<?php
namespace onekit\AppBundle\Entity\Input;


use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class SearchPatient
 * @package onekit\AppBundle\Entity\Api
 */
class SearchPatient
{
    /**
     * @JMS\Type("string")
     *
     * @var string
     * @Assert\NotNull
     */
    public $keyword;

    public $first_name;
    public $last_name;
    public $phone;
    public $email;
}