<?php
namespace onekit\AppBundle\Entity\Input;


use JMS\Serializer\Annotation as Serial;
use Symfony\Component\Validator\Constraints as Assert;
use onekit\AppBundle\Validator\Constraints as AppAssert;

/**
 * Class CreateAssistant
 * @package onekit\AppBundle\Rest\Input
 *
 */
class CreateAssistant
{
    /**
     * @var string $title
     *
     * @Assert\NotNull()
     * @Serial\Type("string")
     */
    public $title;

    /**
     * @var bool $edit
     *
     * @Assert\NotNull()
     * @Serial\Type("boolean")
     */
    public $edit;

    /**
     * @var bool $manage_patients
     *
     * @Assert\NotNull()
     * @Serial\Type("boolean")
     */
    public $manage_patients;

    /**
     * @var bool $manage_appointments
     *
     * @Assert\NotNull()
     * @Serial\Type("boolean")
     */
    public $manage_appointments;

    /**
     * @var bool $check_balance
     *
     * @Assert\NotNull()
     * @Serial\Type("boolean")
     */
    public $check_balance;

    /**
     * @var bool $make_payment
     *
     * @Assert\NotNull()
     * @Serial\Type("boolean")
     */
    public $make_payment;

    /**
     * @var integer $account_id
     *
     * @AppAssert\EntityExists("onekit\AppBundle\Entity\User", nullable=true)
     * @Serial\Type("integer")
     */
    public $account_id;
}