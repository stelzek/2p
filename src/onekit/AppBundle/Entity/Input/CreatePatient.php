<?php
namespace onekit\AppBundle\Entity\Input;


use JMS\Serializer\Annotation as Serial;
use onekit\AppBundle\Entity\User;
use Symfony\Component\Validator\Constraints as Assert;
use onekit\AppBundle\Validator\Constraints as AppAssert;

/**
 * Class PatientCreate
 * @package onekit\AppBundle\Rest\Input
 *
 * @AppAssert\AtLeastOne({"email", "phone"})
 * @AppAssert\NotifyTargetExists
 */
class CreatePatient
{
    /**
     * @var string $title
     *
     * @Serial\Type("string")
     */
    public $title;

    /**
     * @var string $first_name
     *
     * @Assert\NotBlank()
     * @Serial\Type("string")
     */
    public $first_name;

    /**
     * @var string $last_name
     *
     * @Assert\NotBlank()
     * @Serial\Type("string")
     */
    public $last_name;

    /**
     * @var string $insuranceType
     *
     * @Serial\Type("string")
     */
    public $insuranceType;

    /**
     * @var string $insuranceCompany
     *
     * @Serial\Type("string")
     */
    public $insuranceCompany;

    /**
     * @var integer $gender
     *
     * @Assert\NotNull()
     * @Assert\Choice(choices = {0, 1})
     * @Serial\Type("integer")
     */
    public $gender;

    /**
     * @var string $email
     *
     * @Assert\Email()
     * @Serial\Type("string")
     */
    public $email;

    /**
     * @var integer $notify_email
     *
     * @Serial\Type("boolean")
     */
    public $notify_email;

    /**
     * @var string $phone
     *
     * @Assert\Length(min=12)
     * @Serial\Type("string")
     */
    public $phone;

    /**
     * @var integer $notify_sms
     *
     * @Serial\Type("boolean")
     */
    public $notify_sms;

    /**
     * @var integer $account_id
     *
     * @AppAssert\EntityExists("onekit\AppBundle\Entity\User", nullable=true)
     * @Serial\Type("integer")
     */
    public $account_id;

    /**
     * @var string $language
     *
     * @Assert\Choice(callback = {"\onekit\AppBundle\Entity\User", "getLanguages"})
     * @Serial\Type("string")
     */
    public $language;
}