<?php
namespace onekit\AppBundle\Entity\Input;


use JMS\Serializer\Annotation as Serial;
use Symfony\Component\Validator\Constraints as Assert;
use onekit\AppBundle\Validator\Constraints as AppAssert;

/**
 * Class DoctorContact
 * @package onekit\AppBundle\Entity\Input
 *
 * @AppAssert\AtLeastOne({"email", "phone"})
 */
class DoctorContact
{
    /**
     * @var string $email
     *
     * @Assert\Email()
     * @Assert\Length(min=2, max=255)
     * @Serial\Type("string")
     */
    public $email;

    /**
     * @var string $phone
     *
     * @Assert\Length(min=12)
     * @Serial\Type("string")
     */
    public $phone;
}