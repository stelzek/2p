<?php
namespace onekit\AppBundle\Entity\Input;


use JMS\Serializer\Annotation as Serial;
use Symfony\Component\Validator\Constraints as Assert;
use onekit\AppBundle\Validator\Constraints as AppAssert;

/**
 * Class DoctorCreate
 * @package onekit\AppBundle\Rest\Input
 */
class CreateDoctor
{
    /**
     * @var string $title
     *
     * @Assert\NotNull()
     * @Serial\Type("string")
     */
    public $title;

    /**
     * @var string $phone
     *
     * @Assert\NotNull()
     * @Serial\Type("string")
     */
    public $phone;

    /**
     * @var integer $account_id
     *
     * @Assert\NotBlank()
     * @AppAssert\EntityExists("onekit\AppBundle\Entity\User")
     * @Serial\Type("integer")
     */
    public $account_id;
}