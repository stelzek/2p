<?php
namespace onekit\AppBundle\Entity\Input;


use JMS\Serializer\Annotation as Serial;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;
use Symfony\Component\Security\Core\Validator\Constraints as SecurityAssert;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

/**
 * Class UserCredentials
 * @package onekit\AppBundle\Entity\Input
 *
 * @DoctrineAssert\UniqueEntity("username", service="app.validator.unique_user")
 */
class UserCredentials
{
    /**
     * @var string
     *
     * @Assert\Length(min=2, max=255)
     * @Serial\Type("string")
     */
    public $username;

    /**
     * @var string $oldPassword
     *
     * @SecurityAssert\UserPassword
     * @Serial\Type("string")
     */
    public $old_password;

    /**
     * @var string
     *
     * @Assert\Length(min=6, max=4096)
     * @Serial\Type("string")
     */
    public $password;

    /**
     * @var string
     *
     * @Serial\Type("string")
     */
    public $password_repeat;

    /**
     * @Assert\Callback
     *
     * @param ExecutionContextInterface $context
     */
    public function validate(ExecutionContextInterface $context)
    {
        if ($this->password) {
            if ($this->password !== $this->password_repeat) {
                $context->buildViolation('Password mismatch.')
                    ->atPath('password_repeat')
                    ->addViolation();
            }
        }
    }
}