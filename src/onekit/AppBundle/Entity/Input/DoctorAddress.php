<?php
namespace onekit\AppBundle\Entity\Input;


use JMS\Serializer\Annotation as Serial;
use Symfony\Component\Validator\Constraints as Assert;

class DoctorAddress
{
    /**
     * @var string $company
     *
     * @Assert\NotNull()
     * @Serial\Type("string")
     */
    public $company;

    /**
     * @var string $street
     *
     * @Assert\NotNull()
     * @Serial\Type("string")
     */
    public $street;

    /**
     * @var string $building
     *
     * @Assert\NotNull()
     * @Serial\Type("string")
     */
    public $building;

    /**
     * @var string $office
     *
     * @Assert\NotNull()
     * @Serial\Type("string")
     */
    public $office;

    /**
     * @var integer $zip
     *
     * @Assert\NotNull()
     * @Serial\Type("integer")
     */
    public $zip;

    /**
     * @var string $city
     *
     * @Assert\NotNull()
     * @Serial\Type("string")
     */
    public $city;

    /**
     * @var string $country
     *
     * @Assert\NotNull()
     * @Serial\Type("string")
     */
    public $country;

    /**
     * @var boolean $signature
     *
     * @Assert\NotNull()
     * @Serial\Type("boolean")
     */
    public $signature;
}