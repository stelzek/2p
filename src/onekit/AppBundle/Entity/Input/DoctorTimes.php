<?php
namespace onekit\AppBundle\Entity\Input;


use JMS\Serializer\Annotation as Serial;
use Symfony\Component\Validator\Constraints as Assert;
use onekit\AppBundle\Validator\Constraints as AppAssert;

class DoctorTimes
{
    /**
     * @var DoctorHours[] $times
     *
     * @Assert\Valid()
     * @AppAssert\UniqueId("day")
     * @Serial\Type("array<onekit\AppBundle\Entity\Input\DoctorHours>")
     */
    public $times;

    /**
     * @var boolean $validate
     *
     * @Assert\NotNull()
     * @Serial\Type("boolean")
     */
    public $validate;
}