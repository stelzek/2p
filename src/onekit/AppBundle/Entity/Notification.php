<?php

namespace onekit\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints\DateTime;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Session
 *
 * @ORM\Table(name="notification")
 * @ORM\Entity(repositoryClass="onekit\AppBundle\Repository\NotificationRepository")
 */
class Notification
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="onekit\AppBundle\Entity\Appointment", inversedBy="notifications", cascade={"persist"})
     * @ORM\JoinColumn(name="appointment_id", referencedColumnName="id")
     */
    protected $appointment;

    /**
     * @ORM\ManyToOne(targetEntity="onekit\AppBundle\Entity\Doctor", inversedBy="notifications", cascade={"persist"})
     * @ORM\JoinColumn(name="doctor_id", referencedColumnName="id", nullable=false)
     */
    protected $doctor;

    /**
     * @var \onekit\AppBundle\Entity\Patient
     * @ORM\ManyToOne(targetEntity="onekit\AppBundle\Entity\Patient", inversedBy="notifications", cascade={"persist"})
     * @ORM\JoinColumn(name="patient_id", referencedColumnName="id", nullable=true)
     */
    protected $patient;

    /**
     * @var datetime $created
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    protected $created;


    /**
     * @var string $costIn
     * @ORM\Column(name="cost_in", nullable=true)
     */
    protected $costIn;

    /**
     * @var string $costOut
     * @ORM\Column(name="cost_out", nullable=true)
     */
    protected $costOut;

    /**
     * @var string $smsStatus
     * @ORM\Column(name="sms_status", nullable=true)
     */
    protected $smsStatus;

    /**
     * @var string $smsStatus
     * @ORM\Column(name="sms_text", nullable=true)
     */
    protected $smsText;

    /**
     * @var string $smsType
     * @ORM\Column(name="sms_type", nullable=true)
     */
    protected $smsType;

    /**
     * @var string $action
     * @ORM\Column(name="action", nullable=true)
     */
    protected $action;

    /**
     * @var string $ip
     * @ORM\Column(name="ip", nullable=true)
     */
    protected $ip;


    /**
     * @var boolean $enabled
     * @ORM\Column(name="enabled", type="boolean", length=1)
     */
    protected $enabled = true;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return $this
     */
    public function setCreated($created)
    {
        $this->created = $created;
    
        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set action
     *
     * @param string $action
     *
     * @return $this
     */
    public function setAction($action)
    {
        $this->action = $action;
    
        return $this;
    }

    /**
     * Get action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return $this
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
    
        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set enabled
     *
     * @param boolean $enabled
     *
     * @return $this
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
    
        return $this;
    }

    /**
     * Get enabled
     *
     * @return boolean
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * @return string
     */
    public function getCostIn()
    {
        return $this->costIn;
    }

    /**
     * @param string $costIn
     */
    public function setCostIn($costIn)
    {
        $this->costIn = $costIn;
    }

    /**
     * @return string
     */
    public function getCostOut()
    {
        return $this->costOut;
    }

    /**
     * @param string $costOut
     */
    public function setCostOut($costOut)
    {
        $this->costOut = $costOut;
    }

    /**
     * @return string
     */
    public function getSmsStatus()
    {
        return $this->smsStatus;
    }

    /**
     * @param string $smsStatus
     */
    public function setSmsStatus($smsStatus)
    {
        $this->smsStatus = $smsStatus;
    }

    /**
     * @return string
     */
    public function getSmsText()
    {
        return $this->smsText;
    }

    /**
     * @param string $smsText
     */
    public function setSmsText($smsText)
    {
        $this->smsText = $smsText;
    }

    /**
     * @return string
     */
    public function getSmsType()
    {
        return $this->smsType;
    }

    /**
     * @param string $smsType
     */
    public function setSmsType($smsType)
    {
        $this->smsType = $smsType;
    }

    /**
     * Set appointment
     *
     * @param \onekit\AppBundle\Entity\Appointment $appointment
     *
     * @return Notification
     */
    public function setAppointment(\onekit\AppBundle\Entity\Appointment $appointment = null)
    {
        $this->appointment = $appointment;

        return $this;
    }

    /**
     * Get appointment
     *
     * @return \onekit\AppBundle\Entity\Appointment
     */
    public function getAppointment()
    {
        return $this->appointment;
    }

    /**
     * Set doctor
     *
     * @param \onekit\AppBundle\Entity\Doctor $doctor
     *
     * @return Notification
     */
    public function setDoctor(\onekit\AppBundle\Entity\Doctor $doctor)
    {
        $this->doctor = $doctor;

        return $this;
    }

    /**
     * Get doctor
     *
     * @return \onekit\AppBundle\Entity\Doctor
     */
    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * Set patient
     *
     * @param \onekit\AppBundle\Entity\Patient $patient
     *
     * @return Notification
     */
    public function setPatient(\onekit\AppBundle\Entity\Patient $patient = null)
    {
        $this->patient = $patient;

        return $this;
    }

    /**
     * Get patient
     *
     * @return \onekit\AppBundle\Entity\Patient
     */
    public function getPatient()
    {
        return $this->patient;
    }
}
