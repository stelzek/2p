<?php
namespace onekit\AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as Serial;

use onekit\AppBundle\Entity\Behavior\SoftDeletable;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Appointment
 *
 * @ORM\Table(name="appointment")
 * @ORM\Entity(repositoryClass="onekit\AppBundle\Repository\AppointmentRepository")
 * @Gedmo\SoftDeleteable
 */
class Appointment
{
    use SoftDeletable;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serial\Groups({"default", "appointment"})
     */
    protected $id;

    /**
     * @var Doctor
     * @ORM\ManyToOne(targetEntity="onekit\AppBundle\Entity\Doctor", inversedBy="appointments")
     * @ORM\JoinColumn(name="doctor_id", referencedColumnName="id")
     * @Serial\SerializedName("doctor")
     * @Serial\MaxDepth(1)
     * @Serial\Groups({"appointment", "appointment_list"})
     */
    protected $doctor;

    /**
     * @var Patient
     * @ORM\ManyToOne(targetEntity="onekit\AppBundle\Entity\Patient", inversedBy="appointments")
     * @ORM\JoinColumn(name="patient_id", referencedColumnName="id")
     * @Serial\SerializedName("patient")
     * @Serial\MaxDepth(1)
     * @Serial\Groups({"appointment", "appointment_list", "doctor_appointment_list"})
     */
    protected $patient;

    /**
     * @var $notifications \Doctrine\Common\Collections\ArrayCollection
     * @ORM\OneToMany(targetEntity="onekit\AppBundle\Entity\Notification", mappedBy="appointment", cascade={"all"})
     */
    protected $notifications;

    /**
     * @var $schedules \Doctrine\Common\Collections\ArrayCollection
     * @ORM\OneToMany(targetEntity="onekit\AppBundle\Entity\Schedule", mappedBy="appointment", cascade={"persist"})
     */
    protected $schedules;

    /**
     * @var \DateTime $start
     *
     * @ORM\Column(name="start_time", type="datetime", nullable=false)
     * @Assert\NotBlank()
     * @Assert\DateTime()
     * @Serial\SerializedName("start_time")
     * @Serial\Groups({"default", "appointment"})
     * @Serial\Type("DateTime")
     */
    protected $start;

    /**
     * @var \DateTime $end
     *
     * @ORM\Column(name="end_time", type="datetime", nullable=false)
     * @Serial\SerializedName("end_time")
     * @Serial\Groups({"default", "appointment"})
     * @Serial\Type("DateTime")
     */
    protected $end;

    /**
     * @var boolean
     *
     * @ORM\Column(name="notify_sms", type="boolean", nullable=false)
     * @Serial\SerializedName("notify_sms")
     * @Serial\Groups({"appointment", "appointment_notify"})
     * @Serial\Type("boolean")
     */
    protected $notifySMS;

    /**
     * @var boolean
     *
     * @ORM\Column(name="notify_email", type="boolean", nullable=false)
     * @Serial\SerializedName("notify_email")
     * @Serial\Groups({"appointment", "appointment_notify"})
     * @Serial\Type("boolean")
     */
    protected $notifyEmail;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->notifications = new ArrayCollection();
        $this->schedules = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Doctor
     *
     * @param Doctor $doctor
     *
     * @return Appointment
     */
    public function setDoctor(Doctor $doctor)
    {
        $this->doctor = $doctor;
    
        return $this;
    }

    /**
     * Get Doctor
     *
     * @return Doctor
     */
    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * Set Patient
     *
     * @param Patient $patient
     *
     * @return Appointment
     */
    public function setPatient(Patient $patient)
    {
        $this->patient = $patient;
    
        return $this;
    }

    /**
     * Get Patient
     *
     * @return Patient
     */
    public function getPatient()
    {
        return $this->patient;
    }

    /**
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param \DateTime $start
     */
    public function setStart($start)
    {
        $this->start = $start;
    }

    /**
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @param \DateTime $end
     */
    public function setEnd($end)
    {
        $this->end = $end;
    }


    /**
     * Add Schedule
     *
     * @param Schedule $schedule
     *
     * @return Schedule
     */
    public function addSchedule(Schedule $schedule)
    {
        $this->schedules[] = $schedule;

        return $this;
    }

    /**
     * Remove Schedule
     *
     * @param Schedule $schedule
     */
    public function removeSchedule(Schedule $schedule)
    {
        $this->schedules->removeElement($schedule);
    }

    /**
     * Get schedules
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSchedules()
    {
        return $this->schedules;
    }

    /**
     * Set notifySMS
     *
     * @param boolean $notifySMS
     *
     * @return Appointment
     */
    public function setNotifySMS($notifySMS)
    {
        $this->notifySMS = $notifySMS;

        return $this;
    }

    /**
     * Get notifySMS
     *
     * @return boolean
     */
    public function getNotifySMS()
    {
        return $this->notifySMS;
    }

    /**
     * Set notifyEmail
     *
     * @param boolean $notifyEmail
     *
     * @return Appointment
     */
    public function setNotifyEmail($notifyEmail)
    {
        $this->notifyEmail = $notifyEmail;

        return $this;
    }

    /**
     * Get notifyEmail
     *
     * @return boolean
     */
    public function getNotifyEmail()
    {
        return $this->notifyEmail;
    }


    /**
     * Add notification
     *
     * @param \onekit\AppBundle\Entity\Notification $notification
     *
     * @return Appointment
     */
    public function addNotification(\onekit\AppBundle\Entity\Notification $notification)
    {
        $this->notifications[] = $notification;

        return $this;
    }

    /**
     * Remove notification
     *
     * @param \onekit\AppBundle\Entity\Notification $notification
     */
    public function removeNotification(\onekit\AppBundle\Entity\Notification $notification)
    {
        $this->notifications->removeElement($notification);
    }

    /**
     * Get notifications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotifications()
    {
        return $this->notifications;
    }
}
