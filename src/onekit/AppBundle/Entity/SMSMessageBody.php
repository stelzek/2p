<?php

namespace onekit\AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SMSMessageTemplate
 *
 * @ORM\Table(name="sms_message_body")
 * @ORM\Entity
 */
class SMSMessageBody
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string $language
     * @ORM\Column(name="language", type="string", length=2)
     * @Assert\NotBlank()
     */
    protected $language = "de";

    /**
     * @var string
     * @Assert\Length(
     *      min = 20,
     *      max = 700,
     *      minMessage = "Message must be at least {{ limit }} characters long",
     *      maxMessage = "Message must be at least cannot be longer than {{ limit }} characters"
     * )
     * @ORM\Column(name="message", type="text")
     */
    protected $message;

    /**
     * @ORM\ManyToOne(targetEntity="onekit\AppBundle\Entity\SMSMessageTemplate", inversedBy="SMSMessageBodies", cascade={"persist"})
     * @ORM\JoinColumn(name="template_id", referencedColumnName="id")
     */
    protected $smsMessageTemplate;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @param string $language
     */
    public function setLanguage($language)
    {
        $this->language = $language;
    }

    /**
     * @return mixed
     */
    public function getSmsMessageTemplate()
    {
        return $this->smsMessageTemplate;
    }

    /**
     * @param mixed $smsMessageTemplate
     */
    public function setSMSMessageTemplate($smsMessageTemplate)
    {
        $this->smsMessageTemplate = $smsMessageTemplate;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param string $message
     */
    public function setMessage($message)
    {
        $this->message = $message;
    }

}
