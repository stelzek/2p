<?php
namespace onekit\AppBundle\Entity;


use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Session
 *
 * @ORM\Table(name="schedule")
 * @ORM\Entity(repositoryClass="onekit\AppBundle\Repository\ScheduleRepository")
 */
class Schedule
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var \DateTime $execute
     * @ORM\Column(name="execute", type="datetime")
     */
    protected $execute;

    /**
     * @ORM\ManyToOne(targetEntity="onekit\AppBundle\Entity\Appointment", inversedBy="schedules", cascade={"persist"})
     * @ORM\JoinColumn(name="appointment_id", referencedColumnName="id")
     */
    protected $appointment;

    /**
     * @ORM\ManyToOne(targetEntity="onekit\AppBundle\Entity\NotificationEvent")
     * @ORM\JoinColumn(name="event_id", referencedColumnName="id")
     */
    protected $notificationEvent;

    /**
     * @ORM\ManyToOne(targetEntity="onekit\AppBundle\Entity\Doctor", inversedBy="schedules", cascade={"persist"})
     * @ORM\JoinColumn(name="doctor_id", referencedColumnName="id")
     */
    protected $doctor;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->execute = new \DateTime('0000-00-00 00:00:00');
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getExecute()
    {
        return $this->execute;
    }

    /**
     * @param \DateTime $execute
     */
    public function setExecute($execute)
    {
        $this->execute = $execute;
    }


    /**
     * @return Doctor
     */
    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * @param Doctor $doctor
     */
    public function setDoctor($doctor)
    {
        $this->doctor = $doctor;
    }

    /**
     * @return Appointment
     */
    public function getAppointment()
    {
        return $this->appointment;
    }

    /**
     * @param Appointment $appointment
     */
    public function setAppointment(Appointment $appointment)
    {
        $this->appointment = $appointment;
    }

    /**
     * @return NotificationEvent
     */
    public function getNotificationEvent()
    {
        return $this->notificationEvent;
    }

    /**
     * @param NotificationEvent $notificationEvent
     */
    public function setNotificationEvent(NotificationEvent $notificationEvent)
    {
        $this->notificationEvent = $notificationEvent;
    }

}
