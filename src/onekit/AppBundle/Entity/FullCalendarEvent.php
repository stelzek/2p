<?php
namespace onekit\AppBundle\Entity;

class FullCalendarEvent
{
    /**
     * @var mixed Unique identifier of this event (optional).
     */
    protected $id;
    
    /**
     * @var string Title/label of the calendar event.
     */
    protected $title;

    /**
     * @var boolean.
     */
    protected $allDay;

    /**
     * @var \DateTime DateTime object of the event start date/time.
     */
    protected $start;

    /**
     * @var \DateTime DateTime object of the event end date/time.
     */
    protected $end;

    /**
     * @var string URL Relative to current path.
     */
    protected $url;

    /**
     * @var string css class for the event label
     */
    protected $className;


    /**
     * @var boolean
     */
    protected $editable;


    /**
     * @var boolean
     */
    protected $startEditable;


    /**
     * @var boolean
     */
    protected $durationEditable;

    /**
     * @var string
     */
    protected $rendering;

    /**
     * @var boolean
     */
    protected $overlap;

    /**
     * @var string HTML color code for the bg color of the event label.
     */
    protected $backgroundColor;

    /**
     * @var string
     */
    protected $color;

    /**
     * @var string
     */
    protected $borderColor;

    /**
     * @var string HTML color code for the foregorund color of the event label.
     */
    protected $textColor;
    
    /**
     * @var array Non-standard fields
     */
    protected $otherFields = array();
    
    public function __construct($title, \DateTime $start, \DateTime $end = null)
    {
        $this->title = $title;
        $this->start = $start;
        $this->end = $end;
    }

    /**
     * Convert calendar event details to an array
     * 
     * @return array $event 
     */
    public function toArray()
    {
        $event = array();
        
        if ($this->id !== null) {
            $event['id'] = $this->id;
        }
        
        $event['title'] = $this->title;
        $event['start'] = $this->start->format("Y-m-d H:i:s");
        
        if ($this->url !== null) {
            $event['url'] = $this->url;
        }
        
        if ($this->backgroundColor !== null) {
            $event['backgroundColor'] = $this->backgroundColor;
            $event['borderColor'] = $this->borderColor;
        }
        
        if ($this->borderColor !== null) {
            $event['borderColor'] = $this->borderColor;
        }

        if ($this->textColor !== null) {
            $event['textColor'] = $this->textColor;
        }
        
        if ($this->className !== null) {
            $event['className'] = $this->className;
        }

        if ($this->editable !== null) {
            $event['editable'] = $this->editable;
        }

        if ($this->startEditable !== null) {
            $event['startEditable'] = $this->startEditable;
        }

        if ($this->durationEditable !== null) {
            $event['durationEditable'] = $this->durationEditable;
        }

        if ($this->rendering !== null) {
            $event['rendering'] = $this->rendering;
        }

        if ($this->overlap !== null) {
            $event['overlap'] = $this->overlap;
        }

        if ($this->color !== null) {
            $event['color'] = $this->color;
        }

        if ($this->end !== null) {
            $event['end'] = $this->end->format("Y-m-d H:i:s");
        }
    
        foreach ($this->otherFields as $field => $value) {
            $event[$field] = $value;
        }
        
        return $event;
    }

    public function setId($id)
    {
        $this->id = $id;
    }
    
    public function getId()
    {
        return $this->id;
    }
    
    public function setTitle($title) 
    {
        $this->title = $title;
    }
    
    public function getTitle() 
    {
        return $this->title;
    }

    public function setUrl($url)
    {
        $this->url = $url;
    }
    
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $name
     * @param string $value
     */
    public function addField($name, $value)
    {
        $this->otherFields[$name] = $value;
    }

    /**
     * @param string $name
     */
    public function removeField($name)
    {
        if (!array_key_exists($name, $this->otherFields)) {
            return;
        }

        unset($this->otherFields[$name]);
    }

    /**
     * @return boolean
     */
    public function isAllDay()
    {
        return $this->allDay;
    }

    /**
     * @param boolean $allDay
     */
    public function setAllDay($allDay)
    {
        $this->allDay = $allDay;
    }

    /**
     * @return \DateTime
     */
    public function getStart()
    {
        return $this->start;
    }

    /**
     * @param \DateTime $start
     */
    public function setStart($start)
    {
        $this->start = $start;
    }

    /**
     * @return \DateTime
     */
    public function getEnd()
    {
        return $this->end;
    }

    /**
     * @param \DateTime $end
     */
    public function setEnd($end)
    {
        $this->end = $end;
    }

    /**
     * @return string
     */
    public function getClassName()
    {
        return $this->className;
    }

    /**
     * @param string $className
     */
    public function setClassName($className)
    {
        $this->className = $className;
    }

    /**
     * @return boolean
     */
    public function isEditable()
    {
        return $this->editable;
    }

    /**
     * @param boolean $editable
     */
    public function setEditable($editable)
    {
        $this->editable = $editable;
    }

    /**
     * @return boolean
     */
    public function isStartEditable()
    {
        return $this->startEditable;
    }

    /**
     * @param boolean $startEditable
     */
    public function setStartEditable($startEditable)
    {
        $this->startEditable = $startEditable;
    }

    /**
     * @return boolean
     */
    public function isDurationEditable()
    {
        return $this->durationEditable;
    }

    /**
     * @param boolean $durationEditable
     */
    public function setDurationEditable($durationEditable)
    {
        $this->durationEditable = $durationEditable;
    }

    /**
     * @return string
     */
    public function getBackgroundColor()
    {
        return $this->backgroundColor;
    }

    /**
     * @param string $backgroundColor
     */
    public function setBackgroundColor($backgroundColor)
    {
        $this->backgroundColor = $backgroundColor;
    }

    /**
     * @return string
     */
    public function getTextColor()
    {
        return $this->textColor;
    }

    /**
     * @param string $textColor
     */
    public function setTextColor($textColor)
    {
        $this->textColor = $textColor;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * @return string
     */
    public function getRendering()
    {
        return $this->rendering;
    }

    /**
     * @param string $rendering
     */
    public function setRendering($rendering)
    {
        $this->rendering = $rendering;
    }

    /**
     * @return boolean
     */
    public function isOverlap()
    {
        return $this->overlap;
    }

    /**
     * @param boolean $overlap
     */
    public function setOverlap($overlap)
    {
        $this->overlap = $overlap;
    }

    /**
     * @return string
     */
    public function getBorderColor()
    {
        return $this->borderColor;
    }

    /**
     * @param string $borderColor
     */
    public function setBorderColor($borderColor)
    {
        $this->borderColor = $borderColor;
    }
}
