<?php
namespace onekit\AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use onekit\AppBundle\Validator\Constraints as AppAssert;
use JMS\Serializer\Annotation as Serial;

use onekit\AppBundle\Entity\Behavior\SoftDeletable;

/**
 * @ORM\Entity(repositoryClass = "onekit\AppBundle\Repository\AssistantRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table("assistant")
 * @Gedmo\SoftDeleteable
 */
class Assistant
{
    use SoftDeletable;

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serial\Groups({"default", "assistant"})
     */
    protected $id;

    /**
     * @var \onekit\AppBundle\Entity\Doctor
     *
     * @ORM\ManyToOne(targetEntity="onekit\AppBundle\Entity\Doctor", cascade={"persist"})
     * @ORM\JoinColumn(name="doctor_id", referencedColumnName="id", nullable=false)
     */
    protected $doctor;

    /**
     * @var \onekit\AppBundle\Entity\User
     *
     * @ORM\OneToOne(targetEntity="onekit\AppBundle\Entity\User", inversedBy="assistant", cascade={"persist"})
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", nullable=false)
     */
    protected $account;

    /**
     * @var string $title
     * @Assert\NotBlank()
     * @ORM\Column(name="title", type="string", length=255, nullable=false)
     * @Serial\Groups({"default", "assistant"})
     */
    protected $title;

    /**
     * @var boolean $edit
     * @ORM\Column(name="edit", type="boolean", nullable=false)
     * @Serial\Groups({"assistant"})
     */
    protected $edit = false;

    /**
     * @var boolean $managePatients
     * @ORM\Column(name="manage_patients", type="boolean", nullable=false)
     * @Serial\Groups({"assistant"})
     */
    protected $managePatients = false;

    /**
     * @var boolean $manageAppointments
     * @ORM\Column(name="manage_appointments", type="boolean", nullable=false)
     * @Serial\Groups({"assistant"})
     */
    protected $manageAppointments = false;


    /**
     * @var boolean $checkBalance
     * @ORM\Column(name="check_balance", type="boolean", nullable=false)
     * @Serial\Groups({"assistant"})
     */
    protected $checkBalance = false;

    /**
     * @var boolean $makePayment
     * @ORM\Column(name="make_payment", type="boolean", nullable=false)
     * @Serial\Groups({"assistant"})
     */
    protected $makePayment = false;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Assistant
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set edit
     *
     * @param boolean $edit
     *
     * @return Assistant
     */
    public function setEdit($edit)
    {
        $this->edit = !!$edit;

        return $this;
    }

    /**
     * Get edit
     *
     * @return boolean
     */
    public function getEdit()
    {
        return $this->edit;
    }

    /**
     * Set managePatients
     *
     * @param boolean $managePatients
     *
     * @return Assistant
     */
    public function setManagePatients($managePatients)
    {
        $this->managePatients = !!$managePatients;

        return $this;
    }

    /**
     * Get managePatients
     *
     * @return boolean
     */
    public function getManagePatients()
    {
        return $this->managePatients;
    }

    /**
     * Set manageAppointments
     *
     * @param boolean $manageAppointments
     *
     * @return Assistant
     */
    public function setManageAppointments($manageAppointments)
    {
        $this->manageAppointments = !!$manageAppointments;

        return $this;
    }

    /**
     * Get manageAppointments
     *
     * @return boolean
     */
    public function getManageAppointments()
    {
        return $this->manageAppointments;
    }

    /**
     * Set checkBalance
     *
     * @param boolean $checkBalance
     *
     * @return Assistant
     */
    public function setCheckBalance($checkBalance)
    {
        $this->checkBalance = !!$checkBalance;

        return $this;
    }

    /**
     * Get checkBalance
     *
     * @return boolean
     */
    public function getCheckBalance()
    {
        return $this->checkBalance;
    }

    /**
     * Set makePayment
     *
     * @param boolean $makePayment
     *
     * @return Assistant
     */
    public function setMakePayment($makePayment)
    {
        $this->makePayment = !!$makePayment;

        return $this;
    }

    /**
     * Get makePayment
     *
     * @return boolean
     */
    public function getMakePayment()
    {
        return $this->makePayment;
    }

    /**
     * Set doctor
     *
     * @param \onekit\AppBundle\Entity\Doctor $doctor
     *
     * @return Assistant
     */
    public function setDoctor(\onekit\AppBundle\Entity\Doctor $doctor)
    {
        $this->doctor = $doctor;

        return $this;
    }

    /**
     * Get doctor
     *
     * @return \onekit\AppBundle\Entity\Doctor
     */
    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * Set account
     *
     * @param \onekit\AppBundle\Entity\User $account
     *
     * @return Assistant
     */
    public function setAccount(\onekit\AppBundle\Entity\User $account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return \onekit\AppBundle\Entity\User
     */
    public function getAccount()
    {
        return $this->account;
    }
}
