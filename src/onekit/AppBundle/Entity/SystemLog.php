<?php

namespace onekit\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\Validator\Constraints\DateTime;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

/**
 * Session
 *
 * @ORM\Table(name="patient_messenger_system_log")
 * @ORM\Entity(repositoryClass="onekit\AppBundle\Repository\SystemLogRepository")
 */
class SystemLog
{
    const USER_CREATED_ACTION = 'user-created-action';
    const USER_EDITED_ACTION = 'user-edited-action';
    const USER_DELETED_ACTION = 'user-deleted-action';

    const DOCTOR_CREATED_ACTION = 'doctor-created-action';
    const DOCTOR_EDITED_ACTION = 'doctor-edited-action';
    const DOCTOR_DELETED_ACTION = 'doctor-deleted-action';

    const ASSISTANT_CREATED_ACTION = 'assistant-created-action';
    const ASSISTANT_EDITED_ACTION = 'assistant-edited-action';
    const ASSISTANT_DELETED_ACTION = 'assistant-deleted-action';

    const PATIENT_CREATED_ACTION = 'patient-created-action';
    const PATIENT_EDITED_ACTION = 'patient-edited-action';
    const PATIENT_DELETED_ACTION = 'patient-deleted-action';

    const APPOINTMENT_CREATED_ACTION = 'appointment-created-action';
    const APPOINTMENT_EDITED_ACTION = 'appointment-edited-action';
    const APPOINTMENT_DELETED_ACTION = 'appointment-deleted-action';

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="onekit\AppBundle\Entity\User", inversedBy="systemLogs")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)
     */
    protected $user;

    /**
     * @var datetime $created
     * @ORM\Column(name="created", type="datetime", nullable=false)
     */
    protected $created;

    /**
     * @var int $targetId
     * @ORM\Column(name="target_id", type="integer", nullable=true)
     */
    protected $targetId;

    /**
     * @var string $action
     * @ORM\Column(name="action", nullable=false)
     */
    protected $action;

    /**
     * @var string $ip
     * @ORM\Column(name="ip", nullable=true)
     */
    protected $ip;


    public function __construct(User $user = null, $ip = null, $action = null, $targetId = null)
    {
        $this->setUser($user);
        $this->setIp($ip);
        $this->setAction($action);
        $this->setTargetId($targetId);
        $this->setCreated(new \DateTime());
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return SystemLog
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set action
     *
     * @param string $action
     *
     * @return SystemLog
     */
    public function setAction($action)
    {
        $this->action = $action;

        return $this;
    }

    /**
     * Get action
     *
     * @return string
     */
    public function getAction()
    {
        return $this->action;
    }

    /**
     * Set $targetId
     *
     * @param integer $targetId
     *
     * @return SystemLog
     */
    public function setTargetId($targetId)
    {
        $this->targetId = $targetId;

        return $this;
    }

    /**
     * Get targetId
     *
     * @return integer
     */
    public function getTargetId()
    {
        return $this->targetId;
    }

    /**
     * Set ip
     *
     * @param string $ip
     *
     * @return SystemLog
     */
    public function setIp($ip)
    {
        $this->ip = $ip;

        return $this;
    }

    /**
     * Get ip
     *
     * @return string
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * Set user
     *
     * @param \onekit\AppBundle\Entity\User $user
     *
     * @return SystemLog
     */
    public function setUser(\onekit\AppBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \onekit\AppBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
