<?php
namespace onekit\AppBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;
use onekit\AppBundle\Validator\Constraints as AppAssert;
use JMS\Serializer\Annotation as Serial;

use onekit\AppBundle\Entity\Behavior\SoftDeletable;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity(repositoryClass = "onekit\AppBundle\Repository\PatientRepository")
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table("patient")
 * @AppAssert\AtLeastOne({"email", "phone"})
 * @Gedmo\SoftDeleteable
 */
class Patient
{
    use SoftDeletable;

    /**
     * @var integer
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serial\Groups({"default"})
     */
    protected $id;

    /**
     * @var \onekit\AppBundle\Entity\User
     *
     * @ORM\ManyToOne(targetEntity="onekit\AppBundle\Entity\User")
     * @ORM\JoinColumn(name="account_id", referencedColumnName="id", nullable=true)
     */
    protected $account;

    /**
     * @var \onekit\AppBundle\Entity\Doctor
     *
     * @ORM\ManyToOne(targetEntity="onekit\AppBundle\Entity\Doctor", inversedBy="patients")
     * @ORM\JoinColumn(name="doctor_id", referencedColumnName="id", nullable=false)
     */
    protected $doctor;

    /**
     * @ORM\Column(name="title", type="string", nullable=true)
     * @Serial\SerializedName("title")
     * @Serial\Groups({"patient"})
     */
    protected $title;

    /**
     * @var string $firstName
     * @Assert\NotBlank()
     * @ORM\Column(name="first_name", type="string", length=255, nullable=false)
     * @Serial\SerializedName("first_name")
     * @Serial\Groups({"default"})
     */
    protected $firstName;

    /**
     * @var string $lastName
     * @Assert\NotBlank()
     * @ORM\Column(name="last_name", type="string", length=255, nullable=false)
     * @Serial\SerializedName("last_name")
     * @Serial\Groups({"default"})
     */
    protected $lastName;

    /**
     * @var string $insuranceType
     * @ORM\Column(name="insurance_type", type="string", length=255, nullable=true)
     * @Serial\SerializedName("insurance_type")
     * @Serial\Groups({"patient"})
     */
    protected $insuranceType;

    /**
     * @var string $insuranceCompany
     * @ORM\Column(name="insurance_company", type="string", length=255, nullable=true)
     * @Serial\SerializedName("insurance_company")
     * @Serial\Groups({"patient"})
     */
    protected $insuranceCompany;


    /**
     * @var integer $gender
     * @ORM\Column(name="gender", type="integer", length=1, nullable=false)
     * @Serial\SerializedName("gender")
     * @Serial\Groups({"patient"})
     */
    protected $gender = 1;

    /**
     * @var string $email
     * @ORM\Column(name="email", type="string", length=16, nullable=true)
     * @Assert\Email()
     * @Serial\SerializedName("email")
     * @Serial\Groups({"patient"})
     */
    protected $email;

    /**
     * @var boolean
     *
     * @ORM\Column(name="notify_email", type="boolean", nullable=false)
     * @Serial\SerializedName("notify_email")
     * @Serial\Groups({"patient"})
     */
    protected $notifyEmail;

    /**
     * @var string $phone
     * @ORM\Column(name="phone", type="string", length=16, nullable=true)
     * @Assert\Length(min=12)
     * @Serial\SerializedName("phone")
     * @Serial\Groups({"patient"})
     */
    protected $phone;

    /**
     * @var boolean
     *
     * @ORM\Column(name="notify_sms", type="boolean", nullable=false)
     * @Serial\SerializedName("notify_sms")
     * @Serial\Groups({"patient"})
     */
    protected $notifySMS;

    /**
     * @var string $language
     * @ORM\Column(name="language", type="string", length=2, nullable=false)
     * @Assert\Choice(callback = {"onekit\AppBundle\Entity\User", "getLanguages"})
     * @Assert\NotNull()
     * @Serial\Groups({"patient"})
     */
    protected $language;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Appointment", mappedBy="patient")
     */
    protected $appointments;

    /**
     * @var $notifications \Doctrine\Common\Collections\ArrayCollection
     * @ORM\OneToMany(targetEntity="onekit\AppBundle\Entity\Notification", mappedBy="patient")
     * @ORM\OrderBy({"created"="desc"})
     */
    protected $notifications;

    public function __toString()
    {
        return 'patient_'.$this->getId();
    }

    /**
     * @return string
     */
    public function getFullTitle()
    {
        return sprintf('%s %0.1s. %s', $this->getTitle(), $this->getFirstName(), $this->getLastName());
    }

    /**
     * @return string
     */
    public function getFullName()
    {
        return sprintf('%s %s', $this->getFirstName(), $this->getLastName());
    }

    /**
     * Set language
     *
     * @param string|null $language
     *
     * @return Patient
     */
    public function setLanguage($language = null)
    {
        $this->language = is_null($language) ? (is_null($this->getAccount()) ? $this->getDoctor()->getLanguage() : $this->getAccount()->getLanguage()) : $language;

        return $this;
    }

    public function isGermanPhoneNumber()
    {
        return substr($this->getPhone(), 0, 2) == 49;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->appointments = new ArrayCollection();
        $this->notifications = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Patient
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Patient
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Patient
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set insuranceType
     *
     * @param string $insuranceType
     *
     * @return Patient
     */
    public function setInsuranceType($insuranceType)
    {
        $this->insuranceType = $insuranceType;

        return $this;
    }

    /**
     * Get insuranceType
     *
     * @return string
     */
    public function getInsuranceType()
    {
        return $this->insuranceType;
    }

    /**
     * Set insuranceCompany
     *
     * @param string $insuranceCompany
     *
     * @return Patient
     */
    public function setInsuranceCompany($insuranceCompany)
    {
        $this->insuranceCompany = $insuranceCompany;

        return $this;
    }

    /**
     * Get insuranceCompany
     *
     * @return string
     */
    public function getInsuranceCompany()
    {
        return $this->insuranceCompany;
    }

    /**
     * Set gender
     *
     * @param integer $gender
     *
     * @return Patient
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return integer
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Patient
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Patient
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set account
     *
     * @param User|null $account
     *
     * @return Patient
     */
    public function setAccount(User $account = null)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return User|null
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set doctor
     *
     * @param Doctor $doctor
     *
     * @return Patient
     */
    public function setDoctor(Doctor $doctor)
    {
        $this->doctor = $doctor;

        return $this;
    }

    /**
     * Get doctor
     *
     * @return Doctor
     */
    public function getDoctor()
    {
        return $this->doctor;
    }

    /**
     * Add appointment
     *
     * @param Appointment $appointment
     *
     * @return Patient
     */
    public function addAppointment(Appointment $appointment)
    {
        $this->appointments[] = $appointment;
    
        return $this;
    }

    /**
     * Remove appointment
     *
     * @param Appointment $appointment
     */
    public function removeAppointment(Appointment $appointment)
    {
        $this->appointments->removeElement($appointment);
    }

    /**
     * Get appointments
     *
     * @return ArrayCollection
     */
    public function getAppointments()
    {
        return $this->appointments;
    }

    /**
     * Set notifyEmail
     *
     * @param boolean $notifyEmail
     *
     * @return Patient
     */
    public function setNotifyEmail($notifyEmail)
    {
        $this->notifyEmail = $notifyEmail;

        return $this;
    }

    /**
     * Get notifyEmail
     *
     * @return boolean
     */
    public function getNotifyEmail()
    {
        return $this->notifyEmail;
    }

    /**
     * Set notifySMS
     *
     * @param boolean $notifySMS
     *
     * @return Patient
     */
    public function setNotifySMS($notifySMS)
    {
        $this->notifySMS = $notifySMS;

        return $this;
    }

    /**
     * Get notifySMS
     *
     * @return boolean
     */
    public function getNotifySMS()
    {
        return $this->notifySMS;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Add notification
     *
     * @param \onekit\AppBundle\Entity\Notification $notification
     *
     * @return Patient
     */
    public function addNotification(\onekit\AppBundle\Entity\Notification $notification)
    {
        $this->notifications[] = $notification;

        return $this;
    }

    /**
     * Remove notification
     *
     * @param \onekit\AppBundle\Entity\Notification $notification
     */
    public function removeNotification(\onekit\AppBundle\Entity\Notification $notification)
    {
        $this->notifications->removeElement($notification);
    }

    /**
     * Get notifications
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNotifications()
    {
        return $this->notifications;
    }
}
