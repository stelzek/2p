<?php

namespace onekit\AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use onekit\AppBundle\Entity\SMSMessageBody;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * SMSMessageTemplate
 *
 * @ORM\Table(name="sms_message_template")
 * @ORM\Entity
 */
class SMSMessageTemplate
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\ManyToOne(targetEntity="onekit\AppBundle\Entity\Doctor", inversedBy="smsMessageTemplates")
     * @ORM\JoinColumn(name="doctor_id", referencedColumnName="id")
     */
    protected $doctor;


    /**
     * @ORM\OneToMany(targetEntity="onekit\AppBundle\Entity\SMSMessageBody", mappedBy="smsMessageTemplate", cascade={"persist"})
     */
    protected $SMSMessageBodies;


    /**
     * @ORM\ManyToOne(targetEntity="onekit\AppBundle\Entity\NotificationEvent", cascade={"persist"})
     * @ORM\JoinColumn(name="event_id", referencedColumnName="id")
     */
    protected $notificationEvent;


    public function __construct()
    {
        $this->SMSMessageBodies = new ArrayCollection();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getNotificationEvent()
    {
        return $this->notificationEvent;
    }

    /**
     * @param mixed $notificationEvent
     */
    public function setNotificationEvent(NotificationEvent $notificationEvent)
    {
        $this->notificationEvent = $notificationEvent;
    }

    /**
     * Add SMSMessageBody
     *
     * @param \onekit\AppBundle\Entity\SMSMessageBody $SMSMessageBodies
     * @return $this
     */
    public function addSMSMessageBody(SMSMessageBody $SMSMessageBodies)
    {
        $this->SMSMessageBodies[] = $SMSMessageBodies;
        $SMSMessageBodies->setSMSMessageTemplate($this);
        return $this;
    }

    /**
     * Remove SMSMessageBody
     *
     * @param SMSMessageBody $SMSMessageBody
     */
    public function removeSMSMessageBody(SMSMessageBody $SMSMessageBody)
    {
        $this->SMSMessageBodies->removeElement($SMSMessageBody);
    }

    /**
     * @return mixed
     */
    public function getSMSMessageBodies()
    {
        return $this->SMSMessageBodies;
    }

    /**
     * @param $lang
     * @return SMSMessageBody|null
     */
    public function getTranslatedBody($lang)
    {
        $bodies = $this->SMSMessageBodies->filter(
            function(SMSMessageBody $body) use ($lang) {
                return $body->getLanguage() == $lang;
            }
        );
        return $bodies->count() ? $bodies->first() : null;
    }

    /**
     * Set doctor
     *
     * @param \onekit\AppBundle\Entity\Doctor $doctor
     *
     * @return SMSMessageTemplate
     */
    public function setDoctor(\onekit\AppBundle\Entity\Doctor $doctor = null)
    {
        $this->doctor = $doctor;

        return $this;
    }

    /**
     * Get doctor
     *
     * @return \onekit\AppBundle\Entity\Doctor
     */
    public function getDoctor()
    {
        return $this->doctor;
    }
}
