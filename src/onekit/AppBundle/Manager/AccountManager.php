<?php

namespace onekit\AppBundle\Manager;

use Doctrine\ORM\EntityManager;
use FOS\UserBundle\Doctrine\UserManager as FOSUserManager;
use FOS\UserBundle\Model\UserInterface;
use FOS\UserBundle\Util\UserManipulator;
use onekit\AppBundle\Entity\Input\CreateUser;
use onekit\AppBundle\Entity\Input\UserCredentials;
use onekit\AppBundle\Entity\Input\UserLanguage;
use onekit\AppBundle\Entity\Input\UserPicture;
use onekit\AppBundle\Entity\User;
use onekit\AppBundle\Repository\UserRepository;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;

class AccountManager extends ApiManager
{
    /**
     * @var UserRepository
     */
    protected $repo;

    /**
     * @var UserManipulator
     */
    protected $userManipulator;

    /**
     * @var FOSUserManager
     */
    protected $userManager;

    /**
     * @var string
     */
    protected $mediaPath;

    public function __construct(EntityManager $em, UserManipulator $userManipulator, FOSUserManager $userManager, $mediaPath)
    {
        parent::__construct($em);
        $this->userManipulator = $userManipulator;
        $this->userManager = $userManager;
        $this->mediaPath = realpath($mediaPath);
    }

    /**
     * @param CreateUser $createUser
     * @return User
     */
    public function create(CreateUser $createUser)
    {
        /** @var User $user */
        $user = $this->userManipulator->create($createUser->username, $createUser->password, $createUser->email, true, $createUser->is_admin);
        $user->setLanguage($createUser->language);
        $this->userManager->updateUser($user);
        $this->userManager->reloadUser($user);
        return $user;
    }

    /**
     * @param User $user
     * @param CreateUser $createUser
     * @return User
     */
    public function update(User $user, CreateUser $createUser)
    {
        $user->setUsername($createUser->username);
        $user->setEmail($createUser->email);
        $user->setPlainPassword($createUser->password);
        $user->setSuperAdmin($createUser->is_admin);
        $this->userManager->updateUser($user);
        return $user;
    }

    public function setCredentials(User $user, UserCredentials $userCredentials)
    {
        if ($userCredentials->username) {
            $user->setUsername($userCredentials->username);
        }
        if ($userCredentials->password) {
            $user->setPlainPassword($userCredentials->password);
        }
        $this->userManager->updateUser($user);
        return $user;
    }

    public function setLanguage(User $user, UserLanguage $userLanguage)
    {
        $user->setLanguage($userLanguage->language);
        $this->userManager->updateUser($user);
        return $user;
    }

    public function setPicture(User $user, UserPicture $picture = null)
    {
        if (!is_null($picture)) {
            $image = $picture->image;
            $fs = new Filesystem();
            $fullPath = sprintf('%s/userpic-%u.%s', $this->mediaPath, $user->getId(), $image->guessExtension());
            $fs->copy($image->getRealPath(), $fullPath, true);
            $user->setImagePath(rtrim($fs->makePathRelative($fullPath, $this->mediaPath), '/'));
        } else {
            $user->setImagePath(null);
        }
        $this->userManager->updateUser($user);
        return $user;
    }

    /**
     * @return $this
     */
    public function getList()
    {
        $this->qb = $this->repo->getListQB();
        return $this;
    }

    public function order($column, $direction)
    {
        $this->qb->orderBy('u.'.$column, $direction);
        return $this;
    }

    protected function setRepository()
    {
        $this->repo = $this->em->getRepository('AppBundle:User');
    }
}
