<?php

namespace onekit\AppBundle\Manager\SMSExpert;

use Exception;

class SMSExpertSenderException extends Exception{

	function __construct($strMessage){
		parent::__construct($strMessage);
	}
}
