<?php

namespace onekit\AppBundle\Manager\SMSExpert;

class SMSExpertSender
{

    //user: frankscrock
    //pass: 6wSzkMrb

    /**
     * @var string $user Enter Your Username here
     */
    private static $user = 'frankscrock';
    /**
     * @var string $gateway_password Enter Your Gateway-Password here
     */
    private static $gateway_password = 'frankpassword';
    /**
     * @var string $sendmode Possible values for sendmode: curl, socket, file
     */
    private static $sendmode = 'socket';
    /**
     * @var boolean $debug enables or disables debugging output
     */
    private static $debug = false;

    private static $gateway_protocol = 'https://';
    private static $gateway_host = 'gateway.sms-expert.de';
    private static $gateway_port = 443;
    private static $gateway_urlpath = '/send/';
    private static $version = 1.01;

    private static $typeList = array('standard', 'expert','email');
    private $type = null;
    private $receiver = null;
    private $sender = null;
    private $message = null;
    private $timestamp = null;
    private $responseStatusCode = null;
    private $responseStatusText = null;
    private $responseMessageID = null;
    private $responseCost = null;

    /**
     *  Sets the type of the SMS
     * @param string $type Type of the SMS. Possible values "standard" or "expert"
     */
    public function setSMSType($type)
    {
        $type = strtolower($type);
        if (!in_array($type, self::$typeList)) {
            throw new SMSExpertSenderException('Invalid SMS type');
        }
        $this->type = $type;
    }

    /**
     * Gets the type of the sms
     * @return string Type of the SMS
     */
    public function getSMSType()
    {
        return $this->type;
    }

    /**
     * Sets the phone number of the receiver
     * @param integer $receiver Number in international format WITHOUT leading + or 00
     * @throws SMSExpertSenderException
     */
    public function setReceiver($receiver)
    {
        $regex = "/^[1-9]{1}[0-9]{5,15}$/i";
        if (!preg_match($regex, $receiver)) {
            throw new SMSExpertSenderException('Invalid recipient');
        }
        $this->receiver = $receiver;
    }

    /**
     *  Returns the phone number of the receiver
     * @return integer Phone number of the receiver
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * Sets the sender phone number or a text
     * @param string $sender Sender phone number in international format WITHOUT leading + or 00. From 6 to 16 digits possible. For a text as sender up to 11 characters possible.
     * @throws SMSExpertSenderException
     */
    public function setSender($sender)
    {

        if (mb_strlen($sender) > 11) {
            throw new SMSExpertSenderException('Invalid sender number');
        }

        $this->sender = $sender;
    }

    /**
     * Returns the sender of the SMS
     * @return string Sender of the SMS
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * Sets the message text.
     * @param string $message The message text. Up to 1530 characters possible.
     * @throws SMSExpertSenderException
     */
    public function setMessage($message)
    {
        $len = mb_strlen($message);
        if ($len < 1 || $len > 1530) {
            throw new SMSExpertSenderException('Invalid message');
        }
        $this->message = $message;
    }

    /**
     * Returns the message text.
     * @return string The message text
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     *  Sets the date and the time for a timeshifted SMS. DON'T call these function if you want send the SMS immediately.
     * @param integer $year Year (4 digits, YYYY)
     * @param integer $month Month (2 digits, mm)
     * @param integer $day Day (2 digits, dd)
     * @param integer $hour Hour (2 digits, HH)
     * @param integer $minute Minute (2 digits, ii)
     * @throws SMSExpertSenderException
     */
    public function setSendDateTime($year, $month, $day, $hour, $minute)
    {
        if (!checkdate($month, $day, $year)) {
            throw new SMSExpertSenderException('Ungültiges Datum für den Versandzeitpunkt eingegeben.');
        }
        if ($hour < 0 || $hour > 23) {
            throw new SMSExpertSenderException('Ungültige Stunde für den Versandzeitpunkt eingegeben.');
        }
        if ($minute < 0 || $minute > 59) {
            throw new SMSExpertSenderException('Ungültige Minute für den Versandzeitpunkt eingegeben.');
        }
        $timestamp_send = mktime($hour, $minute, 0, $month, $day, $year);
        $timestamp_now = time();
        if (($timestamp_now + 300) > $timestamp_send) {
            throw new SMSExpertSenderException('Versandzeitpunkt befindet sich in der Vergangenzeit oder innerhalb der nächsten 5 Minuten.');
        }
        $this->timestamp = $timestamp_send;
    }

    /**
     * Returns the date and time for a timeshifted SMS
     * @param string $format Date format
     * @return string Date and Time
     * @see http://php.net/manual/de/function.date.php
     */
    public function getSendDateTime($format = "YYYY-mm-dd HH:ii")
    {
        $time_string = date($format, $this->timestamp);
        return $time_string;
    }

    /**
     * Checks and sends the SMS
     * @throws SMSExpertSenderException
     */
    public function send()
    {
        if (is_null($this->type) || is_null($this->receiver) || is_null($this->message)) {
            throw new SMSExpertSenderException('Not all required parameters are set.');
        }
        if (is_null($this->sender) && $this->type == 'expert') {
            throw new SMSExpertSenderException('The required parameter "sender" for the type "Expert" is not set.');
        }
        $status = FALSE;
        if (self::$sendmode == 'curl') {
            $status = $this->sendModeCurl();
        }
        if (self::$sendmode == 'socket') {
            $status = $this->sendModeSocket();

        }
        if (self::$sendmode == 'file') {
            $status = $this->sendModeFile();
        }
        if (!$status) {
            throw new SMSExpertSenderException('SMS sending failed! SMS gateway is not reachable or the selected sendMode is not supported.');
        }
    }

    /**
     *  Sends the SMS via Curl (POST)
     * @link http://de.php.net/manual/de/book.curl.php
     */
    private function sendModeCurl()
    {
        $cu = curl_init();
        if (!$cu) {
            return false;
        }
        $url = self::$gateway_protocol . self::$gateway_host . self::$gateway_urlpath;
        if (self::$debug) {
            echo "<strong>URL: </strong>$url</br>";
            echo "<strong>Port: </strong>" . self::$gateway_port . "</br>";
        }
        curl_setopt($cu, CURLOPT_URL, $url);
        curl_setopt($cu, CURLOPT_PORT, self::$gateway_port);
        curl_setopt($cu, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($cu, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($cu, CURLOPT_USERAGENT, 'SMSExpertSenderPHP v' . number_format(self::$version, 2, '.', ','));
        curl_setopt($cu, CURLOPT_POST, true);
        curl_setopt($cu, CURLOPT_POSTFIELDS, $this->getRequestData());
        $response = curl_exec($cu);
        $this->readResponse($response);
        curl_close($cu);
        return true;
    }

    /**
     * Sends the SMS via Socket (POST)
     * @link http://de.php.net/manual/de/function.fsockopen.php
     * @link http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html
     */
    private function sendModeSocket()
    {
        if (self::$gateway_protocol == 'https://') {
            $protocol = 'ssl://';
        } elseif (self::$gateway_protocol == 'http://') {
            $protocol = 'tcp://';
        } else {
            return false;
        }
        $hostname = $protocol . self::$gateway_host;
        if (self::$debug) {
            echo "<strong>Hostname: </strong>$hostname</br>";
            echo "<strong>Port: </strong>" . self::$gateway_port . "</br>";
            echo "<strong>Path: </strong>" . self::$gateway_urlpath . "</br>";
        }
        // Open connection
        $socket = fsockopen($hostname, self::$gateway_port, $errno, $errstr, 2);
        if (!$socket) { // Unable to connect
            return false;
        }
        // Create a header
        $request = $this->getRequestData();
        $header = 'POST ' . self::$gateway_urlpath . " HTTP/1.1\r\n";
        $header .= 'Host: ' . self::$gateway_host . "\r\n";
        $header .= 'User-Agent: SMSExpertSenderPHP v' . number_format(self::$version, 2, '.', ',') . "\r\n";
        $header .= 'Content-Type: application/x-www-form-urlencoded' . "\r\n";
        $header .= 'Content-Length: ' . strlen($request) . "\r\n";
        $header .= 'Connection: close' . "\r\n\r\n";
        $header .= $request . "\r\n";
        fputs($socket, $header); // Send header
        $response = null;
        while (!feof($socket)) {
            $response[] = fgets($socket, 4096); // Receive response
        }
        fclose($socket); // close connection
        $content = $this->getContentOfHTTPResponse($response);
        $this->readResponse($content);
        return true;
    }

    /**
     * Extracts the content of the HTTP-Response. Needed only for the sendmode "socket".
     * @return string Content of the HTTP-Response.
     */
    private function getContentOfHTTPResponse(Array $response)
    {
        $content = '';
        $content_start = null;
        for ($i = 0; $i < sizeof($response); $i++) {
            if (is_null($content_start) && $response[$i] == "\r\n") {
                $content_start = $i + 1;
            }
            if (!is_null($content_start) && $i >= $content_start) {
                $content .= $response[$i];
            }
        }
        return $content;
    }

    /**
     * Sends the SMS via file (GET)
     * @link http://www.php.net/manual/de/function.file-get-contents.php
     */
    private function sendModeFile()
    {
        $url = self::$gateway_protocol . self::$gateway_host . ':' . self::$gateway_port;
        $url .= self::$gateway_urlpath . '?' . $this->getRequestData();
        if (self::$debug) {
            echo "<strong>URL: </strong>$url</br>";
        }
        $response = file_get_contents($url);
        $this->readResponse($response);
        return true;
    }

    /**
     * Returns urlencoded name-value pairs for the HTTP-Request
     * @return string Urlencoded name-value pairs
     */
    private function getRequestData()
    {
        $request = 'user=' . urlencode(self::$user) . '&';
        $request .= 'type=' . urlencode($this->type) . '&';
        $request .= 'receiver=' . urlencode($this->receiver) . '&';
        if (!is_null($this->sender)) {
            $request .= 'sender=' . urlencode($this->sender) . '&';
        }
        $request .= 'message=' . urlencode($this->message) . '&';
        if (!is_null($this->timestamp)) {
            $request .= 'timestamp=' . urlencode($this->timestamp) . '&';
        }
        $request .= 'hash=' . urlencode($this->getHash());
        if (self::$debug) {
            echo '<strong>Request data: </strong>' . $request . '</br>';
        }
        return $request;
    }

    /**
     * Returns the MD5 hash for the SMS
     * Hash Source: User|GatewayPassword|Type|Sender|Receiver|Message|Timestamp
     * @return string MD5-Hash
     */
    private function getHash()
    {
        $data = self::$user . '|' . self::$gateway_password . '|' . $this->type . '|';
        $data .= $this->sender . '|' . $this->receiver . '|' . $this->message;
        $data .= '|' . $this->timestamp;
        if (self::$debug) {
            echo "<strong>Hash source: </strong>$data</br>";
        }
        $hash = md5($data);
        if (self::$debug) {
            echo "<strong>Hash MD5: </strong>$hash</br>";
        }
        return $hash;
    }

    /**
     *  Reads the XML-Response of the SMS-Gateway
     * @param string $response The XML-Response of the gateway.
     * @link http://de3.php.net/manual/de/function.simplexml-load-string.php
     */
    private function readResponse($response)
    {
        $xml_start = stripos($response, '<?xml');
        $xml_end = strrpos($response, '>');
        if ($xml_start === false || $xml_end === false) {
            return false;
        }
        $response = substr($response, $xml_start, ($xml_end + 1) - strlen($response));
        if (self::$debug) {
            echo '<strong>XML-Response: </strong>' . htmlspecialchars($response) . '</br>';
        }
        $xml = simplexml_load_string($response);
        $this->responseStatusCode = (string)$xml->statusCode[0];
        $this->responseStatusText = (string)$xml->statusText[0];
        $this->responseMessageID = (string)$xml->messageId[0];
        $this->responseCost = (string)$xml->cost[0];
    }

    /**
     * Returns the status code of the response
     * @return integer Status code
     */
    public function getResponseStatusCode()
    {
        return $this->responseStatusCode;
    }

    /**
     * Returns the status text of the response
     * @return string Status text
     */
    public function getResponseStatusText()
    {
        return $this->responseStatusText;
    }

    /**
     * Returns the message id of the response
     * @return string Message-ID
     */
    public function getResponseMessageID()
    {
        return $this->responseMessageID;
    }

    /**
     * Returns the cost of the response
     * @return double Cost in EUR
     */
    public function getResponseCost()
    {
        return $this->responseCost;
    }
}

?>