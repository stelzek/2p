<?php

namespace onekit\AppBundle\Manager;

use Doctrine\ORM\EntityManager;
use onekit\AppBundle\Entity\Doctor;
use onekit\AppBundle\Entity\Input\CreatePatient;
use onekit\AppBundle\Entity\Input\SearchPatient;
use onekit\AppBundle\Entity\Patient;
use onekit\AppBundle\Entity\User;
use onekit\AppBundle\Repository\PatientRepository;
use Vivait\SettingsBundle\Services\SettingsChain;

class PatientManager extends ApiManager
{
    /**
     * @var AccountManager
     */
    protected $userManager;

    /**
     * @var DoctorManager
     */
    protected $doctorManager;

    /**
     * @var SettingsChain
     */
    protected $settings;

    /**
     * @var PatientRepository
     */
    protected $repo;

    /**
     * PatientManager constructor.
     *
     * @param EntityManager $entityManager
     * @param AccountManager $accountManager
     * @param DoctorManager $doctorManager
     * @param SettingsChain $settings
     */
    public function __construct(EntityManager $entityManager, AccountManager $accountManager, DoctorManager $doctorManager, SettingsChain $settings)
    {
        parent::__construct($entityManager);
        $this->userManager = $accountManager;
        $this->doctorManager = $doctorManager;
        $this->settings = $settings;
    }

    protected function setRepository()
    {
        $this->repo = $this->em->getRepository('AppBundle:Patient');
    }

    /**
     * @param Doctor $doctor
     * @param CreatePatient $createPatient
     * @return Patient
     */
    public function create(Doctor $doctor, CreatePatient $createPatient)
    {
        $patient = new Patient();
        $patient->setTitle($createPatient->title);
        $patient->setFirstName($createPatient->first_name);
        $patient->setLastName($createPatient->last_name);
        $patient->setInsuranceType($createPatient->insuranceType);
        $patient->setInsuranceCompany($createPatient->insuranceCompany);
        $patient->setGender($createPatient->gender);
        $patient->setEmail($createPatient->email);
        $patient->setPhone($createPatient->phone);
        /** @var User|null $user */
        $user = $createPatient->account_id ? $this->userManager->get($createPatient->account_id) : null;
        $patient->setAccount($user);
        $patient->setDoctor($doctor);
        $patient->setLanguage($createPatient->language);
        $patient->setNotifyEmail(isset($createPatient->notify_email) ? $createPatient->notify_email : $this->settings->get('notify_email'));
        $patient->setNotifySMS(isset($createPatient->notify_sms) ? $createPatient->notify_sms : $this->settings->get('notify_sms'));
        $this->em->persist($patient);
        $this->em->flush();
        return $patient;
    }

    /**
     * @param Patient $patient
     * @param CreatePatient $createPatient
     * @return User
     */
    public function update(Patient $patient, CreatePatient $createPatient)
    {
        $patient->setTitle($createPatient->title);
        $patient->setFirstName($createPatient->first_name);
        $patient->setLastName($createPatient->last_name);
        $patient->setInsuranceType($createPatient->insuranceType);
        $patient->setInsuranceCompany($createPatient->insuranceCompany);
        $patient->setGender($createPatient->gender);
        $patient->setEmail($createPatient->email);
        $patient->setPhone($createPatient->phone);
        /** @var User|null $user */
        $user = $createPatient->account_id ? $this->userManager->get($createPatient->account_id) : null;
        $patient->setAccount($user);
        $patient->setLanguage($createPatient->language);
        $patient->setNotifyEmail(isset($createPatient->notify_email) ? $createPatient->notify_email : $this->settings->get('notify_email'));
        $patient->setNotifySMS(isset($createPatient->notify_sms) ? $createPatient->notify_sms : $this->settings->get('notify_sms'));
        $this->em->flush($patient);
        return $patient;
    }

    public function getList()
    {
        $this->qb = $this->repo->getListQB();
        return $this;
    }

    /**
     * @param Doctor $doctor
     * @return $this
     */
    public function getDoctorList(Doctor $doctor)
    {
        $this->getList();
        $this->qb->andWhere('p.doctor = :doctor')->setParameter('doctor', $doctor);
        return $this;
    }

    /**
     * @param SearchPatient $searchPatient
     * @return $this
     */
    public function filter(SearchPatient $searchPatient)
    {
        if (trim($searchPatient->keyword)) {
            $this->qb
                ->andWhere('lower(p.firstName) LIKE lower(:keyword) OR lower(p.lastName) LIKE lower(:keyword)')
                ->setParameter('keyword', '%'.trim($searchPatient->keyword).'%');
        }
        return $this;
    }

    public function order($column, $direction)
    {
        $this->qb->orderBy('p.'.$column, $direction);
        return $this;
    }
}
