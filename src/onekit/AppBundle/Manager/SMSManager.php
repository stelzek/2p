<?php

namespace onekit\AppBundle\Manager;

use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use onekit\AppBundle\Entity\Appointment;
use onekit\AppBundle\Entity\NotificationEvent;
use onekit\AppBundle\Entity\SMSMessageTemplate;
use onekit\AppBundle\Entity\User;
use onekit\AppBundle\Handler\TimeHandler;
use onekit\AppBundle\Manager\SMSExpert;
use onekit\AppBundle\Manager\SMSExpert\SMSExpertSender;
use onekit\AppBundle\Manager\SMSExpert\SMSExpertSenderException;
use Swift_Mailer;
use Swift_Transport;
use Swift_Message;
use Vivait\SettingsBundle\Services\SettingsChain;


class SMSManager
{
    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var Swift_Mailer
     */
    protected $mailer;

    protected $transport;

    /**
     * @var \Doctrine\Common\Persistence\ObjectRepository
     */
    protected $eventRepository;

    /**
     * @var NotificationManager
     */
    protected $notificationManager;

    protected $repo;

    /**
     * @var SettingsChain
     */
    protected $settings;

    /**
     * @var TimeHandler
     */
    protected $timeHandler;

    protected function getNotifyEmail()
    {
        return $this->settings->get('notify_email');
    }

    protected function getNotifySMS()
    {
        return $this->settings->get('notify_sms');
    }

    protected function getNotificationEmail()
    {
        return $this->settings->get('notification_email');
    }

    public function __construct(EntityManagerInterface $em, NotificationManager $notificationManager, Swift_Mailer $mailer, Swift_Transport $transport, SettingsChain $settings, TimeHandler $timeHandler)
    {
        $this->em = $em;
        $this->notificationManager = $notificationManager;
        $this->mailer = $mailer;
        $this->transport = $transport;
        $this->settings = $settings;
        $this->timeHandler = $timeHandler;
        $this->eventRepository = $em->getRepository('AppBundle:NotificationEvent');
    }

    public function sendByAppointment(Appointment $appointment, NotificationEvent $notificationEvent)
    {
        $messageBodyTemp = null;

        /** @var SMSMessageTemplate $messageTemplate */
        $messageTemplate = $this->em->getRepository('AppBundle:SMSMessageTemplate')->findOneBy(array(
            'notificationEvent' => $notificationEvent,
            'doctor' => $appointment->getDoctor(),
        ));

        /** @var SMSMessageTemplate $defaultMessageTemplate */
        $defaultMessageTemplate = $this->em->getRepository('AppBundle:SMSMessageTemplate')->findOneBy(array(
            'notificationEvent' => $notificationEvent,
            'doctor' => null,
        ));
        if (!($messageTemplate || $defaultMessageTemplate)) {
            return false;
        }
        $messageBody = $messageTemplate ? $messageTemplate->getTranslatedBody($appointment->getPatient()->getLanguage()) : null;
        if (!$messageBody) {
            $messageBody = $defaultMessageTemplate ? $defaultMessageTemplate->getTranslatedBody($appointment->getPatient()->getLanguage()) : null;
        }
        if (!$messageBody) {
            return false;
        }

        $messageBodyTemp = $messageBody->getMessage();

        $patient = $appointment->getPatient();
        $time = $appointment->getStart()->format($appointment->getPatient()->getLanguage() == User::LANG_EN ? 'h:i a' : 'H:i');
        $fullDate = $appointment->getStart()->format('d.m.Y');
        $genderTitle = $patient->getTitle();
        $minutesLeft = $this->timeHandler->timeRemaining($appointment->getStart(), $appointment->getPatient()->getLanguage());
        $fullName = $patient->getFullName();

        $messageBodyTemp = preg_replace('{{fullName}}', $fullName, $messageBodyTemp);
        $messageBodyTemp = preg_replace('{{fullDate}}', $fullDate, $messageBodyTemp);
        $messageBodyTemp = preg_replace('{{time}}', $time, $messageBodyTemp);
        $messageBodyTemp = preg_replace('{{senderName}}', $appointment->getDoctor()->getTitle(), $messageBodyTemp);

        $messageBodyTemp = preg_replace('{{title}}', $genderTitle, $messageBodyTemp);
        $messageBodyTemp = preg_replace('{{firstname}}', $patient->getFirstName(), $messageBodyTemp);
        $messageBodyTemp = preg_replace('{{lastname}}', $patient->getLastName(), $messageBodyTemp);
        $messageBodyTemp = preg_replace('{{minutesLeft}}', $minutesLeft, $messageBodyTemp);
        $messageOut = substr($messageBodyTemp,0,140); // ;)
        $receiverNumber = $appointment->getPatient()->getPhone();

        $result = false;

        //if patient's phone in Germany and default transport is SMS
        if ($this->getNotifySMS() && $appointment->getPatient()->isGermanPhoneNumber() && $appointment->getDoctor()->isExpired()) {
            try {
                $sender = null;
                $type = 'standard';
                $sender = new SMSExpertSender();
                $sender->setSMSType($type);
                //if sms-expert.de SMSType change to expert, than it allow send international SMS (not only in Germany) and allow change From field
                if ($type == 'expert') {
                    $sender->setSender($appointment->getDoctor()->getTitle());
                }
                $sender->setReceiver($receiverNumber);
                $sender->setMessage($messageOut);
                $sender->send();
                $this->notificationManager->setNotification($appointment->getDoctor(), $appointment->getPatient(), $appointment, $notificationEvent->getAction(), $sender);
                $result = true;
            }
            catch (SMSExpertSenderException $e){ //TODO: change exceptions logic (send error message to MQ)
                echo '<strong>SMS Error: </strong>' . $e->getMessage() . '</br>';
            }
            catch (Exception $e){ //TODO: change exceptions logic (send error message to MQ)
                echo '<strong>Error: </strong>' . $e->getMessage() . '</br>';
            }

        }
        if ($this->getNotifyEmail()) {
            $sender = null;
            $message = Swift_Message::newInstance()
                ->setSubject('Notification from Patient Messenger')
                //->setFrom($appointment->getUser()->getEmail())
                ->setFrom($this->getNotificationEmail())
                ->setTo($appointment->getPatient()->getEmail())
                ->setBody($messageOut, "text/plain");
            $this->mailer->send($message);

            $sender = new SMSExpertSender();
            $sender->setMessage($messageOut);
            $sender->setSMSType('email');

            $spool = $this->mailer->getTransport()->getSpool();
            $spool->flushQueue($this->transport);
            $this->notificationManager->setNotification($appointment->getDoctor(), $appointment->getPatient(), $appointment, $notificationEvent->getAction(), $sender);
            $result = true;
        }
        return $result;
    }
}

