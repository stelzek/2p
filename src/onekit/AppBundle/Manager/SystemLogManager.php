<?php
namespace onekit\AppBundle\Manager;

use Doctrine\Common\Persistence\ObjectManager;
use onekit\AppBundle\Entity\SystemLog;
use onekit\AppBundle\Entity\User;

class SystemLogManager
{
    private $om;

    function __construct(ObjectManager $om)
    {
        // TODO: Imploment __construct() method.
        $this->om = $om;
    }

    public function setTransferLog(User $sender, User $receiver, $transactionAmount)
    {
        try {
            $systemLog = new SystemLog();
            $systemLog->setSender($sender);
            $systemLog->setReceiver($receiver);
            $systemLog->setAction(SystemLog::TRANSFER_ACTION);
            $systemLog->setTransactionAmount($transactionAmount);

            $this->update($systemLog, true);
        } catch(\Exception $e) {
            return false;
        }
    }

    public function setUserLog(User $sender, User $receiver, $action)
    {
        try {
            $systemLog = new SystemLog();
            $systemLog->setSender($sender);
            $systemLog->setReceiver($receiver);
            $systemLog->setAction($action);

            $this->update($systemLog, true);
        } catch(\Exception $e) {
            return false;
        }

        return true;
    }



    public function update(SystemLog $systemLog, $flush = false)
    {
        $this->om->persist($systemLog);
        if ($flush) {
            $this->om->flush();
        }
    }

    public function delete(SystemLog $systemLog)
    {
        $this->om->remove($systemLog);
        $this->om->flush();
    }

    public function disable(SystemLog $systemLog)
    {
        $systemLog->setEnabled(false);
        $this->update($systemLog, true);
    }
}