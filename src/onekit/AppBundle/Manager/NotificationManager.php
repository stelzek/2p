<?php

namespace onekit\AppBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use onekit\AppBundle\Entity\Appointment;
use onekit\AppBundle\Entity\Doctor;
use onekit\AppBundle\Entity\Notification;
use onekit\AppBundle\Entity\Patient;
use onekit\AppBundle\Manager\SMSExpert\SMSExpertSender;
use Symfony\Component\DependencyInjection\ContainerInterface;

class NotificationManager
{
    /**
     * @var ContainerInterface
     */
    protected $container;
    protected $repo;
    private $transactionPrice;

    public function __construct(ContainerInterface $container, EntityManagerInterface $em, $transactionPrice)
    {
        $this->em = $em;
        $this->transactionPrice = $transactionPrice;
        $this->container = $container;
        $this->repo = $em->getRepository('AppBundle:Notification');
    }

    public function loadById($id)
    {
        return $this->repo->findOneBy(['id' => $id]);
    }

    /**
     * @param Doctor $doctor
     * @param Patient $patient
     * @param Appointment|null $appointment
     * @param null $action
     * @param SMSExpertSender|null $sender
     * @return array
     */
    public function setNotification(Doctor $doctor, Patient $patient, Appointment $appointment = null, $action = null, SMSExpertSender $sender = null)
    {
        $status = true;

        $costIn = null;
        if ($appointment && $appointment->getNotifySMS() && $sender->getResponseStatusCode() == 200) {
             $costIn = $this->transactionPrice;
        }

        $log = new Notification();
        $log->setDoctor($doctor);
        $log->setPatient($patient);
        $log->setAppointment($appointment);
        $log->setCostIn($costIn);
        if ($sender) {
            $log->setCostOut($sender->getResponseCost());
            $log->setSmsStatus($sender->getResponseStatusCode());
            $log->setSmsType($sender->getSMSType());
            $log->setSmsText($sender->getMessage());
        }
        $log->setCreated(new \DateTime());
        $log->setAction($action);
        //$log->setIp($this->container->get('request')->getClientIp());
        $this->update($log, true);

        return ['status' => $status, 'transaction' => $costIn, 'action' => $action];
    }


    public function update(Notification $log, $flush = false)
    {
        $this->em->persist($log);
        if ($flush) {
            $this->em->flush();
        }
        $this->MQAppointmentUpdate($log);
    }

    public function delete(Notification $log)
    {
        $this->MQAppointmentUpdate($log);
        $this->em->remove($log);
        $this->em->flush();
    }

    public function disable(Notification $log)
    {
        $log->setEnabled(false);
        $this->MQAppointmentUpdate($log);
        $this->update($log, true);

    }

    private function MQAppointmentUpdate(Notification $log)
    {
        $dataEntry = [];
        if ($log->getEnabled() == false) {
            $dataEntry['delete'] = true; //to inform subscribers to remove or not from calendar
        }
        $dataEntry['id'] = 'patient' . $log->getPatient()->getId();
        $dataEntry['type'] = 'appointment';
        $dataEntry['alertType'] = 'success';
        $dataEntry['action'] = $this->container->get('translator')->trans(sprintf('notifications.%s.title', $log->getAction()));
        $dataEntry['appointment_id'] = $log->getAppointment() ? $log->getAppointment()->getId() : 0;
        $dataEntry['notify_sms'] = $log->getAppointment() ? $log->getAppointment()->getNotifySMS() : 0;
        $dataEntry['notify_email'] = $log->getAppointment() ? $log->getAppointment()->getNotifyEmail() : 0;
        $dataEntry['start'] = $log->getAppointment() ? $log->getAppointment()->getStart()->format('Y-m-d H:i') : new \DateTime();
    }
}

