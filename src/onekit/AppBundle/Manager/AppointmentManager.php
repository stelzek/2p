<?php

namespace onekit\AppBundle\Manager;

use Doctrine\ORM\EntityManager;
use onekit\AppBundle\Entity\Appointment;
use onekit\AppBundle\Entity\User;
use onekit\AppBundle\Form\Type\AppointmentFormType;
use onekit\AppBundle\Repository\AppointmentRepository;
use onekit\AppBundle\Repository\UserRepository;
use onekit\AppBundle\Entity\Output\Result as RestResult;
use Symfony\Component\DependencyInjection\ContainerAware;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RequestStack;
use JMS\DiExtraBundle\Annotation as DI;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;


class AppointmentManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var AppointmentRepository
     */
    protected $repo;

    /**
     * @var UserRepository
     */
    protected $userRepo;

    /**
     * @var TokenStorage
     */
    private $securityContext;

    private $formFactory;

    /**
     * @var RequestStack
     */
    private $requestStack;

    public function __construct($em, FormFactory $formFactory, RequestStack $requestStack, TokenStorage $tokenStorage)
    {
        $this->em = $em;
        $this->repo = $this->em->getRepository('AppBundle:Appointment');
        $this->userRepo = $this->em->getRepository('AppBundle:User');
        $this->formFactory = $formFactory;
        $this->requestStack = $requestStack;
        $this->securityContext = $tokenStorage;
    }

    public function loadById($id)
    {
        return $this->repo->findOneBy(['id' => $id]);
    }

    public function loadByUser($user)
    {
        $qb = $this->repo->findOneByUser($user);
        return $qb;
    }

    public function getList()
    {
        $request = $this->requestStack->getCurrentRequest();

        $startDate = $request->query->get('start') ? new \DateTime($request->query->get('start')) : null;
        $finishDate = $request->query->get('finish') ? new \DateTime($request->query->get('finish')) : null;

        /** @var User $currentUser */
        $currentUser = $this->securityContext->getToken()->getUser();

        if ($currentUser->hasRole('ROLE_SUPER_ADMIN')) {
            $qb = $this->getListForAdmin(true, $startDate, $finishDate);
        } elseif ($currentUser->hasRole('ROLE_USER')) {
            $qb = $this->getListForDoctor($currentUser, true, $startDate, $finishDate);
        } else {
            $qb = $this->getListForPatient($currentUser, true, $startDate, $finishDate);
        }

        $orderBy =  $request->get('orderBy') ? $request->get('orderBy') : 'start';
        $orderDir = $request->get('orderDir') ? $request->get('orderDir') :'ASC';
        $page = $request->get('page') ? $request->get('page') : 1;
        $limit = $request->get('limit') ? $request->get('limit') : 50;

        $qb
            ->orderBy("a.".$orderBy, $orderDir)
            ->setMaxResults($limit)
            ->setFirstResult(($page - 1) * $limit);

        return $data = $qb->getQuery()->getResult();
    }

    protected function getListForAdmin($isEnabled, \DateTime $startDate = null, \DateTime $finishDate = null)
    {
        $qb = $this->repo->getAppointmentListQB(
            $isEnabled,
            $startDate,
            $finishDate
        );
        return $qb;
    }

    protected function getListForDoctor(User $doctor, $isEnabled, \DateTime $startDate = null, \DateTime $finishDate = null)
    {
        $qb = $this->repo->getAppointmentListQB(
            $isEnabled,
            $startDate,
            $finishDate
        );
        $qb->andWhere('a.user = :doctor')->setParameter('doctor', $doctor);
        $qb->orWhere(
            $qb->expr()->in(
                'a.user',
                $this->userRepo->getAssistantListQB($doctor, $isEnabled)->getDQL()
            )
        );
        return $qb;
    }

    protected function getListForPatient(User $patient, $isEnabled, \DateTime $startDate = null, \DateTime $finishDate = null)
    {
        $qb = $this->repo->getAppointmentListQB(
            $isEnabled,
            $startDate,
            $finishDate
        );
        $qb->andWhere('a.patient = :patient')->setParameter('patient', $patient);
        return $qb;
    }


    /**
     * @param $id
     * @return Appointment|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function getEnabledAppointmentById($id)
    {
        return $this->repo->getAppointmentByIdQB($id, true)->getQuery()->getOneOrNullResult();
    }

    public function disabledAppointmentById($id)
    {
        $appointment = $this->repo->getAppointmentByIdQB($id, true)->getQuery()->getOneOrNullResult();
        if ($appointment) {
            $this->disable($appointment);
        }
        return $appointment;
    }

    public function createAppointment($appointment = null)
    {
        if (!$appointment) {
            $appointment = new Appointment();
        }

        $form = $this->formFactory->create(new AppointmentFormType(), $appointment);
        $form->handleRequest($this->requestStack->getCurrentRequest());
        if ($form->isValid()) {
            $this->update($appointment, true);
            return $appointment;
        } else {
            return new RestResult($form->getErrors(true, true), 'error', 1);
        }
    }


    public function update(Appointment $appointment, $flush = false)
    {
        $this->em->persist($appointment);
        if ($flush) {
            $this->em->flush();
        }
    }

    public function delete(Appointment $appointment)
    {
        $this->em->remove($appointment);
        $this->em->flush();

    }

    public function disable(Appointment $appointment)
    {
        $appointment->setEnabled(false);
        $this->update($appointment,true);
    }
}

