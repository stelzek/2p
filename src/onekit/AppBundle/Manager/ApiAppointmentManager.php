<?php
namespace onekit\AppBundle\Manager;


use Doctrine\ORM\EntityManager;
use onekit\AppBundle\Entity\Appointment;
use onekit\AppBundle\Entity\Doctor;
use onekit\AppBundle\Entity\Input\CreateAppointment;
use onekit\AppBundle\Entity\Input\DateFilter;
use onekit\AppBundle\Entity\Patient;
use onekit\AppBundle\Repository\AppointmentRepository;

class ApiAppointmentManager extends ApiManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var DoctorManager
     */
    protected $doctorManager;

    /**
     * @var PatientManager
     */
    protected $patientManager;

    /**
     * @var AppointmentRepository
     */
    protected $repo;

    public function __construct(EntityManager $em, DoctorManager $doctorManager, PatientManager $patientManager)
    {
        parent::__construct($em);
        $this->doctorManager = $doctorManager;
        $this->patientManager = $patientManager;
    }

    public function create(Patient $patient, CreateAppointment $createAppointment)
    {
        $appointment = new Appointment();
        $appointment->setDoctor($patient->getDoctor());
        $appointment->setPatient($patient);
        $appointment->setStart($createAppointment->start_time);
        $appointment->setEnd($createAppointment->end_time);
        $appointment->setNotifySMS(is_null($createAppointment->notify_sms) ? $patient->getNotifySMS() : $createAppointment->notify_sms);
        $appointment->setNotifyEmail(is_null($createAppointment->notify_email) ? $patient->getNotifyEmail() : $createAppointment->notify_email);
        $this->em->persist($appointment);
        $this->em->flush();
        return $appointment;
    }

    /**
     * @param Appointment $appointment
     * @param CreateAppointment $createAppointment
     * @return Appointment
     */
    public function update(Appointment $appointment, CreateAppointment $createAppointment)
    {
        $appointment->setStart($createAppointment->start_time);
        $appointment->setEnd($createAppointment->end_time);
        $this->em->persist($appointment);
        $this->em->flush();
        return $appointment;
    }

    /**
     * @return $this
     */
    public function getList()
    {
        $this->qb = $this->repo->getListQB();
        return $this;
    }

    /**
     * @param Doctor $doctor
     * @return $this
     */
    public function getDoctorList(Doctor $doctor)
    {
        $this->getList();
        $this->qb->andWhere('a.doctor = :doctor')->setParameter('doctor', $doctor);
        return $this;
    }

    /**
     * @param Patient $patient
     * @return $this
     */
    public function getPatientList(Patient $patient)
    {
        $this->getList();
        $this->qb->andWhere('a.patient = :patient')->setParameter('patient', $patient);
        return $this;
    }

    public function filterByDate(\DateTime $start, \DateTime $end)
    {
        $this->qb
            ->andWhere('a.start >= :start')->setParameter('start', $start)
            ->andWhere('a.end <= :end')->setParameter('end', $end)
        ;
        return $this;
    }

    public function order($column, $direction)
    {
        $this->qb->orderBy('a.'.$column, $direction);
        return $this;
    }

    protected function setRepository()
    {
        $this->repo = $this->em->getRepository('AppBundle:Appointment');
    }
}