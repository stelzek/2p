<?php
namespace onekit\AppBundle\Manager;

use Doctrine\ORM\EntityManagerInterface;
use onekit\AppBundle\Entity\NotificationEvent;
use onekit\AppBundle\Repository\ScheduleRepository;
use onekit\AppBundle\Entity\Appointment;
use onekit\AppBundle\Entity\Schedule;

class ScheduleManager extends ApiManager
{

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var ScheduleRepository
     */
    protected $repo;

    public function getList()
    {
        $this->qb = $this->repo->getListQB();
        return $this;
    }

    public function getAppointmentList(Appointment $appointment)
    {
        $this->getList();
        $this->qb->andWhere('s.appointment = :appointment')->setParameter('appointment', $appointment);
        return $this;
    }

    public function update(Schedule $Schedule, $flush = false)
    {
        $this->em->persist($Schedule);
        if ($flush) {
            $this->em->flush();
        }
    }

    public function assignToAppointment(Appointment $appointment, $timeOffset)
    {
        $schedule = new Schedule();
        /** @var NotificationEvent $notificationEvent */
        $notificationEvent = $this->em->getRepository('AppBundle:NotificationEvent')->findOneBy(['action' => 'api_post_appointment_notify']);
        $schedule->setNotificationEvent($notificationEvent);
        $schedule->setAppointment($appointment);
        $schedule->setDoctor($appointment->getDoctor());
        $schedule->setExecute(new \DateTime(sprintf('%s %s', $appointment->getStart()->format('Y-m-d H:i:s'), $timeOffset)));
        $this->em->persist($schedule);
        $this->em->flush();
    }

    public function deleteAppointmentList(Appointment $appointment)
    {
        $this->repo->clearAppointmentQB($appointment)->getQuery()->execute();
    }

    public function updateAppointmentList(Appointment $appointment, $delete = true)
    {
        if ($delete) {
            $this->deleteAppointmentList($appointment);
        }
        $this->assignToAppointment($appointment, '-1 day');
        $this->assignToAppointment($appointment, '-1 hour');
        $this->assignToAppointment($appointment, '-25 minutes');
    }

    protected function setRepository()
    {
        $this->repo = $this->em->getRepository('AppBundle:Schedule');
    }

    public function order($column, $direction)
    {
        $this->qb->orderBy('s.'.$column, $direction);
        return $this;
    }
}

