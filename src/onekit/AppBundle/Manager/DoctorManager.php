<?php
namespace onekit\AppBundle\Manager;


use Doctrine\ORM\EntityManager;
use onekit\AppBundle\Entity\Doctor;
use onekit\AppBundle\Entity\Input\CreateDoctor;
use onekit\AppBundle\Entity\Input\DoctorAddress;
use onekit\AppBundle\Entity\Input\DoctorContact;
use onekit\AppBundle\Entity\Input\DoctorTimes;
use onekit\AppBundle\Entity\User;
use onekit\AppBundle\Repository\DoctorRepository;

use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use JMS\Serializer\Serializer as JMSSerializer;


class DoctorManager extends ApiManager
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var AccountManager
     */
    protected $userManager;

    /**
     * @var DoctorRepository
     */
    protected $repo;

    /**
     * @var JMSSerializer
     */
    protected $serializer;

    public function __construct(EntityManager $em, AccountManager $accountManager, JMSSerializer $serializer)
    {
        parent::__construct($em);
        $this->userManager = $accountManager;
        $this->serializer = $serializer;
    }

    protected function serialize($value)
    {
        return $this->serializer->serialize($value, 'json');
    }

    protected function deserialize($value, $type)
    {
        return $this->serializer->deserialize($value, $type, 'json');
    }

    public function create(CreateDoctor $createDoctor)
    {
        $doctor = new Doctor();
        $doctor->setTitle($createDoctor->title);
        $doctor->setPhone($createDoctor->phone);
        /** @var User $user */
        $user = $createDoctor->account_id ? $this->userManager->get($createDoctor->account_id) : null;
        $doctor->setAccount($user);
        $this->em->persist($doctor);
        $this->em->flush();
        return $doctor;
    }

    public function update(Doctor $doctor, CreateDoctor $createDoctor)
    {
        $doctor->setTitle($createDoctor->title);
        $doctor->setPhone($createDoctor->phone);
        /** @var User $user */
        $user = $createDoctor->account_id ? $this->userManager->get($createDoctor->account_id) : null;
        $doctor->setAccount($user);
        $this->em->flush($doctor);
        return $doctor;
    }

    public function setContact(Doctor $doctor, DoctorContact $contact)
    {
        $doctor->setPhone($contact->phone);
        $doctor->setEmail($contact->email);
        $this->em->flush($doctor);
        return $doctor;
    }

    public function setTimes(Doctor $doctor, DoctorTimes $times)
    {
        $doctor->setTimes($this->serialize($times));
        $this->em->flush($doctor);
        return $doctor;
    }

    /**
     * @param Doctor $doctor
     * @return DoctorTimes
     */
    public function getTimes(Doctor $doctor)
    {
        $times = $this->deserialize($doctor->getTimes(), 'onekit\AppBundle\Entity\Input\DoctorTimes');
        return $times;
    }

    public function validateAppointments(Doctor $doctor)
    {
        $times = $this->getTimes($doctor);
        return $times->validate;
    }

    public function setAddress(Doctor $doctor, DoctorAddress $address)
    {
        $doctor->setAddress(json_encode($address));
        $this->em->flush($doctor);
        return $doctor;
    }

    /**
     * @param Doctor $doctor
     * @return DoctorAddress
     */
    public function getAddress(Doctor $doctor)
    {
        $serializer = new Serializer(array(new ObjectNormalizer()), array(new JsonEncoder()));
        return $serializer->deserialize($doctor->getAddress(), 'onekit\AppBundle\Entity\Input\DoctorAddress', 'json');
    }

    /**
     * @return $this
     */
    public function getList()
    {
        $this->qb = $this->repo->getListQB();
        return $this;
    }

    public function order($column, $direction)
    {
        $this->qb->orderBy('d.'.$column, $direction);
        return $this;
    }

    protected function setRepository()
    {
        $this->repo = $this->em->getRepository('AppBundle:Doctor');
    }
}