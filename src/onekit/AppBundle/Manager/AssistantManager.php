<?php

namespace onekit\AppBundle\Manager;

use Doctrine\ORM\EntityManager;
use onekit\AppBundle\Entity\Assistant;
use onekit\AppBundle\Entity\Doctor;
use onekit\AppBundle\Entity\Input\CreateAssistant;
use onekit\AppBundle\Entity\User;
use onekit\AppBundle\Repository\AssistantRepository;

class AssistantManager extends ApiManager
{
    /**
     * @var AccountManager
     */
    protected $userManager;

    /**
     * @var DoctorManager
     */
    protected $doctorManager;

    /**
     * @var AssistantRepository
     */
    protected $repo;

    /**
     * AssistantManager constructor.
     *
     * @param EntityManager $entityManager
     * @param AccountManager $accountManager
     * @param DoctorManager $doctorManager
     */
    public function __construct(EntityManager $entityManager, AccountManager $accountManager, DoctorManager $doctorManager)
    {
        parent::__construct($entityManager);
        $this->userManager = $accountManager;
        $this->doctorManager = $doctorManager;
    }

    protected function setRepository()
    {
        $this->repo = $this->em->getRepository('AppBundle:Assistant');
    }

    /**
     * @param Doctor $doctor
     * @param CreateAssistant $createAssistant
     * @return Assistant
     */
    public function create(Doctor $doctor, CreateAssistant $createAssistant)
    {
        $assistant = new Assistant();
        $assistant->setTitle($createAssistant->title);
        $assistant->setEdit($createAssistant->edit);
        $assistant->setManageAppointments($createAssistant->manage_appointments);
        $assistant->setManagePatients($createAssistant->manage_patients);
        $assistant->setCheckBalance($createAssistant->check_balance);
        $assistant->setMakePayment($createAssistant->make_payment);
        /** @var User|null $user */
        $user = $createAssistant->account_id ? $this->userManager->get($createAssistant->account_id) : null;
        $assistant->setAccount($user);
        $assistant->setDoctor($doctor);
        $this->em->persist($assistant);
        $this->em->flush();
        return $assistant;
    }

    public function update(Assistant $assistant, CreateAssistant $createAssistant)
    {
        /** @var User|null $user */
        $assistant->setTitle($createAssistant->title);
        $assistant->setEdit($createAssistant->edit);
        $assistant->setManageAppointments($createAssistant->manage_appointments);
        $assistant->setManagePatients($createAssistant->manage_patients);
        $assistant->setCheckBalance($createAssistant->check_balance);
        $assistant->setMakePayment($createAssistant->make_payment);
        $user = $createAssistant->account_id ? $this->userManager->get($createAssistant->account_id) : null;
        $assistant->setAccount($user);
        $this->em->flush($assistant);
        return $assistant;
    }

    public function getList()
    {
        $this->qb = $this->repo->getListQB();
        return $this;
    }

    /**
     * @param Doctor $doctor
     * @return $this
     */
    public function getDoctorList(Doctor $doctor)
    {
        $this->getList();
        $this->qb->andWhere('da.doctor = :doctor')->setParameter('doctor', $doctor);
        return $this;
    }


    public function order($column, $direction)
    {
        $this->qb->orderBy('da.'.$column, $direction);
        return $this;
    }
}
