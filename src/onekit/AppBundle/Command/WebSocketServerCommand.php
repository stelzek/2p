<?php

namespace onekit\AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

use onekit\AppBundle\PubSub;

class WebSocketServerCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('websocket:listen')
            ->setDescription('Launch WebSocket server for real-time update');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $port = 9200;
        $zmqPort = 8001;
        $output->writeln("Starting WebSocket server");
        $output->writeln("Listening WebSocket on port " . $port);
        $output->writeln("Listening ZMQ on port " . $zmqPort);

        $pubsub = new PubSub;

        $loop = \React\EventLoop\Factory::create();
        $context = new \React\ZMQ\Context($loop);
        $socket = $context->getSocket(\ZMQ::SOCKET_PULL);
        $socket->bind('tcp://127.0.0.1:'.$zmqPort);
        $socket->on('message', array($pubsub, 'onCurrentUpdate'));
        $socket->on('error', function ($e) {
            //var_dump($e->getMessage());
        });


        $webSock = new \React\Socket\Server($loop);
        $webSock->listen($port, '0.0.0.0');
        $webServer = new \Ratchet\Server\IoServer(
            new \Ratchet\Http\HttpServer(
                new \Ratchet\WebSocket\WsServer(
                    new \Ratchet\Wamp\WampServer(
                        $pubsub
                    )
                )
            ),
            $webSock
        );
        $loop->run();


    }
}
