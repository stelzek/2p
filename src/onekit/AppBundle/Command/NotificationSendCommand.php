<?php

namespace onekit\AppBundle\Command;

use DateInterval;
use onekit\AppBundle\Entity\Log;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use DateTime;

class NotificationSendCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('notification:send')
            ->setDescription('Notifications send once');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
            $mailer = $this->getContainer()->get('mailer');
            $defaultNotificationTransport = $this->getContainer()->getParameter('default_notification_transport');
            $adminEmail = $this->getContainer()->getParameter('admin_email');
            if ($defaultNotificationTransport == 'email') {
                $messageOut = 'Test message sent from Command Line';
                $subject ='Notification from Patient Messenger';
                $message = Swift_Message::newInstance()
                    ->setSubject($subject)
                    ->setFrom($adminEmail)
                    ->setTo($adminEmail)
                    ->setContentType("text/plain")
                    ->setBody($messageOut);
                $result = $mailer->send($message);
                $output->writeln($result);
            }
            $output->writeln("Done"); //nothing to do


        }


}
