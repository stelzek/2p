<?php

namespace onekit\AppBundle\Command;

use DateInterval;
use Doctrine\ORM\EntityManagerInterface;
use onekit\AppBundle\Entity\Log;
use onekit\AppBundle\Entity\Schedule;
use onekit\AppBundle\Manager\ScheduleManager;
use onekit\AppBundle\Manager\SMSManager;
use onekit\AppBundle\Repository\ScheduleRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use DateTime;
use Symfony\Component\HttpKernel\Kernel;

class ScheduleServerCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('schedule:start')
            ->setDescription('Notifications daemon');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        ob_start();

        $interval = 5; //seconds

        /** @var EntityManagerInterface $em */
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        /** @var ScheduleManager $managerSchedule */
        $managerSchedule = $this->getContainer()->get('api.manager.schedule');
        /** @var SMSManager $managerSMS */
        $managerSMS = $this->getContainer()->get('manager.sms');

        /** @var ScheduleRepository $scheduleRepo */
        $scheduleRepo = $em->getRepository('AppBundle:Schedule');

        /** @var Kernel $realKernel */
        $realKernel = $this->getContainer()->get('kernel');
        $output->writeln(sprintf('Environment: %s', $realKernel->getEnvironment()), OutputInterface::OUTPUT_PLAIN);
        $output->writeln(sprintf('Database: %s', $em->getConnection()->getDatabase()));
        $output->writeln('========================================================================');
        ob_flush();
        ob_start();

        $qb = $scheduleRepo->createQueryBuilder('s');
        $qb->andWhere("s.execute < :expDate")->setParameter('expDate', new \DateTime());
        /** @var Schedule $schedule */
        foreach ($qb->getQuery()->getResult() as $schedule) {
            $output->writeln('Remove expired notification: ' . $schedule->getExecute()->format('Y-m-d H:i:s'));
            $managerSchedule->delete($schedule);
        }

        $qb = $scheduleRepo->createQueryBuilder('s');
        $qb->andWhere("s.execute < :currentTime");
        $qb->setParameter('currentTime', new \DateTime());
        $qb->orderBy("s.execute", "asc");

        while (true) {
            $schedules = $qb->getQuery()->useQueryCache(false)->useResultCache(false)->getResult();
            if (count($schedules)) {
                $output->writeln(sprintf('Pool size: %s', count($schedules)));
                $output->writeln('========================================================================');
                ob_flush();
                ob_start();
                foreach ($schedules as $schedule) {
                    $appointment = $schedule->getAppointment();
                    $output->writeln(sprintf('Action: %s', $schedule->getNotificationEvent()->getAction()));
                    $output->writeln(sprintf('Appointment ID: %s', $schedule->getAppointment()->getId()));
                    $output->writeln(sprintf('Reception time: %s', $schedule->getAppointment()->getStart()->format('Y-m-d H:i:s')));
                    $output->writeln(sprintf('Schedule time: %s', $schedule->getExecute()->format('Y-m-d H:i:s')));
                    $output->writeln(sprintf('Doctor: %s', $schedule->getDoctor()->getTitle()));
                    $output->writeln(sprintf('Patient: %s', $schedule->getAppointment()->getPatient()->getTitle()));
                    $output->writeln(sprintf('SMS notification: %s', $appointment->getNotifySMS() ? 'on' : 'off'));
                    if ($appointment->getNotifySMS()) {
                        $output->writeln(sprintf('Phone: %s', $schedule->getAppointment()->getPatient()->getPhone()));
                    }
                    $output->writeln(sprintf('E-mail notification: %s', $appointment->getNotifyEmail() ? 'on' : 'off'));
                    if ($appointment->getNotifyEmail()) {
                        $output->writeln(sprintf('E-mail: %s', $schedule->getAppointment()->getPatient()->getEmail()));
                    }
                    if ($managerSMS->sendByAppointment($appointment, $schedule->getNotificationEvent())) {
                        $managerSchedule->delete($schedule);
                    }
                    $output->writeln('========================================================================');
                    ob_flush();
                    ob_start();
                }
            }
            sleep($interval);
        }
        $output->writeln("Done"); //nothing to do
    }


}
