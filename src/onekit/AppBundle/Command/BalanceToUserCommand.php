<?php

namespace onekit\AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;


class BalanceToUserCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this
            ->setName('balance:add')
            ->setDescription('Change balance for user.')
            ->addArgument(
                'user',
                InputArgument::OPTIONAL,
                'Username'
            )
            ->addArgument(
                'amount',
                InputArgument::OPTIONAL,
                'Add amount to balance'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $username = $input->getArgument('user');
        $amount = $input->getArgument('amount');
        if ($username && $amount) {
            $user = $em->getRepository('AppBundle:User')->findOneBy(['username' => $username]);
            $currentAmount = $user->getAmount();
            $amountAfter = $currentAmount + $amount; //put into config
            $user->setAmount($amountAfter);
            $em->persist($user);
            $em->flush();
            $output->writeln("Added ".$amount);
            $output->writeln("Now user ".$username." have ".$amountAfter." credits");
        } else {
            $output->writeln("Not enough arguments");
        }


    }


}
