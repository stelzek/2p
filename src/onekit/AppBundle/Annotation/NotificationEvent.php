<?php
namespace onekit\AppBundle\Annotation;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ConfigurationAnnotation;

/**
 * Class NotificationEvent
 * @package onekit\AppBundle\Annotation
 *
 * @Annotation
 */
class NotificationEvent extends ConfigurationAnnotation
{
    protected $action;

    public function setValue($value)
    {
        $this->action = $value;
    }

    public function getAction()
    {
        return $this->action;
    }


    public function allowArray()
    {
        return false;
    }

    public function getAliasName()
    {
        return 'NotificationEvent';
    }
}