Feature: Testing patients
  Scenario: Get full Patient list by admin.
    Given I am an "admin" with password "admin"
    When I send a GET request to "api/doctors/1/patients"
    And the response should be successful
    And the JSON node "data" should exist
    And count of the JSON node "data" should be equal "3"

  Scenario: Get limited Patient list by admin
    Given I am an "admin" with password "admin"
    When I send a GET request to "api/doctors/1/patients?limit=1"
    And the response should be successful
    And the JSON node "data" should exist
    And count of the JSON node "data" should be equal "1"

  Scenario: Anonymous request
    Given I am anonymous user
    When I send a GET request to "api/doctors/1/patients"
    Then the response status code should be 401

  Scenario: Anonymous user try to search patients
    Given I am anonymous user
    And I set "Content-Type" header equal to "application/json"
    When I send a POST request to "api/doctors/1/patients/search" with body:
    """
    {
        "keyword": "Storm"
    }
    """
    Then the response status code should be 401

  Scenario: A patient try to search patients
    Given I am an patient with password "admin"
    And I set "Content-Type" header equal to "application/json"
    When I send a POST request to "api/doctors/1/patients/search" with body:
    """
    {
        "keyword": "Storm"
    }
    """
    Then the response status code should be 403

  Scenario: Admin searches patients with keyword like "Storm"
    Given I am an admin with password "admin"
    And I set "Content-Type" header equal to "application/json"
    When I send a POST request to "api/doctors/1/patients/search" with body:
    """
    {
        "keyword": "Storm"
    }
    """
    Then the response status code should be 200
    And count of the JSON node "data" should be equal 2

  Scenario: Admin searches patients with keyword like "Anna"
    Given I am an admin with password "admin"
    And I set "Content-Type" header equal to "application/json"
    When I send a POST request to "api/doctors/1/patients/search" with body:
    """
    {
        "keyword": "Anna"
    }
    """
    Then the response status code should be 200
    And count of the JSON node "data" should be equal 1

  Scenario: Anon gets 1 patient
    Given I am anonymous user
    When I get 1 patient
    Then the response status code should be 401

  Scenario: User gets 1 patient
    Given I am an patient with password "admin"
    When I get 1 patient
    Then the response status code should be 403

  Scenario: Admin gets 1 patient
    Given I am an admin with password "admin"
    When I get 1 patient
    Then the response status code should be 200
    And the JSON node "data.first_name" should be equal to "Brad"

  Scenario: Add a Patient by admin.
    Given I am an admin with password "admin"
    And I set "Content-Type" header equal to "application/json"
    When I send a POST request to "api/doctors/1/patients" with body:
    """
    {
        "title": "Mr.",
        "first_name": "Sam",
        "last_name": "Smith",
        "phone": "123123123123",
        "gender": 1
    }
    """
    Then the response status code should be 201

  Scenario: Check incremented list by admin
    Given use auth token
    When I send a GET request to "api/doctors/1/patients"
    And the response should be successful
    And the JSON node "data" should exist
    And count of the JSON node "data" should be equal "4"

#  Scenario: Unique user validation on create patient
#    Given use auth token
#    And I set "Content-Type" header equal to "application/json"
#    When I send a POST request to "api/patients" with body:
#    """
#    {
#        "username": "testPatientCreate",
#        "email": "testPatientCreate@2p-med.de",
#        "password": "123123123"
#    }
#    """
#    Then the response status code should be 400

#  Scenario: Short password validation on create patient
#    Given use auth token
#    And I set "Content-Type" header equal to "application/json"
#    When I send a POST request to "api/patients" with body:
#    """
#    {
#        "username": "testPatientCreateUnique",
#        "email": "testPatientCreateUnique@2p-med.de",
#        "password": "123"
#    }
#    """
#    Then the response status code should be 400

  Scenario: Security access check on create patient
    Given I am an patient with password "admin"
    And I set "Content-Type" header equal to "application/json"
    When I send a POST request to "api/doctors/1/patients" with body:
    """
    {
        "title": "Mr.",
        "first_name": "Sam",
        "last_name": "Smith",
        "phone": "123123123123",
        "gender": 1
    }
    """
    Then the response status code should be 403

  Scenario: Auth check on create patient
    Given I am anonymous user
    And I set "Content-Type" header equal to "application/json"
    When I send a POST request to "api/doctors/1/patients" with body:
    """
    {
        "title": "Mr.",
        "first_name": "Sam",
        "last_name": "Smith",
        "phone": "123123123123",
        "gender": 1
    }
    """
    Then the response status code should be 401

