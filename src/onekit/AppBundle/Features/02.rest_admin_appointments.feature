Feature: Testing appointments
  Scenario: Get appointment list as anonymous
    Given I am anonymous user
    When get appointments
    Then the response status code should be 401

  Scenario: Get appointment list by a non admin user
    Given I am a "doctor" with password "admin"
    When get appointments
    Then the response status code should be 403

  Scenario: Get limited Appointment list by admin.
    Given I am an "admin" with password "admin"
    When get appointments from page 1 with limit 5
    And the response should be successful
    And the JSON node "data" should exist
    And count of the JSON node "data" should be equal 5
