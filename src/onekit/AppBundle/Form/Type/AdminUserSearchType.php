<?php

namespace onekit\AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class AdminUserSearchType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('POST')
            ->add(
                'user',
                'entity_typeahead',
                [
                    'class' => 'AppBundle:User',
                    'route' => 'admin_user_autocomplete',
                    'render' => 'value',
                    'callback' => 'unescapeHtml',
                    'required' => true,
                    'label' => 'Username or E-mail',
                    'attr' => [
                        'autocomplete' => 'off',
                        'spellcheck' => 'false',
                        'class' => 'typeahead app-form__control',
                        'style' => 'min-width: 620px;',
                        'placeholder' => 'Username or E-mail', //'user@email'
                    ]
                ]
            )
            ->add('go', 'submit', ['label' => 'GO!', 'attr' => ['class' => 'btn btn-lg btn-primary']]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return '';
    }
}

