<?php

namespace onekit\AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SMSMessageTemplateFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('POST')
            ->add(
                'SMSMessageBodies',
                'collection',
                [
                    'type' => new SMSMessageBodyFormType(),
                    'prototype' => false,
                    'options' => array('data_class' => 'onekit\AppBundle\Entity\SMSMessageBody'),
                    'allow_add' => false,
                    'allow_delete' => false,
                    'by_reference' => false,
                    'required' => true

                ]
            )
            ->add('save', 'submit', ['label' => 'Save', 'attr' => ['class' => 'btn btn-lg btn-primary']]);
    }



    /**
     * @return string
     */
    public function getName()
    {
        return 'sms_message_template';
    }
}

