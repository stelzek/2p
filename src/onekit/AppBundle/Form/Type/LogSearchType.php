<?php

namespace onekit\AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class LogSearchType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('GET')
            ->add('user_login', 'text', ['required' => false, 'attr' => ['class' => 'app-form__control', 'placeholder'=>'User login']])
            ->add('search', 'submit', ['label' => 'Search', 'attr' => ['class' => 'btn btn-lg btn-primary app-form__control']]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return '';
    }
}

