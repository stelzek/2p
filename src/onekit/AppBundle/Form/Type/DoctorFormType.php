<?php

namespace onekit\AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Collection;

class DoctorFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'title',
                'text',
                [
                    'label' => 'Title',
                    'required' => true,
                    'attr' => [
                        'class' => 'app-form__control'
                    ]
                ]
            )
            ->add(
                'address',
                'text',
                [
                    'label' => 'Address',
                    'required' => false,
                    'attr' => [
                        'class' => 'app-form__control'
                    ]
                ]
            )
            ->add(
                'phone',
                'text',
                [
                    'label' => 'Phone',
                    'required' => false,
                    'empty_data' => '49',
                    'attr' => [
                        'maxlength' => '13',
                        'class' => 'app-form__control'
                    ]
                ]
            )
            ->add('Save', 'submit', ['label' => 'Save', 'attr' => ['class' => 'btn btn-block btn-lg btn-primary']]);
    }


    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false
        ]);
    }

    public function getName()
    {
        return '';
    }
}