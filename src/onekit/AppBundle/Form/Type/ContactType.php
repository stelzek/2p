<?php

namespace onekit\AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Collection;

class ContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'email', array('label' => 'Sign up for Newsletters', 'required'=>true,
                'attr' => array(
                    'class' => 'app-body__email',
                    'placeholder' => 'Your Email Address'
                )
            ))
            ->add('submit', 'submit', ['label' => 'Register now!', 'attr' => ['class' => 'btn-md btn-primary']]);
    }

    public function getName()
    {
        return 'contact';
    }
}