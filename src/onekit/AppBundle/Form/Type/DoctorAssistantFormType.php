<?php

namespace onekit\AppBundle\Form\Type;

use onekit\AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Email;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Collection;

class DoctorAssistantFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('language', 'choice', array(
                'choices' => array(
                    'de' => 'German',
                    'en' => 'English'
                ),
                'required'    => true,
                'attr' => ['class' => 'app-form__control']
            ))
            ->add('gender', 'choice', array(
                'choices' => array(
                    1 => 'Male',
                    0 => 'Female'
                ),
                'required'    => false,
                'placeholder' => 'Choose your gender',
                'empty_data'  => null,
                'attr' => ['class' => 'app-form__control']
            ))
            ->add('firstname','text',['label'=>'First name','required' => true, 'attr' => ['class' => 'app-form__control']])
            ->add('lastname','text',['label'=>'Last name','required' => true, 'attr' => ['class' => 'app-form__control']])
            ->add('username','text',['label'=>'Username','required' => true, 'attr' => ['minlength'=>'3','class' => 'app-form__control']])
            ->add('email','email',['label'=>'Email','required' => true, 'attr' => ['minlength'=>'3','class' => 'app-form__control']])
            ->add('plainPassword','text',['label'=>'Password', 'attr' => ['class' => 'app-form__control']])
            ->add('phone','text',['label'=>'Phone','required' => true, 'attr' => ['maxlength'=>'13','class' => 'app-form__control']])
            ->add('enabled', 'checkbox', ['data'=> true,'label' => 'Enabled', 'required' => false, 'attr' => ['class' => '']])
            ->add('Save', 'submit', ['label' => 'Save', 'attr' => ['class' => 'btn btn-lg btn-primary']]);
    }


    public function getName()
    {
        return 'assistant';
    }
}