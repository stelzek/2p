<?php
namespace onekit\AppBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Vivait\SettingsBundle\Driver\ParametersStorageInterface;
use Vivait\SettingsBundle\Form\DataTransformer\KeyToArrayTransformer;
use Vivait\SettingsBundle\Form\EventListener\SettingsSubscriber;
use Vivait\SettingsBundle\Services\SettingsChain;

/**
 * Class SettingsType
 * @package onekit\AppBundle\Form\Type
 */
class SettingsType extends AbstractType
{
    /**
     * @var $driver ParametersStorageInterface
     */
    protected $driver;

    /**
     * @var SettingsChain
     */
    protected $registry;

    public function __construct(ParametersStorageInterface $driver, SettingsChain $registry) {
        $this->driver = $driver;
        $this->registry = $registry;
    }

    protected function generateDefaults($keys)
    {
        $defaults = [];
        foreach ($keys as $key) {
            $defaults[$key] = $this->registry->drivers(['doctrine', 'yaml'])->get($key);
        }
        return $defaults;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults($this->generateDefaults([
            'notify_email',
            'notify_sms',
        ]));
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('notify_email', 'checkbox', array('label' => 'settings.form.notify_email.label', 'required' => false))
            ->add('notify_sms', 'checkbox', array('label' => 'settings.form.notify_sms.label', 'required' => false))
            ->add('save', 'submit')
        ;
        $builder->addEventSubscriber(new SettingsSubscriber($this->driver));
        $builder->addModelTransformer(new KeyToArrayTransformer());
    }

    public function getName()
    {
        return 'settings';
    }
}