<?php

namespace onekit\AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Length;

class SMSMessageBodyFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('language', 'hidden')
            ->add('message', 'text', ['constraints' => [new Length(['min' => 20, 'max' => 700,'minMessage' => 'Message must be at least {{ limit }} characters long', 'maxMessage' => 'Message must be at least cannot be longer than {{ limit }} characters'])], 'required' => true, 'label' => 'Message', 'attr' => ['class' => 'app-form__control', 'placeholder' => 'Message']]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'sms_message_body';
    }
}

