<?php
namespace onekit\AppBundle\Form\Type;


use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\Collection;

class AppointmentFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'start',
                'datetime',
                array(
                    'label' => 'Reception date and time',
                    'widget'=>'single_text',
                    'format' => 'yyyy-MM-dd HH:mm',
                    'required' => true,
                    'attr' => array(
                        'class' => 'datetimepicker app-form__control',
                        'placeholder'=>'Reception',
                        'required'=> true
                    )
                )
            )
            ->add(
                'end',
                'datetime',
                [
                    'label' => 'Ending date and time',
                    'widget'=>'single_text',
                    'format' => 'yyyy-MM-dd HH:mm',
                    'required' => false,
                    'attr' => [
                        'class' => 'datetimepicker app-form__control',
                        'placeholder'=>'Reception',
                        'required'=> false
                    ]
                ]
            )
            /*
            ->add(
                'doctor',
                'entity',
                array(
                    'class' => 'AppBundle:Doctor',
                    'choice_label' => 'title',
                )
            )
            */
            ->add(
                'notify_sms',
                'checkbox',
                [
                    'label' => 'Send SMS notification to patient',
                    'required' => false,
                    'attr' => [
                        'class' => ''
                    ]
                ]
            )
            ->add(
                'notify_email',
                'checkbox',
                [
                    'label' => 'Send E-mail notification to patient',
                    'required' => false,
                    'attr' => [
                        'class' => ''
                    ]
                ]
            )
            ->add('Save', 'submit', ['label' => 'Save', 'attr' => ['class' => 'btn btn-lg btn-primary']]);
    }


    public function getName()
    {
        return 'appointment';
    }
}