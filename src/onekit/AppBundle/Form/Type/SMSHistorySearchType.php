<?php

namespace onekit\AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class SMSHistorySearchType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('GET')
            ->add('startRange', 'datetime', ['required' => false, 'widget'=>'single_text', 'format' => 'yyyy-MM-dd HH:mm', 'attr' => ['autocomplete'=>'off','spellcheck'=>'false','class' => 'datetimepicker app-form__control', 'placeholder'=>'Start range']])
            ->add('endRange', 'datetime', ['required' => false, 'widget'=>'single_text', 'format' => 'yyyy-MM-dd HH:mm', 'attr' => ['autocomplete'=>'off','spellcheck'=>'false','class' => 'datetimepicker app-form__control', 'placeholder'=>'End range']])
            ->add('search', 'submit', ['label' => 'Search', 'attr' => ['class' => 'btn btn-lg btn-primary']]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return '';
    }
}

