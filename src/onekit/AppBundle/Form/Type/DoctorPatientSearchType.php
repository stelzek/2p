<?php

namespace onekit\AppBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class DoctorPatientSearchType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setMethod('GET')
            ->add('firstname', 'text', ['required' => false, 'label' => 'First name', 'attr' => ['autocomplete'=>'off','spellcheck'=>'false','class' => 'typeahead app-form__control','placeholder'=>'First name']])
            ->add('lastname', 'text', ['required' => false, 'label' => 'Last name', 'attr' => ['autocomplete'=>'off','spellcheck'=>'false','class' => 'typeahead app-form__control','placeholder'=>'Last name']])
            ->add('search', 'submit', ['label' => 'Search', 'attr' => ['class' => 'btn btn-lg btn-primary']]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return '';
    }
}

