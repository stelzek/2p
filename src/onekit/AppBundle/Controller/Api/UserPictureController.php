<?php
namespace onekit\AppBundle\Controller\Api;


use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;
use onekit\AppBundle\Annotation as App;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use onekit\AppBundle\Entity\User;
use onekit\AppBundle\Entity\Input\UserPicture;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use onekit\AppBundle\Manager\AccountManager;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UserPictureController
 * @package onekit\AppBundle\Controller\Api
 *
 * @Sensio\Route("/users")
 * @App\RestResult
 */
class UserPictureController extends RestController
{

    /**
     * @var AccountManager
     */
    protected $userManager;

    /**
     * @DI\InjectParams({
     *     "userManager" = @DI\Inject("api.manager.account")
     * })
     *
     * @param AccountManager $userManager
     */
    public function __construct(AccountManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @ApiDoc(
     *      parameters={
     *          {"name"="image", "dataType"="file", "required"=true, "description"="user picture"}
     *      },
     *      views = {"default", "admin"}
     * )
     *
     * @Sensio\Security("has_role('ROLE_SUPER_ADMIN')")
     * @Rest\Post("/{id}/picture", name="api_post_user_picture")
     * @Sensio\ParamConverter("user", converter="doctrine.orm")
     * @Sensio\ParamConverter("picture", converter = "api.converter.user_picture")
     * @Rest\View(serializerGroups={"default", "user_picture"})
     *
     * @param User $user
     * @param UserPicture $picture
     * @param ConstraintViolationListInterface $constraints
     * @return User|Response
     */
    public function postPictureAction(User $user, UserPicture $picture, ConstraintViolationListInterface $constraints)
    {
        if ($constraints->count()) {
            return $this->handleError('Validation errors', $constraints);
        }
        $user = $this->userManager->setPicture($user, $picture);
        return $user;
    }

    /**
     * @ApiDoc(views = {"default", "admin"})
     *
     * @Sensio\Security("has_role('ROLE_SUPER_ADMIN')")
     * @Rest\Get("/{id}/picture", name="api_get_user_picture", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("user", converter="doctrine.orm")
     * @Rest\View(serializerGroups={"default", "user_picture"})
     *
     * @param User $user
     * @return User
     */
    public function getPictureAction(User $user)
    {
        return $user;
    }

    /**
     * @ApiDoc(views = {"default", "admin"})
     *
     * @Sensio\Security("has_role('ROLE_SUPER_ADMIN')")
     * @Rest\Delete("/{id}/picture", name="api_delete_doctor_picture", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("user", converter="doctrine.orm")
     * @Rest\View(serializerGroups={"default", "user_picture"})
     *
     * @param User $user
     * @return User
     */
    public function deletePictureAction(User $user)
    {
        $user = $this->userManager->setPicture($user);
        return $user;
    }
}