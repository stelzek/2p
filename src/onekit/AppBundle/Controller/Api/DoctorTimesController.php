<?php
namespace onekit\AppBundle\Controller\Api;


use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;
use onekit\AppBundle\Annotation as App;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use onekit\AppBundle\Entity\Doctor;
use onekit\AppBundle\Entity\Input\DoctorTimes;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use onekit\AppBundle\Manager\DoctorManager;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DoctorTimesController
 * @package onekit\AppBundle\Controller\Api
 *
 * @Sensio\Route("/doctors")
 * @App\RestResult
 */
class DoctorTimesController extends RestController
{

    /**
     * @var DoctorManager
     */
    protected $doctorManager;

    /**
     * @DI\InjectParams({
     *     "doctorManager" = @DI\Inject("api.manager.doctor")
     * })
     *
     * @param DoctorManager $doctorManager
     */
    public function __construct(DoctorManager $doctorManager)
    {
        $this->doctorManager = $doctorManager;
    }

    /**
     * @ApiDoc(input = "onekit\AppBundle\Entity\Input\DoctorTimes", views = {"default", "admin"})
     *
     * @Sensio\Security("is_granted('edit', doctor)")
     * @Rest\Put("/{id}/times", name="api_put_doctor_times")
     * @Sensio\ParamConverter("doctor", converter="doctrine.orm")
     * @Sensio\ParamConverter("doctorTimes", converter = "fos_rest.request_body")
     * @Rest\View
     *
     * @param Doctor $doctor
     * @param DoctorTimes $doctorTimes
     * @param ConstraintViolationListInterface $constraints
     * @return DoctorTimes|Response
     */
    public function putTimesAction(Doctor $doctor, DoctorTimes $doctorTimes, ConstraintViolationListInterface $constraints)
    {
        if ($constraints->count()) {
            return $this->handleError('Validation errors', $constraints);
        }
        $doctor = $this->doctorManager->setTimes($doctor, $doctorTimes);
        return $this->doctorManager->getTimes($doctor);
    }

    /**
     * @ApiDoc(views = {"default", "admin"})
     *
     * @Sensio\Security("is_granted('view', doctor)")
     * @Rest\Get("/{id}/times", name="api_get_doctor_times", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("doctor", converter="doctrine.orm")
     * @Rest\View
     *
     * @param Doctor $doctor
     * @return DoctorTimes
     */
    public function getTimesAction(Doctor $doctor)
    {
        return $this->doctorManager->getTimes($doctor);
    }
}