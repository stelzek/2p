<?php
namespace onekit\AppBundle\Controller\Api;


use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;
use onekit\AppBundle\Annotation as App;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use onekit\AppBundle\Entity\Doctor;
use onekit\AppBundle\Entity\Input\CreateDoctor;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use onekit\AppBundle\Manager\DoctorManager;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DoctorController
 * @package onekit\AppBundle\Controller\Api
 *
 * @Sensio\Route("/doctors")
 * @App\RestResult
 */
class DoctorController extends RestController
{

    /**
     * @var DoctorManager
     */
    protected $doctorManager;

    /**
     * @DI\InjectParams({
     *     "doctorManager" = @DI\Inject("api.manager.doctor")
     * })
     *
     * @param DoctorManager $doctorManager
     */
    public function __construct(DoctorManager $doctorManager)
    {
        $this->doctorManager = $doctorManager;
    }

    /**
     * @ApiDoc(input = "onekit\AppBundle\Entity\Input\CreateDoctor", views = {"default", "admin"})
     *
     * @Sensio\Security("has_role('ROLE_SUPER_ADMIN')")
     * @Rest\Post("", name="api_post_doctor")
     * @Sensio\ParamConverter("createDoctor", converter = "fos_rest.request_body")
     * @Rest\View(statusCode=201, serializerGroups={"default", "doctor"})
     *
     * @param CreateDoctor $createDoctor
     * @param ConstraintViolationListInterface $constraints
     * @return Doctor|Response
     */
    public function postDoctorAction(CreateDoctor $createDoctor, ConstraintViolationListInterface $constraints)
    {
        if ($constraints->count()) {
            return $this->handleError('Validation errors', $constraints);
        }
        return $this->doctorManager->create($createDoctor);
    }


    /**
     * @ApiDoc(input = "onekit\AppBundle\Entity\Input\CreateDoctor", views = {"default", "admin"})
     *
     * @Sensio\Security("has_role('ROLE_SUPER_ADMIN')")
     * @Rest\Put("/{id}", name="api_put_doctor")
     * @Sensio\ParamConverter("doctor", converter="doctrine.orm")
     * @Sensio\ParamConverter("createDoctor", converter = "fos_rest.request_body")
     * @Rest\View(serializerGroups={"default", "doctor"})
     *
     * @param Doctor $doctor
     * @param CreateDoctor $createDoctor
     * @param ConstraintViolationListInterface $constraints
     * @return Doctor|Response
     */
    public function putDoctorAction(Doctor $doctor, CreateDoctor $createDoctor, ConstraintViolationListInterface $constraints)
    {
        if ($constraints->count()) {
            return $this->handleError('Validation errors', $constraints);
        }
        return $this->doctorManager->update($doctor, $createDoctor);
    }

    /**
     * @ApiDoc(views = {"default", "admin"})
     *
     * @Sensio\Security("has_role('ROLE_SUPER_ADMIN')")
     * @Rest\Get("", name="api_get_doctor_list")
     * @App\RestResult(paginate=true, sort={"id", "first_name", "last_name", "email", "phone"})
     * @Rest\View(serializerGroups={"default"})
     *
     * @param $page
     * @param $limit
     * @param $order
     * @param $direction
     * @return Doctor[]
     */
    public function getDoctorListAction($order, $direction, $page, $limit)
    {
        return $this->doctorManager->getList()->order($order, $direction)->paginate($page, $limit);
    }


    /**
     * @ApiDoc(views = {"default", "admin"})
     *
     * @Sensio\Security("is_granted('view', doctor)")
     * @Rest\Get("/{id}", name="api_get_doctor", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("doctor", converter="doctrine.orm")
     * @Rest\View(serializerGroups={"default", "doctor"})
     *
     * @param Doctor $doctor
     * @return Doctor
     */
    public function getDoctorAction(Doctor $doctor)
    {
        return $doctor;
    }

    /**
     * @ApiDoc(views = {"default", "admin"})
     *
     * @Sensio\Security("is_granted('delete', doctor)")
     * @Rest\Delete("/{id}", name="api_delete_doctor", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("doctor", converter="doctrine.orm")
     * @Rest\View()
     *
     * @param Doctor $doctor
     */
    public function deleteDoctorAction(Doctor $doctor)
    {
        $this->doctorManager->delete($doctor);
    }
}