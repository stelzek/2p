<?php
namespace onekit\AppBundle\Controller\Api;


use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;
use onekit\AppBundle\Annotation as App;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use onekit\AppBundle\Entity\Doctor;
use onekit\AppBundle\Entity\Input\DoctorAddress;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use onekit\AppBundle\Manager\DoctorManager;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DoctorAddressController
 * @package onekit\AppBundle\Controller\Api
 *
 * @Sensio\Route("/doctors")
 * @App\RestResult
 */
class DoctorAddressController extends RestController
{

    /**
     * @var DoctorManager
     */
    protected $doctorManager;

    /**
     * @DI\InjectParams({
     *     "doctorManager" = @DI\Inject("api.manager.doctor")
     * })
     *
     * @param DoctorManager $doctorManager
     */
    public function __construct(DoctorManager $doctorManager)
    {
        $this->doctorManager = $doctorManager;
    }

    /**
     * @ApiDoc(input = "onekit\AppBundle\Entity\Input\DoctorAddress", views = {"default", "admin"})
     *
     * @Sensio\Security("is_granted('edit', doctor)")
     * @Rest\Put("/{id}/address", name="api_put_doctor_address")
     * @Sensio\ParamConverter("doctor", converter="doctrine.orm")
     * @Sensio\ParamConverter("doctorAddress", converter = "fos_rest.request_body")
     * @Rest\View
     *
     * @param Doctor $doctor
     * @param DoctorAddress $doctorAddress
     * @param ConstraintViolationListInterface $constraints
     * @return DoctorAddress|Response
     */
    public function putAddressAction(Doctor $doctor, DoctorAddress $doctorAddress, ConstraintViolationListInterface $constraints)
    {
        if ($constraints->count()) {
            return $this->handleError('Validation errors', $constraints);
        }
        $doctor = $this->doctorManager->setAddress($doctor, $doctorAddress);
        return $this->doctorManager->getAddress($doctor);
    }

    /**
     * @ApiDoc(views = {"default", "admin"})
     *
     * @Sensio\Security("is_granted('view', doctor)")
     * @Rest\Get("/{id}/address", name="api_get_doctor_address", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("doctor", converter="doctrine.orm")
     * @Rest\View
     *
     * @param Doctor $doctor
     * @return DoctorAddress
     */
    public function getAddressAction(Doctor $doctor)
    {
        return $this->doctorManager->getAddress($doctor);
    }
}