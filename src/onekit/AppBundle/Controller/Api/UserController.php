<?php

namespace onekit\AppBundle\Controller\Api;

use JMS\DiExtraBundle\Annotation as DI;
use onekit\AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;
use FOS\RestBundle\Controller\Annotations as Rest;
use onekit\AppBundle\Annotation as App;

use FOS\RestBundle\Controller\FOSRestController;
use JMS\Serializer\SerializationContext;
use onekit\AppBundle\Entity\Input\CreateUser;
use onekit\AppBundle\Manager\AccountManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query\Expr;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Validator\ConstraintViolationListInterface;


/**
 * API User controller.++
 * @Route("/users")
 *
 * @App\RestResult
 */
class UserController extends RestController
{
    /**
     * @var AccountManager
     */
    protected $userManager;

    /**
     * @DI\InjectParams({
     *     "userManager" = @DI\Inject("api.manager.account")
     * })
     *
     * @param AccountManager $userManager
     */
    public function __construct(AccountManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @ApiDoc(input = "\onekit\AppBundle\Entity\Input\CreateUser", views = {"default", "admin"})
     *
     * @Sensio\Security("has_role('ROLE_SUPER_ADMIN')")
     * @Rest\Post("", name="api_post_user")
     * @Rest\View(statusCode=201, serializerGroups={"default", "user"})
     *
     * @param CreateUser $createUser
     * @param ConstraintViolationListInterface $constraints
     * @return \onekit\AppBundle\Entity\User|Response
     */
    public function postUserAction(CreateUser $createUser, ConstraintViolationListInterface $constraints)
    {
        if ($constraints->count()) {
            return $this->handleError('Validation errors', $constraints);
        }
        return $this->userManager->create($createUser);
    }

    /**
     * @ApiDoc(views = {"default", "admin"})
     *
     * @Sensio\Security("has_role('ROLE_SUPER_ADMIN')")
     * @Rest\Get("/{id}", name="api_get_user", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("user", converter="doctrine.orm")
     * @Rest\View(serializerGroups={"default", "user"})
     *
     * @param User $user
     * @return User
     */
    public function getUserAction(User $user)
    {
        return $user;
    }

    /**
     * @ApiDoc(views = {"default", "admin"})
     *
     * @Sensio\Security("has_role('ROLE_SUPER_ADMIN')")
     * @Rest\Get("", name="api_get_user_list")
     * @App\RestResult(paginate=true, sort={"id", "username", "email", "created"})
     * @Rest\View(serializerGroups={"default"})
     *
     * @param $page
     * @param $limit
     * @param $order
     * @param $direction
     * @return User[]
     */
    public function getUserListAction($page, $limit, $order, $direction)
    {
        return $this->userManager->getList()->order($order, $direction)->paginate($page, $limit);
    }

    /**
     * @ApiDoc(input = "\onekit\AppBundle\Entity\Input\CreateUser", views = {"default", "admin"})
     *
     * @Sensio\Security("has_role('ROLE_SUPER_ADMIN')")
     * @Rest\Put("/{id}", name="api_put_user", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("user", converter="doctrine.orm")
     * @Rest\View(serializerGroups={"default", "user"})
     *
     *
     * @param User $user
     * @param CreateUser $createUser
     * @return User
     */
    public function putUserAction(User $user, CreateUser $createUser)
    {
        return $this->userManager->update($user, $createUser);
    }

    /**
     * @ApiDoc(views = {"default", "admin"})
     *
     * @Sensio\Security("has_role('ROLE_SUPER_ADMIN')")
     * @Rest\Delete("/{id}", name="api_delete_user", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("user", converter="doctrine.orm")
     * @Rest\View()
     *
     * @param User $user
     */
    public function deleteUserAction(User $user)
    {
        $this->userManager->delete($user);
    }
}
