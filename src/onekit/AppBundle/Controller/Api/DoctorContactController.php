<?php
namespace onekit\AppBundle\Controller\Api;


use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;
use onekit\AppBundle\Annotation as App;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use onekit\AppBundle\Entity\Doctor;
use onekit\AppBundle\Entity\Input\DoctorContact;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use onekit\AppBundle\Manager\DoctorManager;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class DoctorContactController
 * @package onekit\AppBundle\Controller\Api
 *
 * @Sensio\Route("/doctors")
 * @App\RestResult
 */
class DoctorContactController extends RestController
{

    /**
     * @var DoctorManager
     */
    protected $doctorManager;

    /**
     * @DI\InjectParams({
     *     "doctorManager" = @DI\Inject("api.manager.doctor")
     * })
     *
     * @param DoctorManager $doctorManager
     */
    public function __construct(DoctorManager $doctorManager)
    {
        $this->doctorManager = $doctorManager;
    }

    /**
     * @ApiDoc(input = "onekit\AppBundle\Entity\Input\DoctorContact", views = {"default", "admin"})
     *
     * @Sensio\Security("is_granted('edit', doctor)")
     * @Rest\Put("/{id}/contact", name="api_put_doctor_contact")
     * @Sensio\ParamConverter("doctor", converter="doctrine.orm")
     * @Sensio\ParamConverter("doctorContact", converter = "fos_rest.request_body")
     * @Rest\View(serializerGroups={"default", "doctor_contact"})
     *
     * @param Doctor $doctor
     * @param DoctorContact $doctorContact
     * @param ConstraintViolationListInterface $constraints
     * @return Doctor|Response
     */
    public function putContactAction(Doctor $doctor, DoctorContact $doctorContact, ConstraintViolationListInterface $constraints)
    {
        if ($constraints->count()) {
            return $this->handleError('Validation errors', $constraints);
        }
        return $this->doctorManager->setContact($doctor, $doctorContact);
    }

    /**
     * @ApiDoc(views = {"default", "admin"})
     *
     * @Sensio\Security("is_granted('view', doctor)")
     * @Rest\Get("/{id}/contact", name="api_get_doctor_contact", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("doctor", converter="doctrine.orm")
     * @Rest\View(serializerGroups={"default", "doctor_contact"})
     * @param Doctor $doctor
     * @return Doctor
     */
    public function getContactAction(Doctor $doctor)
    {
        return $doctor;
    }
}