<?php
namespace onekit\AppBundle\Controller\Api;


use onekit\AppBundle\Entity\Doctor;
use onekit\AppBundle\Entity\Input\CreatePatient;
use onekit\AppBundle\Entity\Input\SearchPatient;
use onekit\AppBundle\Entity\Patient;
use onekit\AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\Response;
use onekit\AppBundle\Manager\PatientManager;

use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;
use onekit\AppBundle\Annotation as App;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * API Patient controller.++
 * @App\RestResult
 */
class PatientController extends RestController
{
    /**
     * @var PatientManager
     */
    protected $patientManager;

    /**
     * @DI\InjectParams({
     *     "patientManager" = @DI\Inject("api.manager.patient")
     * })
     *
     * @param PatientManager $patientManager
     */
    public function __construct(PatientManager $patientManager)
    {
        $this->patientManager = $patientManager;
    }

    /**
     * @ApiDoc(input = "\onekit\AppBundle\Entity\Input\CreatePatient", views = {"default", "admin"})
     *
     * @Sensio\Security("is_granted('patients', doctor)")
     * @Rest\Post("/doctors/{id}/patients", name="api_post_patient", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("doctor", converter="doctrine.orm")
     * @Sensio\ParamConverter("createPatient", converter = "fos_rest.request_body")
     * @Rest\View(statusCode=201, serializerGroups={"default", "patient"})
     *
     * @param Doctor $doctor
     * @param CreatePatient $createPatient
     * @param ConstraintViolationListInterface $constraints
     * @return Patient|Response
     */
    public function postPatientAction(Doctor $doctor, CreatePatient $createPatient, ConstraintViolationListInterface $constraints)
    {
        if ($constraints->count()) {
            return $this->handleError('Validation errors', $constraints);
        }
        return $this->patientManager->create($doctor, $createPatient);
    }

    /**
     * @ApiDoc(input = "onekit\AppBundle\Entity\Input\CreatePatient", views = {"default", "admin"})
     *
     * @Sensio\Security("is_granted('edit', patient)")
     * @Rest\Put("/patients/{id}", name="api_put_patient", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("patient", converter="doctrine.orm")
     * @Sensio\ParamConverter("createPatient", converter = "fos_rest.request_body")
     * @Rest\View(serializerGroups={"default", "patient"})
     *
     * @param Patient $patient
     * @param CreatePatient $createPatient
     * @param ConstraintViolationListInterface $constraints
     * @return Patient|Response
     */
    public function putPatientAction(Patient $patient, CreatePatient $createPatient, ConstraintViolationListInterface $constraints)
    {
        if ($constraints->count()) {
            return $this->handleError('Validation errors', $constraints);
        }
        return $this->patientManager->update($patient, $createPatient);
    }

    /**
     * @ApiDoc(views = {"default", "admin"})
     *
     * @Sensio\Security("is_granted('view', patient)")
     * @Rest\Get("/patients/{id}", name="api_get_patient", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("patient", converter="doctrine.orm")
     * @Rest\View(serializerGroups={"default", "patient"})
     *
     * @param Patient $patient
     * @return Patient
     */
    public function getPatientAction(Patient $patient)
    {
        return $patient;
    }

    /**
     * @ApiDoc(views = {"default", "admin"})
     *
     * @Sensio\Security("is_granted('view', doctor)")
     * @Rest\Get("/doctors/{id}/patients", name="api_get_patient_list", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("doctor", converter="doctrine.orm")
     * @Rest\QueryParam(name="keyword", requirements=".+")
     * @App\RestResult(paginate=true, sort={"id", "first_name", "last_name"})
     * @Rest\View(serializerGroups={"default"})
     *
     * @param Doctor $doctor
     * @param string $keyword
     * @param integer $page
     * @param integer $limit
     * @param string $order
     * @param string $direction
     * @return Response
     */
    public function getPatientListAction(Doctor $doctor, $keyword, $order, $direction, $page, $limit)
    {
        $search = new SearchPatient();
        $search->keyword = $keyword;
        return $this->forward('\onekit\AppBundle\Controller\Api\PatientController::getPatientSearchAction', array(
            'doctor' => $doctor,
            'searchPatient' => $search,
        ), array(
            'page' => $page,
            'limit' => $limit,
            'orderBy' => $order,
            'orderDir' => $direction,
        ));
    }

    /**
     * @ApiDoc(input = "onekit\AppBundle\Entity\Input\SearchPatient", views = {"default", "admin"})
     *
     * @Sensio\Security("is_granted('view', doctor)")
     * @Rest\Post("/doctors/{id}/patients/search", name="api_get_patient_search", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("doctor", converter="doctrine.orm")
     * @Sensio\ParamConverter("searchPatient", converter = "fos_rest.request_body")
     * @App\RestResult(paginate=true, sort={"id", "first_name", "last_name"})
     * @Rest\View(serializerGroups={"default"})
     *
     * @param Doctor $doctor
     * @param SearchPatient $searchPatient
     * @param string $order
     * @param string $direction
     * @param int $page
     * @param int $limit
     * @return User[]
     */
    public function getPatientSearchAction(Doctor $doctor, SearchPatient $searchPatient, $order, $direction, $page, $limit)
    {
        return $this->patientManager->getDoctorList($doctor)->filter($searchPatient)->order($order, $direction)->paginate($page, $limit);
    }

    /**
     * @ApiDoc(views = {"default", "admin"})
     *
     * @Sensio\Security("is_granted('delete', patient)")
     * @Rest\Delete("/patients/{id}", name="api_delete_patient", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("patient", converter="doctrine.orm")
     * @Rest\View()
     *
     * @param Patient $patient
     */
    public function deletePatientAction(Patient $patient)
    {
        $this->patientManager->delete($patient);
    }
}