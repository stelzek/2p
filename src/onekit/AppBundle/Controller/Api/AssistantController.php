<?php
namespace onekit\AppBundle\Controller\Api;


use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;
use onekit\AppBundle\Annotation as App;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use onekit\AppBundle\Entity\Assistant;
use onekit\AppBundle\Entity\Doctor;
use onekit\AppBundle\Entity\Input\CreateAssistant;
use onekit\AppBundle\Manager\AssistantManager;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * API Assistant controller.++
 * @App\RestResult
 */
class AssistantController extends RestController
{
    /**
     * @var AssistantManager
     */
    protected $assistantManager;


    /**
     * @DI\InjectParams({
     *     "assistantManager" = @DI\Inject("api.manager.assistant")
     * })
     *
     * @param AssistantManager $assistantManager
     */
    public function __construct(AssistantManager $assistantManager)
    {
        $this->assistantManager = $assistantManager;
    }

    /**
     * @ApiDoc(input = "\onekit\AppBundle\Entity\Input\CreateAssistant", views = {"default", "admin"})
     *
     * @Sensio\Security("is_granted('assistants', doctor)")
     * @Rest\Post("/doctors/{id}/assistants", name="api_post_assistant", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("doctor", converter="doctrine.orm")
     * @Sensio\ParamConverter("createAssistant", converter = "fos_rest.request_body")
     * @Rest\View(statusCode=201, serializerGroups={"default", "assistant"})
     *
     * @param Doctor $doctor
     * @param CreateAssistant $createAssistant
     * @param ConstraintViolationListInterface $constraints
     * @return Assistant|Response
     */
    public function postAssistantAction(Doctor $doctor, CreateAssistant $createAssistant, ConstraintViolationListInterface $constraints)
    {
        if ($constraints->count()) {
            return $this->handleError('Validation errors', $constraints);
        }
        return $this->assistantManager->create($doctor, $createAssistant);
    }

    /**
     * @ApiDoc(input = "onekit\AppBundle\Entity\Input\CreateAssistant", views = {"default", "admin"})
     *
     * @Sensio\Security("is_granted('edit', assistant)")
     * @Rest\Put("/assistants/{id}", name="api_put_assistant")
     * @Sensio\ParamConverter("assistant", converter="doctrine.orm")
     * @Sensio\ParamConverter("createAssistant", converter = "fos_rest.request_body")
     * @Rest\View(serializerGroups={"default", "assistant"})
     *
     * @param Assistant $assistant
     * @param CreateAssistant $createAssistant
     * @param ConstraintViolationListInterface $constraints
     * @return Doctor|Response
     */
    public function putAssistantAction(Assistant $assistant, CreateAssistant $createAssistant, ConstraintViolationListInterface $constraints)
    {
        if ($constraints->count()) {
            return $this->handleError('Validation errors', $constraints);
        }
        return $this->assistantManager->update($assistant, $createAssistant);
    }

    /**
     * @ApiDoc(views = {"default", "admin"})
     *
     * @Sensio\Security("is_granted('view', assistant)")
     * @Rest\Get("/assistants/{id}", name="api_get_assistant", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("assistant", converter="doctrine.orm")
     * @Rest\View(serializerGroups={"default", "assistant"})
     *
     * @param Assistant $assistant
     * @return Assistant
     */
    public function getAssistantAction(Assistant $assistant)
    {
        return $assistant;
    }

    /**
     * @ApiDoc(views = {"default", "admin"})
     *
     * @Sensio\Security("is_granted('assistants', doctor)")
     * @Rest\Get("/doctors/{id}/assistants", name="api_get_assistant_list", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("doctor", converter="doctrine.orm")
     * @Rest\QueryParam(name="title", requirements=".+")
     * @App\RestResult(paginate=true, sort={"id", "title"})
     * @Rest\View(serializerGroups={"default"})
     *
     * @param Doctor $doctor
     * @param string $title
     * @param integer $page
     * @param integer $limit
     * @param string $order
     * @param string $direction
     * @return Response
     */
    public function getAssistantListAction(Doctor $doctor, $title, $page, $limit, $order, $direction)
    {
        trim($title); // not used
        return $this->assistantManager->getDoctorList($doctor)->order($order, $direction)->paginate($page, $limit);
    }

    /**
     * @ApiDoc(views = {"default", "admin"})
     *
     * @Sensio\Security("is_granted('delete', assistant)")
     * @Rest\Delete("/assistants/{id}", name="api_delete_assistant", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("assistant", converter="doctrine.orm")
     * @Rest\View()
     *
     * @param Assistant $assistant
     */
    public function deleteAssistantAction(Assistant $assistant)
    {
        $this->assistantManager->delete($assistant);
    }
}
