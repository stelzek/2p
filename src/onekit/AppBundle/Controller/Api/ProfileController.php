<?php
namespace onekit\AppBundle\Controller\Api;


use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\Controller\Annotations as Rest;
use onekit\AppBundle\Entity\Input\UserCredentials;
use onekit\AppBundle\Entity\Input\UserLanguage;
use onekit\AppBundle\Entity\Input\UserPicture;
use onekit\AppBundle\Entity\User;
use onekit\AppBundle\Manager\AccountManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;
use onekit\AppBundle\Annotation as App;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * API Profile controller.++
 * @Sensio\Route("/profile")
 *
 * @App\RestResult
 */
class ProfileController extends RestController
{
    /**
     * @var AccountManager
     */
    protected $userManager;

    /**
     * @DI\InjectParams({
     *     "userManager" = @DI\Inject("api.manager.account")
     * })
     *
     * @param AccountManager $userManager
     */
    public function __construct(AccountManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @ApiDoc(views = {"default", "admin"})
     *
     * @Sensio\Security("has_role('ROLE_USER')")
     * @Rest\Get("", name="api_get_profile")
     * @Rest\View(serializerGroups={"profile", "user_picture"})
     *
     * @return User
     */
    public function getProfileAction()
    {
        return $this->getUser();
    }

    /**
     * @ApiDoc(
     *      parameters={
     *          {"name"="image", "dataType"="file", "required"=true, "description"="user picture"}
     *      },
     *      views = {"default", "admin"}
     * )
     *
     * @Sensio\Security("has_role('ROLE_USER')")
     * @Rest\Post("/logo", name="api_post_profile_logo")
     * @Sensio\ParamConverter("picture", converter = "api.converter.user_picture")
     * @Rest\View(serializerGroups={"default", "user_picture"})
     *
     * @param UserPicture $picture
     * @param ConstraintViolationListInterface $constraints
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function postLogoAction(UserPicture $picture, ConstraintViolationListInterface $constraints)
    {
        return $this->forward(
            '\onekit\AppBundle\Controller\Api\UserPictureController::postPictureAction',
            array(
                'user' => $this->getUser(),
                'picture' => $picture,
                'constraints' => $constraints,
            )
        );
    }

    /**
     * @ApiDoc(views = {"default", "admin"})
     *
     * @Sensio\Security("has_role('ROLE_USER')")
     * @Rest\Delete("/logo", name="api_delete_profile_logo")
     * @Rest\View(serializerGroups={"default", "user_picture"})
     *
     * @return Response
     */
    public function deleteLogoAction()
    {
        return $this->forward(
            '\onekit\AppBundle\Controller\Api\UserPictureController::deletePictureAction',
            ['user' => $this->getUser()]
        );
    }

    /**
     * @ApiDoc(input = "\onekit\AppBundle\Entity\Input\UserCredentials", views = {"default", "admin"})
     *
     * @Sensio\Security("has_role('ROLE_USER')")
     * @Rest\Put("/credentials", name="api_put_profile_credentials")
     * @Sensio\ParamConverter("userCredentials", converter = "fos_rest.request_body")
     * @Rest\View(serializerGroups={"profile", "user_picture"})
     *
     * @param UserCredentials $userCredentials
     * @param ConstraintViolationListInterface $constraints
     * @return Response
     */
    public function putCredentialsAction(UserCredentials $userCredentials, ConstraintViolationListInterface $constraints)
    {
        if ($constraints->count()) {
            return $this->handleError('Validation errors', $constraints);
        }
        $user = $this->userManager->setCredentials($this->getUser(), $userCredentials);
        return $user;
    }


    /**
     * @ApiDoc(input = "\onekit\AppBundle\Entity\Input\UserLanguage", views = {"default", "admin"})
     *
     * @Sensio\Security("has_role('ROLE_USER')")
     * @Rest\Put("/language", name="api_put_profile_language")
     * @Sensio\ParamConverter("userLanguage", converter = "fos_rest.request_body")
     *
     * @param UserLanguage $userLanguage
     * @param ConstraintViolationListInterface $constraints
     * @return Response
     */
    public function putLanguageAction(UserLanguage $userLanguage, ConstraintViolationListInterface $constraints)
    {
        return $this->forward(
            '\onekit\AppBundle\Controller\Api\UserLanguageController::putLanguageAction',
            [
                'user' => $this->getUser(),
                'userLanguage' => $userLanguage,
                'constraints' => $constraints,
            ]
        );
    }

}
