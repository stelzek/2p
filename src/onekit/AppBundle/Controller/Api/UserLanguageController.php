<?php
namespace onekit\AppBundle\Controller\Api;


use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\Controller\Annotations as Rest;
use onekit\AppBundle\Entity\Input\UserLanguage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;
use onekit\AppBundle\Annotation as App;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use onekit\AppBundle\Entity\User;
use onekit\AppBundle\Entity\Input\UserPicture;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use onekit\AppBundle\Manager\AccountManager;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class UserLanguageController
 * @package onekit\AppBundle\Controller\Api
 *
 * @Sensio\Route("/users")
 * @App\RestResult
 */
class UserLanguageController extends RestController
{

    /**
     * @var AccountManager
     */
    protected $userManager;

    /**
     * @DI\InjectParams({
     *     "userManager" = @DI\Inject("api.manager.account")
     * })
     *
     * @param AccountManager $userManager
     */
    public function __construct(AccountManager $userManager)
    {
        $this->userManager = $userManager;
    }

    /**
     * @ApiDoc(input = "\onekit\AppBundle\Entity\Input\UserLanguage", views = {"default", "admin"})
     *
     * @Sensio\Security("has_role('ROLE_SUPER_ADMIN')")
     * @Rest\Put("/{id}/picture", name="api_put_user_language")
     * @Sensio\ParamConverter("user", converter="doctrine.orm")
     * @Sensio\ParamConverter("userLanguage", converter = "fos_rest.request_body")
     * @Rest\View(serializerGroups={"default", "profile"})
     *
     * @param User $user
     * @param UserLanguage $userLanguage
     * @param ConstraintViolationListInterface $constraints
     * @return User|Response
     */
    public function putLanguageAction(User $user, UserLanguage $userLanguage, ConstraintViolationListInterface $constraints)
    {
        if ($constraints->count()) {
            return $this->handleError('Validation errors', $constraints);
        }
        $user = $this->userManager->setLanguage($user, $userLanguage);
        return $user;
    }
}