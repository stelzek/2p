<?php

namespace onekit\AppBundle\Controller\Api;

use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\Controller\Annotations as Rest;
use onekit\AppBundle\Annotation as App;
use onekit\AppBundle\Entity\Input\DateFilter;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;

use onekit\AppBundle\Entity\Appointment;
use onekit\AppBundle\Entity\Doctor;
use onekit\AppBundle\Entity\Input\CreateAppointment;
use onekit\AppBundle\Entity\Patient;
use onekit\AppBundle\Manager\ApiAppointmentManager;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query\Expr;
use Symfony\Component\Validator\ConstraintViolationListInterface;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use onekit\AppBundle\Annotation\NotificationEvent;

/**
 * API Appointment controller.++
 * @App\RestResult
 */
class AppointmentController extends RestController
{
    /**
     * @var ApiAppointmentManager
     */
    private $manager;

    /**
     * @DI\InjectParams({
     *     "manager" = @DI\Inject("api.manager.appointment")
     * })
     *
     * @param ApiAppointmentManager $manager
     */
    public function __construct(ApiAppointmentManager $manager)
    {
        $this->manager = $manager;
    }

    /**
     * @ApiDoc(input = "onekit\AppBundle\Entity\Input\CreateAppointment", views = {"default", "admin"})
     *
     * @Sensio\Security("is_granted('appointments', patient)")
     * @Rest\Post("/patients/{id}/appointments", name="api_post_appointment", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("patient", converter="doctrine.orm")
     * @Sensio\ParamConverter("createAppointment", converter = "fos_rest.request_body")
     * @Rest\View(statusCode=201, serializerGroups={"default", "appointment"})
     * @NotificationEvent
     *
     * @param Patient $patient
     * @param CreateAppointment $createAppointment
     * @param ConstraintViolationListInterface $constraints
     * @return Appointment|Response
     */
    public function postAppointmentAction(Patient $patient, CreateAppointment $createAppointment, ConstraintViolationListInterface $constraints)
    {
        if ($constraints->count()) {
            return $this->handleError('Validation errors', $constraints);
        }
        return $this->manager->create($patient, $createAppointment);
    }

    /**
     * @ApiDoc(views = {"default", "admin"})
     *
     * @Sensio\Security("is_granted('edit', appointment)")
     * @Rest\Post("/appointments/{id}/notify", name="api_post_appointment_notify", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("appointment", converter="doctrine.orm")
     * @Rest\View(serializerGroups={"default", "appointment_notify"})
     * @NotificationEvent
     *
     * @param Appointment $appointment
     * @return Appointment
     */
    public function postAppointmentNotifyAction(Appointment $appointment)
    {
        return $appointment;
    }


    /**
     * @ApiDoc(views = {"default", "admin"})
     *
     * @Sensio\Security("is_granted('view', appointment)")
     * @Rest\Get("/appointments/{id}", name="api_get_appointment", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("appointment", converter="doctrine.orm")
     * @Rest\View(serializerGroups={"default", "appointment"})
     *
     * @param Appointment $appointment
     * @return Appointment
     */
    public function getAppointmentAction(Appointment $appointment)
    {
        return $appointment;
    }

    /**
     * @ApiDoc(views = {"default", "admin"})
     *
     * @Sensio\Security("has_role('ROLE_SUPER_ADMIN')")
     * @Rest\Get("/appointments", name="api_get_appointment_list")
     * @App\RestResult(paginate=true, sort={"id"})
     * @Rest\View(serializerGroups={"default", "appointment_list"})
     *
     * @param integer $page
     * @param integer $limit
     * @param string $order
     * @param string $direction
     * @return Appointment[]
     */
    public function getAppointmentListAction($page, $limit, $order, $direction)
    {
        return $this->manager->getList()->order($order, $direction)->paginate($page, $limit);
    }

    /**
     * @ApiDoc(views = {"default", "admin"})
     *
     * @Sensio\Security("is_granted('view', doctor)")
     * @Rest\Get("/doctors/{id}/appointments", name="api_get_doctor_appointment_list", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("doctor", converter="doctrine.orm")
     * @App\RestResult(paginate=true, sort={"id"})
     * @Rest\View(serializerGroups={"default", "doctor_appointment_list"})
     *
     * @param Doctor $doctor
     * @param integer $page
     * @param integer $limit
     * @param string $order
     * @param string $direction
     * @return Appointment[]
     */
    public function getDoctorAppointmentListAction(Doctor $doctor, $page, $limit, $order, $direction)
    {
        return $this->manager->getDoctorList($doctor)->order($order, $direction)->paginate($page, $limit);
    }

    /**
     * @ApiDoc(views = {"default", "admin"})
     *
     * @Sensio\Security("is_granted('view', doctor)")
     * @Rest\Get("/doctors/{id}/appointments/calendar", name="api_get_doctor_appointment_calendar", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("doctor", converter="doctrine.orm")
     * @Rest\QueryParam(name="start", nullable=false)
     * @Rest\QueryParam(name="end", nullable=false)
     * @Rest\View(serializerGroups={"default", "doctor_appointment_list"})
     *
     * @param Doctor $doctor
     * @param \DateTime $start
     * @param \DateTime $end
     * @return Appointment[]
     */
    public function getDoctorAppointmentCalendarAction(Doctor $doctor, \DateTime $start, \DateTime $end)
    {
        return $this->manager->getDoctorList($doctor)->filterByDate($start, $end)->order('start', 'ASC')->all();
    }

    /**
     * @ApiDoc(input = "onekit\AppBundle\Entity\Input\DateFilter", views = {"default", "admin"})
     *
     * @Sensio\Security("is_granted('view', doctor)")
     * @Rest\Post("/doctors/{id}/appointments/calendar", name="api_post_doctor_appointment_calendar", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("doctor", converter="doctrine.orm")
     * @Sensio\ParamConverter("dateFilter", converter = "fos_rest.request_body")
     * @Rest\View(serializerGroups={"default", "doctor_appointment_list"})
     *
     * @param Doctor $doctor
     * @param DateFilter $dateFilter
     * @return Appointment[]
     */
    public function postDoctorAppointmentCalendarAction(Doctor $doctor, DateFilter $dateFilter)
    {
        return $this->manager->getDoctorList($doctor)->filterByDate($dateFilter->start_time, $dateFilter->end_time)->order('start', 'ASC')->all();
    }

    /**
     * @ApiDoc(views = {"default", "admin"})
     *
     * @Sensio\Security("is_granted('view', patient)")
     * @Rest\Get("/patients/{id}/appointments", name="api_get_patient_appointment_list", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("patient", converter="doctrine.orm")
     * @App\RestResult(paginate=true, sort={"id"})
     * @Rest\View(serializerGroups={"default"})
     *
     * @param Patient $patient
     * @param integer $page
     * @param integer $limit
     * @param string $order
     * @param string $direction
     * @return Appointment[]
     */
    public function getPatientAppointmentListAction(Patient $patient, $page, $limit, $order, $direction)
    {
        return $this->manager->getPatientList($patient)->order($order, $direction)->paginate($page, $limit);
    }


    /**
     * @ApiDoc(input = "onekit\AppBundle\Entity\Input\CreateAppointment", views = {"default", "admin"})
     *
     * @Sensio\Security("is_granted('edit', appointment)")
     * @Rest\Put("/appointments/{id}", name = "api_put_appointment", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("appointment", converter="doctrine.orm")
     * @Sensio\ParamConverter("createAppointment", converter = "fos_rest.request_body")
     * @Rest\View(serializerGroups={"default", "appointment"})
     * @NotificationEvent
     *
     * @param Appointment $appointment
     * @param CreateAppointment $createAppointment
     * @param ConstraintViolationListInterface $constraints
     * @return Appointment|Response
     */
    public function putAppointmentAction(Appointment $appointment, CreateAppointment $createAppointment, ConstraintViolationListInterface $constraints)
    {
        if ($constraints->count()) {
            return $this->handleError('Validation errors', $constraints);
        }
        $this->manager->update($appointment, $createAppointment);
        return $appointment;
    }

    /**
     * @ApiDoc(views = {"default", "admin"})
     *
     * @Sensio\Security("is_granted('delete', appointment)")
     * @Rest\Delete("/appointments/{id}", name="api_delete_appointment", requirements={"id" = "\d+"})
     * @Sensio\ParamConverter("appointment", converter="doctrine.orm")
     * @Rest\View()
     * @NotificationEvent
     *
     * @param Appointment $appointment
     */
    public function deleteAppointmentAction(Appointment $appointment)
    {
        $this->manager->delete($appointment);
    }
}
