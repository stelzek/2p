<?php
namespace onekit\AppBundle\Controller\Api;


use JMS\DiExtraBundle\Annotation as DI;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration as Sensio;
use onekit\AppBundle\Annotation as App;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use onekit\AppBundle\Manager\AccountManager;
use Doctrine\ORM\Query\Expr;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * API Time controller.++
 * @Sensio\Route("/time")
 *
 * @App\RestResult
 */
class TimeController extends RestController
{
    /**
     * @var AccountManager
     */
    protected $userManager;

    /**
     * @var TranslatorInterface
     */
    protected $translator;

    /**
     * @ApiDoc(views = {"default", "admin"})
     *
     * @Rest\Get("", name="api_get_time")
     *
     * @return \DateTime
     */
    public function getTimeAction()
    {
        return new \DateTime();
    }
}
