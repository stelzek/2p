<?php
namespace onekit\AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller as BaseController;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use Symfony\Component\HttpFoundation\Session\Session;

abstract class LegacyController extends BaseController
{
    protected function getPaginateOptions()
    {
        return array(
            'sortFieldParameterName' => 'orderBy',
            'sortDirectionParameterName' => 'orderDir',
        );
    }

    /**
     * @return FlashBag
     */
    protected function getFlashBag()
    {
        return $this->getSession()->getFlashBag();
    }

    /**
     * @return Session
     */
    protected function getSession()
    {
        return $this->get('session');
    }

    protected function translate($id, array $parameters = array(), $domain = null, $locale = null)
    {
        $translator = $this->get('translator');
        return $translator->trans($id, $parameters, $domain, $locale);
    }
}