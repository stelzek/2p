<?php

namespace onekit\AppBundle\Controller;

use onekit\AppBundle\Entity\Contact;
use onekit\AppBundle\Form\Type\ContactType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use onekit\AppBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{

    /**
     * @Template()
     * @Route("/", name="home", options={"sitemap"=true})
     */
    public function indexAction(Request $request)
    {
        if($this->get('security.context')->isGranted('ROLE_ADMIN')){
            return $this->redirect($this->generateUrl('admin'));
        }

        if($this->get('security.context')->isGranted('ROLE_USER')){
            return $this->redirect($this->generateUrl('react_dashboard'));
        }

        $contact = new Contact();
        $form = $this->createForm(new ContactType(),$contact);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $message = \Swift_Message::newInstance()
                ->setSubject($form->get('email')->getData())
                ->setFrom('noreply@2p-med.de')
                ->setTo('frank.scrock@2p-med.de')
                ->setBody(
                    $this->renderView(
                        'AppBundle:Mail:contactMail.html.twig',
                        array(
                            'ip' => $request->getClientIp(),
                            'email' => $form->get('email')->getData()
                        )
                    )
                );
            $contact->setEmail($form->get('email')->getData());
            $contact->setIp($request->getClientIp());
            $this->get('mailer')->send($message);
            $em = $this->getDoctrine()->getManager();
            $em->persist($contact);
            $em->flush();
            $request->getSession()->getFlashBag()->add('success', 'Thanks.');
            return $this->redirect($this->generateUrl('home'));
        }

        return ['form' => $form->createView()];

    }

    /**
     * @Template()
     * @Route("/imprint", name="imprint", options={"sitemap"={"priority" = 0.9,"changefreq"="weekly"}})
     */
    public function imprintAction()
    {
        return [];
    }




}
