<?php

namespace onekit\AppBundle\Controller\Admin;

use Doctrine\ORM\EntityManager;
use Liip\ImagineBundle\Imagine\Cache;
use JMS\DiExtraBundle\Annotation as DI;

use onekit\AppBundle\Entity\NotificationEvent;
use onekit\AppBundle\Entity\SMSMessageBody;
use onekit\AppBundle\Entity\User;
use onekit\AppBundle\Extractor\NotificationEventExtractor;
use Symfony\Component\HttpFoundation\Request;
use onekit\AppBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use onekit\AppBundle\Entity\SMSMessageTemplate;
use onekit\AppBundle\Form\Type\SMSMessageTemplateFormType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query\Expr;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;


/**
 * Admin SMS message template controller.
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 * @Route("/sms_message_template")
 */
class SMSMessageTemplateController extends Controller
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    protected $repo;

    /**
     * @var NotificationEventExtractor
     */
    protected $extractor;

    /**
     * @DI\InjectParams({
     *      "em" = @DI\Inject("doctrine.orm.entity_manager"),
     *      "extractor" = @DI\Inject("app.extractor.notification_event"),
     * })
     *
     * @param EntityManager $em
     * @param NotificationEventExtractor $extractor
     */
    public function __construct(EntityManager $em, NotificationEventExtractor $extractor)
    {
        $this->em = $em;
        $this->repo = $em->getRepository('AppBundle:SMSMessageTemplate');
        $this->extractor = $extractor;
    }

    /**
     * @Route("/create/{action}", name = "admin_sms_message_template_create", requirements = {"action" = "[\w_]+"})
     *
     * @param string $action
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function createAction($action) {
        if (!$event = $this->extractor->get($action)) {
            throw $this->createNotFoundException(sprintf('Action `%s` was not found', $action));
        }
        $smsTemplate = new SMSMessageTemplate();
        $smsTemplate->setNotificationEvent($event);
        foreach (User::getLanguages() as $lang) {
            $smsBody = new SMSMessageBody();
            $smsBody->setLanguage($lang);
            $smsBody->setMessage($this->translate(sprintf('notifications.%s.message', $action), array(), null, $lang));
            $smsTemplate->addSMSMessageBody($smsBody);
        }
        $this->em->persist($smsTemplate);
        $this->em->flush();
        return $this->redirect($this->generateUrl('admin_sms_message_template_edit', ['sms_message_template_id' => $smsTemplate->getId()]));
    }

    /**
     * List of sms message templates
     * @Route("/list", name="admin_sms_message_template", requirements={"type" = "html"})
     * @Template()
     */
    public function listAction(Request $request)
    {
        $sort = $request->get('sort') ? $request->get('sort') : 'id';
        $direction = $request->get('direction') ? $request->get('direction'):'asc';
        $limitPerPage = $request->get('limitPerPage') ? $request->get('limitPerPage') : 10;
        $criteria = ['doctor' => null];
        $list = $this->repo->findBy($criteria, [$sort => $direction]);
        $notifications = $this->extractor->all();

        return [
            'list' => $list,
            'candidates' => $notifications,
            'limitPerPage' => $limitPerPage,
        ];
    }

    /**
     * Edit sms message template
     * @Route("/{sms_message_template_id}/edit", name="admin_sms_message_template_edit")
     * @Template()
     */
    public function editAction(Request $request, $sms_message_template_id = null)
    {
        $sms_message_template = $this->repo->find($sms_message_template_id);
        $form = $this->createForm(new SMSMessageTemplateFormType(), $sms_message_template);
        $form->handleRequest($request);
        if ($form->isValid()) {
                $this->em->persist($sms_message_template);
                $this->em->flush();

            return $this->redirect($this->generateUrl('admin_sms_message_template'));
        }
        $result['form'] = $form->createView();
        $result['sms_message_template'] = $sms_message_template;
        return $result;
    }

}
