<?php
namespace onekit\AppBundle\Controller\Admin;


use JMS\DiExtraBundle\Annotation as DI;

use onekit\AppBundle\Controller\LegacyController;
use onekit\AppBundle\Manager\PatientManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Vivait\SettingsBundle\Driver\DoctrineDriver;

/**
 * Admin Patient controller.
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 * @Route("/settings")
 */
class SettingController extends Controller
{
    /**
     * Settings form
     * @Route("", name="admin_settings")
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function indexAction(Request $request)
    {
        $form = $this->createForm('settings');
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /** @var DoctrineDriver $driver */
            $driver = $this->get('vivait_settings.driver.doctrine');
            foreach ($form->getData() as $key => $value) {
                $driver->set($key, $value);
            }
        }

        return ['form' => $form];
    }
}
