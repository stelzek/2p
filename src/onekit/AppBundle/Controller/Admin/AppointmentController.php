<?php
namespace onekit\AppBundle\Controller\Admin;

use Doctrine\ORM\EntityManager;
use Knp\Component\Pager\PaginatorInterface;
use Liip\ImagineBundle\Imagine\Cache;
use JMS\DiExtraBundle\Annotation as DI;

use onekit\AppBundle\Controller\LegacyController;
use onekit\AppBundle\Entity\Patient;
use onekit\AppBundle\Manager\ApiAppointmentManager;
use onekit\AppBundle\Manager\AppointmentManager;
use onekit\AppBundle\Repository\AppointmentRepository;
use Symfony\Component\HttpFoundation\Request;
use onekit\AppBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use onekit\AppBundle\Entity\Appointment;
use onekit\AppBundle\Form\Type\AppointmentFormType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query\Expr;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;

use onekit\AppBundle\Annotation\NotificationEvent;


/**
 * Admin Appointment controller.
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 * @Route("/appointment")
 */
class AppointmentController extends LegacyController
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var AppointmentRepository
     */
    protected $repo;

    /**
     * @var ApiAppointmentManager
     */
    protected $manager;

    /**
     * @var AppointmentManager
     */
    protected $legacyManager;

    /**
     * @DI\InjectParams({
     *      "em" = @DI\Inject("doctrine.orm.entity_manager"),
     *      "manager" = @DI\Inject("api.manager.appointment"),
     *      "legacyManager" = @DI\Inject("manager.appointment"),
     * })
     *
     * @param EntityManager $em
     * @param ApiAppointmentManager $manager
     * @param AppointmentManager $legacyManager
     */
    public function __construct(EntityManager $em, ApiAppointmentManager $manager, AppointmentManager $legacyManager)
    {
        $this->em = $em;
        $this->repo = $this->em->getRepository('AppBundle:Appointment');
        $this->manager = $manager;
        $this->legacyManager = $legacyManager;
    }

    /**
     * List of appointments
     * @Route("/list", name="admin_appointment", requirements={"type" = "html"})
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function listAction(Request $request)
    {
        $qb = $this->manager->getList()->QB();

        $page = $request->query->get('page', 1);
        $limit = $request->query->get('limit', 10);

        /** @var PaginatorInterface $pager */
        $pager = $this->get('knp_paginator');
        $pagination = $pager->paginate($qb, $page, $limit, $this->getPaginateOptions());

        return array(
            'pagination' => $pagination,
            'user' => $this->getUser(),
        );
    }


    /**
     * create Appointment
     *
     * @Route("/{patient_id}/create", name="admin_appointment_create")
     * @Template()
     *
     * @param Request $request
     * @param $patient_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     *
     * @NotificationEvent("api_post_appointment")
     */
    public function createAction(Request $request, $patient_id)
    {
        $appointment = new Appointment();
        /** @var Patient $patient */
        $patient = $this->em->getRepository('AppBundle:Patient')->find($patient_id);
        $appointment->setPatient($patient);
        $form = $this->createForm(new AppointmentFormType(), $appointment);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->legacyManager->update($appointment);
            $request->attributes->set('_notification_event_appointment', $appointment);
            /** @var \onekit\AppBundle\Entity\NotificationEvent $notificationEvent */
            $notificationEvent = $request->attributes->get('_notification_event');
            $this->getFlashBag()->add('success', sprintf(
                '%s %s %s',
                $this->translate(sprintf('notifications.%s.title', $notificationEvent->getAction())),
                $this->translate('for'),
                $appointment->getPatient()->getFullName()
            ));
            return $this->redirect($this->generateUrl('admin_patient_show', ['id' => $patient_id]));
        }
        $result['form'] = $form->createView();
        $result['appointment'] = $appointment;
        return $result;
    }

    /**
     * Edit Appointment
     * @Route("/{appointment_id}/edit", name="admin_appointment_edit")
     * @Template()
     * @NotificationEvent("api_put_appointment")
     *
     * @param Request $request
     * @param null $appointment_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction(Request $request, $appointment_id = null)
    {
        /** @var Appointment $appointment */
        $appointment = $this->manager->get($appointment_id);
        $filename = null;
        $form = $this->createForm(new AppointmentFormType(), $appointment);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->legacyManager->update($appointment, true);
            $request->attributes->set('_notification_event_appointment', $appointment);
            /** @var \onekit\AppBundle\Entity\NotificationEvent $notificationEvent */
            $notificationEvent = $request->attributes->get('_notification_event');
            $this->getFlashBag()->add('success', sprintf(
                '%s %s %s',
                $this->translate(sprintf('notifications.%s.title', $notificationEvent->getAction())),
                $this->translate('for'),
                $appointment->getPatient()->getFullName()
            ));
            return $this->redirect($this->generateUrl('admin_patient_show', ['id' => $appointment->getPatient()->getId()]));
        }
        $result['filename'] = $filename;
        $result['form'] = $form->createView();
        $result['appointment'] = $appointment;
        return $result;
    }


    /**
     * Delete appointment
     * @Route("/{appointment_id}/delete", name="admin_appointment_delete")
     * @Template()
     * @NotificationEvent("api_delete_appointment")
     *
     * @param Request $request
     * @param $appointment_id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction(Request $request, $appointment_id)
    {
        /** @var Appointment $appointment */
        $appointment = $this->manager->get($appointment_id);
        if ($appointment) {
            $this->manager->delete($appointment);
            $request->attributes->set('_notification_event_appointment', $appointment);
            /** @var \onekit\AppBundle\Entity\NotificationEvent $notificationEvent */
            $notificationEvent = $request->attributes->get('_notification_event');
            $this->getFlashBag()->add('success', sprintf(
                '%s %s %s',
                $this->translate(sprintf('notifications.%s.title', $notificationEvent->getAction())),
                $this->translate('for'),
                $appointment->getPatient()->getFullName()
            ));
        }
        return $this->redirect($this->generateUrl('admin_appointment'));
    }

}
