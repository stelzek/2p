<?php
namespace onekit\AppBundle\Controller\Admin;


use JMS\DiExtraBundle\Annotation as DI;

use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\PaginatorInterface;
use onekit\AppBundle\Controller\LegacyController;
use onekit\AppBundle\Entity\Doctor;
use onekit\AppBundle\Entity\SystemLog;
use onekit\AppBundle\Form\Type\DoctorFormType;
use onekit\AppBundle\Manager\DoctorManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * Admin Doctor controller.
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 * @Route("/doctors")
 */
class DoctorController extends LegacyController
{
    /**
     * @var DoctorManager
     */
    protected $doctorManager;

    /**
     * @DI\InjectParams({
     *     "doctorManager" = @DI\Inject("api.manager.doctor")
     * })
     *
     * @param DoctorManager $doctorManager
     */
    public function __construct(DoctorManager $doctorManager)
    {
        $this->doctorManager = $doctorManager;
    }

    /**
     * List of doctors
     *
     * @Route("/list", name="admin_doctor")
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function listAction(Request $request)
    {
        $qb = $this->doctorManager
            ->getList()
            ->QB();

        $page = $request->query->get('page', 1);
        $limit = $request->query->get('limit', 5);

        /** @var PaginatorInterface $pager */
        $pager = $this->get('knp_paginator');
        $pagination = $pager->paginate($qb, $page, $limit, $this->getPaginateOptions());

        return array(
            'pagination' => $pagination,
            'user' => $this->getUser(),
        );
    }

    /**
     * Finds and displays a Doctor entity.
     *
     * @Route("/{id}/show", name="admin_doctor_show")
     * @Method("GET")
     * @Template()
     *
     * @param $id
     * @return array
     */
    public function showAction($id)
    {
        $entity = $this->doctorManager->get($id);
        if (!$entity) {
            throw $this->createNotFoundException('not found');
        }

        return array(
            'entity' => $entity,
        );
    }

    /**
     * create Doctor
     * @Route("/create", name="admin_doctor_create")
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function createAction(Request $request)
    {
        return array();
    }


    /**
     * Edit Doctor
     * @Route("/{id}/edit", name="admin_doctor_edit")
     * @Template()
     *
     * @param Request $request
     * @param $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function editAction(Request $request, $id)
    {
        /** @var Doctor $doctor */
        $doctor = $this->doctorManager->get($id);
        $form = $this->createForm(new DoctorFormType(), $doctor);
        $form->handleRequest($request);
        if ($form->isValid()) {
            //$this->doctorManager->update($doctor);
            return $this->redirect($this->generateUrl('admin_doctor_show', ['id' => $doctor->getId()]));
        }
        $result['form'] = $form->createView();
        $result['doctor'] = $doctor;
        return $result;
    }

}
