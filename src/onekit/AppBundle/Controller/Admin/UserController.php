<?php

namespace onekit\AppBundle\Controller\Admin;

use Doctrine\ORM\EntityManager;
use Liip\ImagineBundle\Imagine\Cache;
use JMS\DiExtraBundle\Annotation as DI;

use onekit\AppBundle\Entity\SystemLog;
use onekit\AppBundle\Manager\UserManager;
use onekit\AppBundle\Repository\UserRepository;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use onekit\AppBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use onekit\AppBundle\Entity\Appointment;
use onekit\AppBundle\Entity\User;
use onekit\AppBundle\Entity\AdminUserSearch;
use onekit\AppBundle\Form\Type\UserFormType;
use onekit\AppBundle\Form\Type\AdminUserSearchType;
use onekit\AppBundle\Form\Type\AppointmentFormType;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query\Expr;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Admin User controller.
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 * @Route("/user")
 */
class UserController extends Controller
{

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * @var UserRepository
     */
    private $repo;

    /**
     * @var UserManager
     */
    private $manager;
    private $imagine;

    /**
     * @DI\InjectParams({
     *     "em" = @DI\Inject("doctrine.orm.entity_manager"),
     *     "manager" = @DI\Inject("manager.user"),
     *     "imagine" = @DI\Inject("liip_imagine.controller"),
     * })
     *
     * @param EntityManager $em
     * @param $manager
     * @param $imagine
     */
    public function __construct(EntityManager $em, $manager, $imagine)
    {
        $this->em = $em;
        $this->repo = $em->getRepository('AppBundle:User');
        $this->manager = $manager;
        $this->imagine = $imagine;
    }


    /**
     * List of users
     * @Route("/list", name="admin_user", requirements={"type" = "html"})
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function listAction(Request $request)
    {
        $sort = $request->get('sort') ? $request->get('sort') : 'u.id';
        $direction = $request->get('direction') ? $request->get('direction'):'desc';
        $userSearch = new AdminUserSearch();
        $options = ['csrf_protection' => false];
        $form = $this->createForm(new AdminUserSearchType(), $userSearch, $options);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $user =  $userSearch->user;
            if ($user instanceof User && $user->getId()) {
                return $this->redirect($this->generateUrl('admin_user_show', array('user_id' => $user->getId())));
            } else {
                $form->get('user')->addError(new FormError('Unknown user'));
            }
        }
        $limitPerPage = $request->get('limitPerPage') ? $request->get('limitPerPage') : 10;

        $qb = $this->repo->createQueryBuilder('u');
        $qb->groupBy('u.id');
        $qb->orderBy($sort,$direction);
        $qb->getQuery();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $qb,
            $this->get('request')->query->get('page', 1),
            $limitPerPage
        );
        return [
            'form'=>$form->createView(),
            'pagination' => $pagination,
            'user' => $this->getUser(),
            'limitPerPage' => $limitPerPage
        ];
    }

    /**
     * create User
     * @Route("/create", name="admin_user_create")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $user = new User();
        $form = $this->createForm(new UserFormType(), $user);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->get('fos_user.user_manager')->updateUser($user, true);
            return $this->redirect($this->generateUrl('admin_user_show', ['user_id' => $user->getId()]));
        }
        $result['form'] = $form->createView();
        $result['user'] = $user;
        return $result;
    }

    /**
     * Finds and displays a User entity.
     *
     * @Route("/{user_id}/show", name="admin_user_show")
     * @Method("GET")
     * @Template()
     *
     * @param $user_id
     * @return array
     */
    public function showAction($user_id)
    {
        $entity = $this->repo->find($user_id);

        if (!$entity) {
            throw $this->createNotFoundException('not found');
        }

        return array(
            'entity' => $entity,
        );
    }

    /**
     * Edit User
     * @Route("/{user_id}/edit", name="admin_user_edit")
     * @Template()
     */
    public function editAction(Request $request, $user_id = null)
    {
        $user = $this->repo->find($user_id);
        $form = $this->createForm(new UserFormType(), $user);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $this->get('fos_user.user_manager')->updateUser($user, true);
            return $this->redirect($this->generateUrl('admin_user_show', ['user_id' => $user->getId()]));
        }
        $result['form'] = $form->createView();
        $result['user'] = $user;
        return $result;
    }


    /**
     * Delete user
     * @Route("/{user_id}/delete", name="admin_user_delete")
     * @Template()
     */
    public function deleteAction($user_id)
    {
        $user = $this->repo->find($user_id);
        if ($user) {
                $this->manager->disable($user);
        }
        return $this->redirect($this->generateUrl('admin_user'));
    }

    /**
     * @Route("/autocomplete", name="admin_user_autocomplete")
     */
    public function autocompleteAction(Request $request)
    {
        /** @var User[] $results */
        $results = $this->manager->adminAutocomplete(
            $request->request->get('query'),
            intval($request->request->get('page', 1)),
            intval($request->request->get('limit', 25))
        );
        foreach ($results as &$user) {
            $user = array(
                'id' => $user->getId(),
                'value' => sprintf('"%s" &lt;%s&gt;', $user->getUsername(), $user->getEmail()),
            );
        }

        $response = new Response(json_encode($results));
        $response->headers->set('Content-Type', 'application/json');

        return $response;
    }

}
