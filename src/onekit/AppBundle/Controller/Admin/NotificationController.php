<?php

namespace onekit\AppBundle\Controller\Admin;

use Doctrine\ORM\EntityManagerInterface;
use Liip\ImagineBundle\Imagine\Cache;
use JMS\DiExtraBundle\Annotation as DI;

use onekit\AppBundle\Entity\LogSearch;
use onekit\AppBundle\Form\Type\LogSearchType;
use onekit\AppBundle\Manager\NotificationManager;
use Symfony\Component\HttpFoundation\Request;
use onekit\AppBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query\Expr;

use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Admin Log controller.
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 * @Route("/log")
 */
class NotificationController extends Controller
{

    private $em;
    private $repo;
    private $manager;

    /**
     * @DI\InjectParams({
     *     "em" = @DI\Inject("doctrine.orm.entity_manager"),
     *     "manager" = @DI\Inject("manager.notification")
     * })
     *
     * @param EntityManagerInterface $em
     * @param NotificationManager $manager
     */
    public function __construct(EntityManagerInterface $em, NotificationManager $manager)
    {
        $this->em = $em;
        $this->repo = $em->getRepository('AppBundle:Notification');
        $this->manager = $manager;
    }

    /**
     * List of logs
     * @Route("/list", name="admin_log", requirements={"type" = "html"})
     * @Template()
     *
     * @param Request $request
     * @return array
     */
    public function listAction(Request $request)
    {
        $sort = $request->get('sort')?$request->get('sort'):'created';
        $direction = $request->get('direction')?$request->get('direction'):'desc';
        $criteria = [];
        $limitPerPage = $request->get('limitPerPage') ? $request->get('limitPerPage') : 10;
        $qb = $this->repo->findBy($criteria,[$sort=>$direction]);
        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $qb,
            $this->get('request')->query->get('page', 1) /*page number*/,
            $limitPerPage /*limit per page*/
        );

         return ['pagination' => $pagination, 'user' => $this->getUser(),'limitPerPage' => $limitPerPage];

    }

}
