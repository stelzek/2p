<?php
namespace onekit\AppBundle\Controller\Admin;


use JMS\DiExtraBundle\Annotation as DI;

use onekit\AppBundle\Controller\LegacyController;
use onekit\AppBundle\Manager\PatientManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\BrowserKit\Request;

/**
 * Admin Patient controller.
 * @Security("has_role('ROLE_SUPER_ADMIN')")
 * @Route("/patients")
 */
class PatientController extends LegacyController
{
    /**
     * @var PatientManager
     */
    protected $patientManager;

    /**
     * @DI\InjectParams({
     *     "patientManager" = @DI\Inject("api.manager.patient")
     * })
     *
     * @param PatientManager $patientManager
     */
    public function __construct(PatientManager $patientManager)
    {
        $this->patientManager = $patientManager;
    }

    /**
     * List of users
     * @Route("/list", name="admin_patient")
     * @Template()
     */
    public function listAction()
    {
        return array();
    }

    /**
     * Finds and displays a Patient entity.
     *
     * @Route("/{id}/show", name="admin_patient_show")
     * @Method("GET")
     * @Template()
     *
     * @param $id
     * @return array
     */
    public function showAction($id)
    {
        $entity = $this->patientManager->get($id);
        if (!$entity) {
            throw $this->createNotFoundException('not found');
        }

        return array(
            'entity' => $entity,
        );
    }

    /**
     * create Patient
     * @Route("/create", name="admin_patient_create")
     * @Template()
     * 
     * @param Request $request
     * @return array
     */
    public function createAction(Request $request)
    {
        return array();
    }

}
