<?php

namespace onekit\AppBundle\Controller;

use Liip\ImagineBundle\Imagine\Cache;
use JMS\DiExtraBundle\Annotation as DI;

use Symfony\Component\HttpFoundation\Request;
use onekit\AppBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\Query\Expr;


/**
 * @Security("has_role('ROLE_USER')")
 * @Route("/user")
 */
class UserController extends Controller
{


    /**
     * @Route("/", name="user")
     */
    public function indexAction()
    {
        return $this->redirect($this->generateUrl('user_appointment'));
    }

}
