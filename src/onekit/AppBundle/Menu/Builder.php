<?php

namespace onekit\AppBundle\Menu;

use Doctrine\ORM\EntityManager;
use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    public function userMenu(FactoryInterface $factory)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'app-nav');
        $menu->addChild('My appointments', ['route' => 'user_appointment'])->setAttribute('class','app-nav__item');
        $menu->addChild('Settings', array('route' => 'fos_user_profile_show'))
            ->setAttribute('class', 'dropdown app-nav__item')
            ->setLinkAttribute('data-toggle', 'dropdown')
            ->setLinkAttribute('class', 'dropdown-toggle')
            ->setChildrenAttribute('class', 'dropdown-menu')
            ->setChildrenAttribute('role', 'menu');
        $menu['Settings']->addChild('See profile', array('route' => 'fos_user_profile_show'));
        $menu['Settings']->addChild('Edit profile', array('route' => 'fos_user_profile_edit'));
        $menu['Settings']->addChild('Change password', array('route' => 'fos_user_change_password'));
        return $menu;
    }

    public function assistantMenu(FactoryInterface $factory) {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'app-nav');
        $menu->addChild('Patients', array('route' => 'assistant_patient'))->setAttribute('class','app-nav__item');
        $menu->addChild('Appointments', array('route' => 'assistant_appointment_calendar'))->setAttribute('class','app-nav__item');
        $menu->addChild('SMS history', array('route' => 'assistant_sms_history'))->setAttribute('class','app-nav__item');
        $menu->addChild('Settings', array('route' => 'fos_user_profile_show'))
            ->setAttribute('class', 'dropdown app-nav__item')
            ->setLinkAttribute('data-toggle', 'dropdown')
            ->setLinkAttribute('class', 'dropdown-toggle')
            ->setChildrenAttribute('class', 'dropdown-menu')
            ->setChildrenAttribute('role', 'menu');
        $menu['Settings']->addChild('See profile', array('route' => 'fos_user_profile_show'));
        $menu['Settings']->addChild('Edit profile', array('route' => 'fos_user_profile_edit'));
        $menu['Settings']->addChild('Change password', array('route' => 'fos_user_change_password'));
        return $menu;
    }

    public function doctorMenu(FactoryInterface $factory) {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'app-nav');
        $menu->addChild('Patients', array('route' => 'doctor_patient'))->setAttribute('class','app-nav__item');
        $menu->addChild('Appointments', array('route' => 'doctor_appointment_calendar'))->setAttribute('class','app-nav__item');
        $menu->addChild('Assistants', array('route' => 'doctor_assistant'))->setAttribute('class','app-nav__item');
        $menu->addChild('SMS history', array('route' => 'doctor_sms_history'))->setAttribute('class','app-nav__item');
        $menu->addChild('SMS templates', array('route' => 'doctor_sms_message_template'))->setAttribute('class','app-nav__item');
        $menu->addChild('Settings', array('route' => 'fos_user_profile_show'))
            ->setAttribute('class', 'dropdown app-nav__item')
            ->setLinkAttribute('data-toggle', 'dropdown')
            ->setLinkAttribute('class', 'dropdown-toggle')
            ->setChildrenAttribute('class', 'dropdown-menu')
            ->setChildrenAttribute('role', 'menu');
        $menu['Settings']->addChild('See profile', array('route' => 'fos_user_profile_show'));
        $menu['Settings']->addChild('Edit profile', array('route' => 'fos_user_profile_edit'));
        $menu['Settings']->addChild('Change password', array('route' => 'fos_user_change_password'));
        return $menu;
    }

    public function adminMenu(FactoryInterface $factory)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'app-nav');
        $menu->addChild('Users', array('route' => 'admin_user'))->setAttribute('class','app-nav__item');
        $menu->addChild('Doctors', array('route' => 'admin_doctor'))->setAttribute('class','app-nav__item');
        $menu->addChild('Patients', array('route' => 'admin_patient'))->setAttribute('class','app-nav__item');
        $menu->addChild('Appointments', array('route' => 'admin_appointment'))->setAttribute('class','app-nav__item');
        $menu->addChild('Logs', array('route' => 'admin_log'))->setAttribute('class','app-nav__item');
        $menu->addChild('SMS templates', array('route' => 'admin_sms_message_template'))->setAttribute('class','app-nav__item');
        $menu->addChild('Settings', array('route' => 'admin_settings'))
            ->setAttribute('class', 'dropdown app-nav__item')
            ->setLinkAttribute('data-toggle', 'dropdown')
            ->setLinkAttribute('class', 'dropdown-toggle')
            ->setChildrenAttribute('class', 'dropdown-menu')
            ->setChildrenAttribute('role', 'menu');
        $menu['Settings']->addChild('Global', array('route' => 'admin_settings'));
        $menu['Settings']->addChild('See profile', array('route' => 'fos_user_profile_show'));
        $menu['Settings']->addChild('Edit profile', array('route' => 'fos_user_profile_edit'));
        $menu['Settings']->addChild('Change password', array('route' => 'fos_user_change_password'));
        return $menu;
    }
}