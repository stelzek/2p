<?php
namespace onekit\AppBundle\Security\Authorization\Voter;


use onekit\AppBundle\Entity\Appointment;
use onekit\AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authorization\Voter\AbstractVoter;

class AppointmentVoter extends AbstractVoter
{

    const VIEW = 'view';
    const EDIT = 'edit';
    const DELETE = 'delete';

    /**
     * @var DoctorVoter
     */
    protected $doctorVoter;

    /**
     * @var PatientVoter
     */
    protected $patientVoter;

    public function __construct(DoctorVoter $doctorVoter, PatientVoter $patientVoter)
    {
        $this->doctorVoter = $doctorVoter;
        $this->patientVoter = $patientVoter;
    }

    protected function getSupportedAttributes()
    {
        return array(
            self::VIEW,
            self::EDIT,
            self::DELETE,
        );
    }

    protected function getSupportedClasses()
    {
        return array('onekit\AppBundle\Entity\Appointment');
    }

    protected function isGranted($attribute, $appointment, $user = null)
    {
        if ($appointment instanceof Appointment && $user instanceof User) {
            if ($user->isSuperAdmin()) {
                return true;
            }
            switch ($attribute) {
                case self::VIEW:
                    $doctorAccess = $this->doctorVoter->isGrantedGate(DoctorVoter::VIEW, $appointment->getDoctor(), $user);
                    $patientAccess = $this->doctorVoter->isGrantedGate(PatientVoter::VIEW, $appointment->getPatient(), $user);
                    return $doctorAccess || $patientAccess;
                case self::EDIT:
                case self::DELETE:
                    return $this->doctorVoter->isGrantedGate(DoctorVoter::MANAGE_APPOINTMENTS, $appointment->getDoctor(), $user);
            }
        }
        return false;
    }

    public function isGrantedGate($attribute, $object, $user)
    {
        return $this->isGranted($attribute, $object, $user);
    }
}