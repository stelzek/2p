<?php
namespace onekit\AppBundle\Security\Authorization\Voter;


use onekit\AppBundle\Entity\Patient;
use onekit\AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authorization\Voter\AbstractVoter;

class PatientVoter extends AbstractVoter
{

    const VIEW = 'view';
    const EDIT = 'edit';
    const MANAGE_APPOINTMENTS = 'appointments';
    const DELETE = 'delete';

    /**
     * @var DoctorVoter
     */
    protected $doctorVoter;

    public function __construct(DoctorVoter $doctorVoter)
    {
        $this->doctorVoter = $doctorVoter;
    }

    protected function getSupportedAttributes()
    {
        return array(
            self::VIEW,
            self::EDIT,
            self::MANAGE_APPOINTMENTS,
            self::DELETE,
        );
    }

    protected function getSupportedClasses()
    {
        return array('onekit\AppBundle\Entity\Patient');
    }

    protected function isGranted($attribute, $patient, $user = null)
    {
        if ($patient instanceof Patient && $user instanceof User) {
            if ($user->isSuperAdmin()) {
                return true;
            }
            if (!is_null($patient->getAccount()) && $patient->getAccount()->getId() == $user->getId()) {
                return true;
            }
            switch ($attribute) {
                case self::VIEW:
                    return $this->doctorVoter->isGrantedGate(DoctorVoter::VIEW, $patient->getDoctor(), $user);
                case self::MANAGE_APPOINTMENTS:
                    return $this->doctorVoter->isGrantedGate(DoctorVoter::MANAGE_APPOINTMENTS, $patient->getDoctor(), $user);
                case self::EDIT:
                case self::DELETE:
                    return $this->doctorVoter->isGrantedGate(DoctorVoter::MANAGE_PATIENTS, $patient->getDoctor(), $user);
            }
        }
        return false;
    }

    public function isGrantedGate($attribute, $object, $user)
    {
        return $this->isGranted($attribute, $object, $user);
    }
}