<?php
namespace onekit\AppBundle\Security\Authorization\Voter;


use onekit\AppBundle\Entity\Assistant;
use onekit\AppBundle\Entity\User;
use Symfony\Component\Security\Core\Authorization\Voter\AbstractVoter;

class AssistantVoter extends AbstractVoter
{

    const VIEW = 'view';
    const EDIT = 'edit';
    const DELETE = 'delete';

    /**
     * @var DoctorVoter
     */
    protected $doctorVoter;

    public function __construct(DoctorVoter $doctorVoter)
    {
        $this->doctorVoter = $doctorVoter;
    }

    protected function getSupportedAttributes()
    {
        return array(
            self::VIEW,
            self::EDIT,
            self::DELETE,
        );
    }

    protected function getSupportedClasses()
    {
        return array('onekit\AppBundle\Entity\Assistant');
    }

    protected function isGranted($attribute, $assistant, $user = null)
    {
        if ($assistant instanceof Assistant && $user instanceof User) {
            if ($user->isSuperAdmin()) {
                return true;
            }
            if (!is_null($assistant->getAccount()) && $assistant->getAccount()->getId() == $user->getId()) {
                return true;
            }
            switch ($attribute) {
                case self::VIEW:
                case self::EDIT:
                case self::DELETE:
                    return $this->doctorVoter->isGrantedGate(DoctorVoter::MANAGE_ASSISTANTS, $assistant->getDoctor(), $user);
            }
        }
        return false;
    }

    public function isGrantedGate($attribute, $object, $user)
    {
        return $this->isGranted($attribute, $object, $user);
    }
}