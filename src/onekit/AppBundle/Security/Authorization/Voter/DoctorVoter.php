<?php
namespace onekit\AppBundle\Security\Authorization\Voter;


use onekit\AppBundle\Entity\Assistant;
use onekit\AppBundle\Entity\Doctor;
use onekit\AppBundle\Entity\User;
use onekit\AppBundle\Repository\AssistantRepository;
use Symfony\Component\Security\Core\Authorization\Voter\AbstractVoter;

class DoctorVoter extends AbstractVoter
{

    const VIEW = 'view';
    const EDIT = 'edit';
    const MANAGE_ASSISTANTS = 'assistants';
    const MANAGE_PATIENTS = 'patients';
    const MANAGE_APPOINTMENTS = 'appointments';
    const CHECK_BALANCE = 'balance';
    const MAKE_PAYMENT = 'payment';

    /**
     * @var AssistantRepository
     */
    protected $assistantRepo;

    public function __construct(AssistantRepository $assistantRepo)
    {
        $this->assistantRepo = $assistantRepo;
    }

    protected function getSupportedAttributes()
    {
        return array(
            self::VIEW,
            self::EDIT,
            self::MANAGE_ASSISTANTS,
            self::MANAGE_PATIENTS,
            self::MANAGE_APPOINTMENTS,
            self::CHECK_BALANCE,
            self::MAKE_PAYMENT,
        );
    }

    protected function getSupportedClasses()
    {
        return array('onekit\AppBundle\Entity\Doctor');
    }

    protected function isGranted($attribute, $doctor, $user = null)
    {
        if ($doctor instanceof Doctor && $user instanceof User) {
            if ($user->isSuperAdmin()) {
                return true;
            }
            if ($doctor->getAccount()->getId() == $user->getId()) {
                return true;
            }
            /** @var Assistant $access */
            $access = $this->assistantRepo->findOneBy(array(
                'doctor' => $doctor,
                'account' => $user,
            ));
            if (!$access) {
                return false;
            }
            switch ($attribute) {
                case self::VIEW:
                    return true;
                case self::EDIT:
                    return $access->getEdit();
                case self::MANAGE_PATIENTS:
                    return $access->getManagePatients();
                case self::MANAGE_APPOINTMENTS:
                    return $access->getManageAppointments();
                case self::CHECK_BALANCE:
                    return $access->getCheckBalance();
                case self::MAKE_PAYMENT:
                    return $access->getMakePayment();
                default:
                    return false;
            }
        }
        return false;
    }

    public function isGrantedGate($attribute, $object, $user)
    {
        return $this->isGranted($attribute, $object, $user);
    }
}